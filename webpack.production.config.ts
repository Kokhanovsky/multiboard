import * as fs from 'fs-extra';
import webpackConst from './backend/const';

fs.remove(webpackConst.buildPath, () => {
    console.log('Build path successfully deleted');
});

let configCommon: any = {
    devtool: 'source-map',
    entry: webpackConst.frontendEntry,
    output: {
        path: webpackConst.buildPath,
        filename: webpackConst.frontendBundleFilename
    },
    resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: ['', '.ts', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loaders: ['ng-annotate', 'ts-loader'],
                exclude: [webpackConst.nodeModulesPath]
            },
            {
                test: /\.css$/,
                loader: 'style!css'
            },
            {
                test: /\.sass$/,
                loader: 'style!css!sass'
            },
            {
                test: /\.scss/,
                loader: 'style!css!sass'
            },
            {
                test: /\.html$/,
                loader: 'raw'
            }
        ]
    }
};

//=================================================
console.log(webpackConst.buildPath)
let configAdmin = (Object as any).assign({}, configCommon, {
    output: {
        path: webpackConst.buildPath,
        filename: webpackConst.adminBundleFilename,
        publicPath: webpackConst.buildPathName
    },
    entry: [
        webpackConst.adminEntry
    ]
});

let configFrontend = (Object as any).assign({}, configCommon, {});


module.exports = [configFrontend, configAdmin];

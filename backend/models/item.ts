import * as mongoose from 'mongoose';
import { Common } from '../lib/common';
import { ICategory } from './category';
import { IRegion } from './region';
import { IMetro } from './metro';
import { ICity } from './city';
import { IDistrict } from './district';
import User, { IUser } from './user';
let mongoosePaginate = require('mongoose-paginate');
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IPlacemarker {
    lat: number,
    lng: number,
    caption?: string;
}

export interface ILocation {
    region: IRegion;
    city: ICity;
    metro?: IMetro;
    district?: IDistrict;
    placemarker?: IPlacemarker;
}

export interface IPhoto {
    file: mongoose.Schema.Types.ObjectId,
    user: mongoose.Schema.Types.ObjectId;
    description: string;
    // todo: remove it
    fileId: mongoose.Schema.Types.ObjectId,
}

export interface IYoutubeItem {
    title: string,
    youtubeId: string
}

export interface IItem extends mongoose.Document {
    id: string;
    user: IUser;
    // todo: remove it after migration
    userId: mongoose.Schema.Types.ObjectId;
    itemNumber: number;
    slug: string;
    phone: string;
    contactPerson: string;
    companyName: string;
    category: ICategory;
    location: ILocation;
    title: string;
    price: number;
    description: string;
    photos: IPhoto[];
    videos: IYoutubeItem[];
    tags: string[];
    upAt: Date;
    status: string,
    createdAt: Date;
    expiredAt: Date;
}

let itemSchema = new mongoose.Schema({
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', index: true },
        userId: { type: mongoose.Schema.Types.ObjectId, index: true },
        itemNumber: { type: Number, index: true },
        contactPerson: String,
        slug: { type: String, index: true },
        companyName: String,
        phone: String,
        category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', index: true },
        location: {
            region: { type: mongoose.Schema.Types.ObjectId, ref: 'Region', index: true },
            city: { type: mongoose.Schema.Types.ObjectId, ref: 'City', index: true },
            metro: { type: mongoose.Schema.Types.ObjectId, ref: 'Metro' },
            district: { type: mongoose.Schema.Types.ObjectId, ref: 'District' },
            placemarker: {
                lat: Number,
                lng: Number,
                caption: String
            }
        },
        title: String,
        price: Number,
        description: String,
        photos: {
            type: [{
                file: {
                    type: mongoose.Schema.Types.ObjectId,
                    ref: 'File'
                },
                description: String
            }],
            default: []
        },
        videos: {
            type: [{
                title: String,
                youtubeId: String
            }],
            default: []
        },
        tags: {
            type: [String],
            default: []
        },
        status: { type: String, 'enum': ['new', 'approved', 'denied'], 'default': 'new' },
        createdAt: { type: Date, 'default': Date.now, index: true },
        upAt: { type: Date, 'default': Date.now, index: true },
        expiredAt: { type: Date }
    })

.plugin(AutoIncrement, { inc_field: 'itemNumber' })
.plugin(mongoosePaginate);

itemSchema.index({
        'title': 'text',
        'tags': 'text',
        'description': 'text',
        'contactPerson': 'text',
        'companyName': 'text'
    },
    {
        name: 'textIndex',
        weights: {
            title: 10,
            tags: 10,
            content: 5,
            contactPerson: 2,
            companyName: 2
        },
        default_language: 'russian'
    });

itemSchema.pre('save', function (next: any) {
    this.slug = Common.translit(this.title);
    next();
});

// update user items
itemSchema.post('save', (item: IItem) => {
    let itemId: any = item.id;
    User.findById(item.user).then((user: IUser) => {
        if (user.items.indexOf(itemId as any) === -1) {
            user.items.push(itemId as any);
            user.save();
        }
    });
});

// update user items on delete
itemSchema.pre('remove', function (next: any) {
    let itemId: any = this.id;
    User.findById(this.userId).then((user: IUser) => {
        user.items && user.items.splice(user.items.indexOf(itemId as any), 1);
        user.save();
        next();
    });
});

const item = mongoose.model<IItem>('Item', itemSchema) as any;

export default item;

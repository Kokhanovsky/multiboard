import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { IItem } from './item';
let validate = require('mongoose-validator');
let sanitizeHtml = require('sanitize-html');
let shortId = require('shortid');
let mongoosePaginate = require('mongoose-paginate');

(mongoose as any).Promise = global.Promise;

export interface IUser extends mongoose.Document {
    id: string;
    shortId: string;
    email: string;
    nickname: string;
    firstname: string;
    lastname: string;
    password: string;
    facebook: string;
    vkontakte: string;
    odnoklassniki: string;
    twitter: string;
    instagram: string;
    google: string;
    avatarBlob: Buffer;
    role: string;
    verificationToken: string;
    verified: boolean;
    resetPasswordToken: string;
    items: IItem[];
    lastActivity: Date;
    createdAt: Date;
    compareVerificationToken: (token: string) => Promise<boolean>;
    comparePassword: (password: string) => Promise<boolean>;
    compareToken: (token: string) => Promise<boolean>;
}

let nameValidator = [
    validate({
        validator: 'isLength',
        arguments: [2, 50],
        passIfEmpty: true,
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

let nicknameValidator = [
    validate({
        validator: 'matches',
        arguments: ['^[a-z][a-z0-9-]{2,18}$']
    }),
    validate({
        validator: 'isLength',
        arguments: [3, 20],
        passIfEmpty: true,
        message: 'Name should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

let emailValidator = [
    validate({
        validator: 'isEmail'
    }),
    validate({
        validator: 'isLength',
        arguments: [5, 100],
        passIfEmpty: true,
        message: 'Email should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

let passwordValidator = [
    validate({
        validator: 'isLength',
        arguments: [5, 255],
        message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters'
    })
];

let userSchema = new mongoose.Schema({
    shortId: {
        type: String,
        unique: true,
        default: shortId.generate
    },
    email: {
        type: String,
        validate: emailValidator
    },
    nickname: {
        type: String,
        validate: nicknameValidator
    },
    firstname: {
        type: String,
        validate: nameValidator,
        required: true
    },
    lastname: {
        type: String,
        validate: nameValidator
    },
    password: {
        type: String,
        validate: passwordValidator
    },
    facebook: String,
    vkontakte: String,
    odnoklassniki: String,
    twitter: String,
    instagram: String,
    google: String,
    role: {
        type: String,
        enum: ['user', 'admin']
    },
    items: [ { type: mongoose.Schema.Types.ObjectId, ref: 'Item' } ],
    avatarBlob: Buffer,
    verificationToken: String,
    verified: { type: Boolean, default: false },
    resetPasswordToken: String,
    lastActivity: { type: Date },
    createdAt: { type: Date, default: Date.now }
},
{
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
})
.plugin(mongoosePaginate);

// remove all "dangerous" html tags
userSchema.pre('validate', function (next: any) {
    this._doc.firstname = sanitizeHtml(this._doc.firstname);
    this._doc.lastname = sanitizeHtml(this._doc.lastname);
    next();
});

userSchema.pre('save', bcryptIt);

userSchema.virtual('fullname').get(function () {
    return [this.firstname, this.lastname].join(' ').trim();
});

userSchema.methods.comparePassword = function (password: string): Promise<boolean> {
    let model = this._doc;
    return new Promise((resolve: any, reject: any) => {
        if (!password) {
            return reject(new Error('Empty password'));
        }
        bcrypt.compare(password, model.password, (err, isMatch) => {
            if (err) {
                return reject(err);
            }
            return resolve(isMatch);
        });
    });
};

userSchema.methods.compareToken = function (token: string): Promise<boolean> {
    let model = this._doc;
    return new Promise((resolve: any, reject: any) => {
        bcrypt.compare(token, model.resetPasswordToken, (err, isMatch) => {
            if (err) {
                return reject(err);
            }
            return resolve(isMatch);
        });
    });
};

userSchema.methods.compareVerificationToken = function (token: string): Promise<boolean> {
    let model = this._doc;
    return new Promise((resolve: any, reject: any) => {
        bcrypt.compare(token, model.verificationToken, (err, isMatch) => {
            if (err) {
                return reject(err);
            }
            return resolve(isMatch);
        });
    });
};

let UserModel = mongoose.model('User', userSchema);

function bcryptIt(next: any) {
    UserModel.findById(this._id).then((user: IUser) => {
        Promise.all([
            getHash(this, 'password', user && user.password),
            getHash(this, 'resetPasswordToken', user && user.resetPasswordToken),
            getHash(this, 'verificationToken', user && user.verificationToken)
        ]).then(() => {
            next();
        }).catch(next);
    });
}

function getHash(model: any, fieldName: string, oldVal: string): Promise<string> {
    return new Promise((resolve, reject) => {
        if ((!model[fieldName]) || (model[fieldName] === oldVal)) {
            return resolve();
        }
        bcrypt.genSalt(10, (err, salt) => {
            if (err) {
                return reject(err);
            }
            bcrypt.hash(model[fieldName], salt, (err, hash) => {
                if (err) {
                    return reject(err);
                }
                model[fieldName] = hash;
                return resolve();
            });
        });
    });
}

const user = mongoose.model<IUser>('User', userSchema) as any;

export default user;

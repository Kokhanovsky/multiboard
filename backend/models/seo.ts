import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

interface IParams {
    q: string;
    locations: mongoose.Schema.Types.ObjectId[];
    category: mongoose.Schema.Types.ObjectId;
}

export interface ISeo extends mongoose.Document {
    params: IParams;
    title: string;
    headerTitle: string;
    metaDescription: string;
    metaKeywords: string[];
    content: string;
}

let seoSchema = new mongoose.Schema({
    params: {
        q: { type: String },
        locations: { type: [mongoose.Schema.Types.ObjectId] },
        category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', index: true }
    },
    title: String,
    headerTitle: String,
    metaDescription: String,
    metaKeywords: [String],
    content: String
});

const Seo = mongoose.model<ISeo>('Seo', seoSchema) as any;

export default Seo;

import * as mongoose from 'mongoose';
import { IDistrict } from "./district";
import { IMetro } from "./metro";
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface ICity extends mongoose.Document {
    kind: 'city',
    cityNumber: number;
    region: mongoose.Schema.Types.ObjectId;
    population: number;
    name: string;
    namePrepositional: string;
    slug: string;
    districts: IDistrict[];
    metros: IMetro[];
    geonameId: number;
    location: {
        type: string;
        coordinates: number[];
    };
    lat: number;
    lng: number;
    // todo: remove it
    regionId: mongoose.Schema.Types.ObjectId;
}

let citySchema = new mongoose.Schema({
    cityNumber: { type: Number, index: true },
    region: { type: mongoose.Schema.Types.ObjectId, ref: 'Region', index: true },
    population: { type: Number, index: true },
    namePrepositional: String,
    name: { type: String, index: true },
    slug: { type: String, index: true },
    districts: [{ type: mongoose.Schema.Types.ObjectId, ref: 'District' }],
    metros: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Metro' }],
    geonameId: { type: Number, index: true },
    location: {
        type: { type: String, 'enum': 'Point', 'default': 'Point' },
        coordinates: { type: [ Number ], 'default': [0, 0] }
    },
    lat: Number,
    lng: Number
})
.plugin(AutoIncrement, { inc_field: 'cityNumber' });

citySchema.index({ location: '2dsphere' });

const City = mongoose.model<ICity>('City', citySchema) as any;

export default City;

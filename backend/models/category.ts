import * as mongoose from 'mongoose';
let materializedPlugin = require('mongoose-materialized');
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface ICategoryAncestor {
    id: string;
    name: string;
    slug: string;
}
export interface ICategory extends mongoose.Document {
    id: string;
    categoryNumber: number;
    name: string;
    slug: string;
    oldSlug: string;
    ids?: mongoose.Schema.Types.ObjectId[];
    ancestors?: ICategoryAncestor[];
}

let categorySchema = new mongoose.Schema({
    categoryNumber: { type: Number, index: true },
    name: { type: String, index: true },
    slug: { type: String, index: true },
    oldSlug: { type: String, index: true }
});

categorySchema
    .plugin(materializedPlugin)
    .plugin(AutoIncrement, { inc_field: 'categoryNumber' });

const Category = mongoose.model<ICategory>('Category', categorySchema) as any;

export default Category;

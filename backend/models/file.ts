import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

export interface IFile extends mongoose.Document {
    id: mongoose.Schema.Types.ObjectId;
    user: mongoose.Schema.Types.ObjectId;
    item: mongoose.Schema.Types.ObjectId;
    createdAt: Date;
    // todo: remove it
    userId: mongoose.Schema.Types.ObjectId;
    itemId: mongoose.Schema.Types.ObjectId;
}

let fileSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    user: mongoose.Schema.Types.ObjectId,
    item: mongoose.Schema.Types.ObjectId,
    createdAt: { type: Date, default: Date.now },
    // todo: remove it
    userId: mongoose.Schema.Types.ObjectId,
    itemId: mongoose.Schema.Types.ObjectId,
});

const File: mongoose.Model<IFile> = mongoose.model<IFile>('File', fileSchema);

export default File;

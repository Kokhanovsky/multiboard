import * as mongoose from 'mongoose';
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IMetro extends mongoose.Document {
    metroNumber: number;
    city: mongoose.Schema.Types.ObjectId;
    name: string;
    slug: string;
}

let metroSchema = new mongoose.Schema({
    metroNumber: { type: Number, index: true },
    name: String,
    slug: String,
    city: mongoose.Schema.Types.ObjectId,
})
.plugin(AutoIncrement, { inc_field: 'metroNumber' });

const Metro = mongoose.model<IMetro>('Metro', metroSchema) as any;

export default Metro;

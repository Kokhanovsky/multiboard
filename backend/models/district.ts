import * as mongoose from 'mongoose';
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IDistrict extends mongoose.Document {
    districtNumber: number;
    name: string;
    slug: string;
    city: mongoose.Schema.Types.ObjectId;
    // todo: remove cityId
    cityId: mongoose.Schema.Types.ObjectId;
}

let districtSchema = new mongoose.Schema({
    districtNumber: { type: Number, index: true },
    name: String,
    slug: String,
    city: mongoose.Schema.Types.ObjectId
})
.plugin(AutoIncrement, { inc_field: 'districtNumber' });

const District = mongoose.model<IDistrict>('District', districtSchema) as any;

export default District;

import * as mongoose from 'mongoose';

(mongoose as any).Promise = global.Promise;

interface ICity {
    id: string;
    name: string;
}

interface IMetro {
    id: string;
    name: string;
}

interface IDistrict {
    id: string;
    name: string;
}

interface ILocation {
    city: ICity;
    metro?: IMetro;
    district?: IDistrict;
}

export interface IDefaultItemValue extends mongoose.Document {
    phone: string;
    contactPerson: string;
    companyName: string;
    category: string;
    location: ILocation;
}

let defItemValueSchema = new mongoose.Schema({
    phone: String,
    contactPerson: String,
    companyName: String,
    category: mongoose.Schema.Types.ObjectId,
    location: {
        city: {
            id: mongoose.Schema.Types.ObjectId,
            name: String,
        },
        metro: {
            id: mongoose.Schema.Types.ObjectId,
            name: String,
        },
        district: {
            id: mongoose.Schema.Types.ObjectId,
            name: String,
        }
    }

});

const defItemValue = mongoose.model<IDefaultItemValue>('IDefaultItemValue', defItemValueSchema) as any;

export default defItemValue;

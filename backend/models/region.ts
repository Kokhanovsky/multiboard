import * as mongoose from 'mongoose';
let AutoIncrement = require('mongoose-sequence');

(mongoose as any).Promise = global.Promise;

export interface IRegion extends mongoose.Document {
    kind: 'region';
    regionNumber: number;
    name: string;
    slug: string;
    namePrepositional: string;
    population: number;
    geonameId: number;
    location: {
        type: string;
        coordinates: number[];
    };
    lat: number;
    lng: number;
}

let regionSchema = new mongoose.Schema({
    regionNumber: { type: Number, index: true },
    name: { type: String, index: true },
    namePrepositional: String,
    slug: { type: String, index: true },
    population: { type: Number, index: true },
    geonameId: { type: Number, index: true },
    location: {
        type: { type: String, 'enum': 'Point', 'default': 'Point' },
        coordinates: { type: [Number], 'default': [0, 0] }
    },
    lat: Number,
    lng: Number
})
.plugin(AutoIncrement, { inc_field: 'regionNumber' });

const Region = mongoose.model<IRegion>('Region', regionSchema) as any;

export default Region;

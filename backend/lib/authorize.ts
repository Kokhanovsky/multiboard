import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import { Request, Response } from 'express';
import User, { IUser } from '../models/user';
import { Server } from '../server/server';
import passport = require('passport');
import moment = require('moment');

export class Authorize {

    private secret: string = config.get('secret').toString();
    private cookieDomain: string = config.get('cookieDomain').toString();
    private redirectToUrl: string = '/login';

    constructor(private server: Server) {
        server.express.use(passport.initialize());
        passport.serializeUser((user: any, done: any) => {
            done(null, user);
        });
    }

    jwt(requiredRole?: string) {
        return (req: Request, res: Response, next: any) => {
            // set authRequired for views
            res.locals.authRequired = requiredRole ? true : false;
            let token = req.cookies.access_token;
            if (token) {
                jwt.verify(token, this.secret, (err: any, decoded: any) => {
                    // unauthorized
                    if (err && requiredRole) {
                        return this.process401(req, res, next);
                    }
                    if (err) {
                        return next();
                    }
                    User.findById(decoded.id).then((user: IUser) => {
                        if (user) {
                            req.user = user;
                            this.setToken(user, req, res);
                        }
                        if (user && (this.roleToInt(user.role) >= this.roleToInt(requiredRole))) {
                            user.lastActivity = moment().toDate();
                            user.save();
                            return next();
                        } else {
                            if (req.originalUrl === this.redirectToUrl) {
                                return next();
                            }
                            return this.process401(req, res, next);
                        }
                    }).catch((err: any) => this.process401(req, res, next));
                });
            }
            else {
                if (!requiredRole) {
                    return next();
                } else {
                    return this.process401(req, res, next);
                }
            }

        };
    }

    login(email: string, password: string, req: Request, res: Response, next: any) {
        return User.findOne({ email }).then((user: IUser) => {
            req.user = user;
            if (!user) {
                return this.process401(req, res, next);
            } else {
                user.comparePassword(password).then(match => {
                    if (!match) {
                        return this.process401(req, res, next);
                    } else {
                        let userJson: any = user.toJSON();
                        this.setToken(userJson, req, res);
                        this.server.sendJson(res, { success: true, role: userJson.role });
                        console.log('successful login');
                    }
                }).catch((err: any) => next(err));
            }
        }).catch((err: any) => next(err));
    }

    logout(res: Response) {
        this.clearCookies(res);
    }

    setToken(payload: IUser, req: Request, res: Response) {
        let pl = {
            id: payload._id.toString()
        };
        let token: string;
        let permanentLogin: boolean = true;
        if (typeof req.body.remember === 'boolean') {
            permanentLogin = req.body.remember;
        } else {
            permanentLogin = !req.cookies.access_session;
        }
        if (permanentLogin) {
            let maxAge = 60 * 60 * 24 * 365;
            token = jwt.sign(pl, this.secret, { expiresIn: maxAge });
            res.cookie('access_token', token, {
                httpOnly: true,
                domain: this.cookieDomain,
                maxAge: maxAge * 1000, // maxAge for the res.cookie should to be in milliseconds
            });
            res.clearCookie('access_session', { domain: this.cookieDomain });
        } else {
            token = jwt.sign(pl, this.secret, { expiresIn: 180 });
            res.cookie('access_token', token, { httpOnly: true, domain: this.cookieDomain });
            res.cookie('access_session', 'true', { domain: this.cookieDomain });
        }
    }

    private process401(req: Request, res: Response, next: any) {
        this.clearCookies(res);
        let err: any = new Error();
        err.status = 401;
        // if html then redirect to the login page
        if (/text\/html/.test(req.get('accept'))) {
            return res.redirect(this.redirectToUrl);
        } else {
            return next(err);
        }
    }

    private roleToInt(role: string): number {
        let val = 0;
        switch (role) {
            case 'user':
                val = 1;
                break;
            case 'admin':
                val = 2;
                break;
        }
        return val;
    }

    private clearCookies(res: Response) {
        res.clearCookie('access_token');
        res.clearCookie('access_session');
        res.clearCookie('access_token', { domain: this.cookieDomain });
        res.clearCookie('access_session', { domain: this.cookieDomain });
    }

}

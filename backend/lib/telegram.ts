import User, { IUser } from '../models/user';
import Chat, { IChat } from '../models/chat';
import Item, { IItem } from '../models/item';
import { Common } from './common';
import * as config from 'config';
import * as moment from 'moment';

let TelegramBot = require('node-telegram-bot-api');

export class Telegram {

    bot: any;
    host: string = config.get('host').toString();
    key: string = config.get('telegramKey') ? config.get('telegramKey').toString() : null;

    constructor() {
        if (!this.key) {
            return;
        }
        this.bot = new TelegramBot(this.key, { polling: true });

        // login into service
        this.bot.onText(/\/login (.+)/, (msg: any, match: any) => {
            let chatId = msg.chat.id;
            this.authorizeChat(chatId, match[1]).then((user: IUser) => {
                this.bot.sendMessage(chatId, 'Привет, ' + user.firstname);
                Chat.findOne({ chatId }).then((chat: IChat) => {
                    if (chat) {
                        return;
                    }
                    let ch = new Chat({ chatId, user });
                    ch.save();
                })
            });
        });

        this.bot.onText(/\/logout/, (msg: any) => {
            let chatId = msg.chat.id;
            Chat.findOne({ chatId }).then((chat: IChat) => {
                if (chat) {
                    chat.remove().then(() => {
                        return this.bot.sendMessage(chatId, 'Вы вышли');
                    });
                } else {
                    return this.bot.sendMessage(chatId, 'Вы не были залогинены');
                }
            });
        });

        this.bot.onText(/\/status/, (msg: any) => {
            let chatId = msg.chat.id;
            this.checkAuth(chatId).then((chat: IChat) => {
                this.getStatus().then((message: string) => {
                    this.bot.sendMessage(chatId, message);
                })
            })
        });
    }

    send(message: string) {
        if (!this.bot) {
            return Promise.resolve();
        }
        Chat.find({}).then((chats: IChat[]) => {
            chats.forEach((chat: IChat) => {
                this.bot.sendMessage(chat.chatId, message).catch((err) => {
                    let response = JSON.parse(err.response.body);
                    if (response.error_code == 400) {
                        Chat.findOneAndRemove({chatId: chat.chatId}).then(() => console.log('Chat has been removed'));
                    }
                })
            });
        });
    }

    noticeNewItem(item: IItem, user: IUser) {
        if (!this.bot) {
            return Promise.resolve();
        }
        this.getStatus().then((statusMessage: string) => {
            Item.findById(item._id).populate('location.city location.region category').then((item: IItem) => {
                let message = "У нас новое объявление!\n\n";
                if (item.location.city)
                    message += 'Город: ' + item.location.city.name + '\n';
                message += 'Категория: ' + item.category.name + '\n';
                message += 'Название: ' + item.title + '\n';
                message += 'Автор: ' + user.firstname + ' ' + user.lastname + ' (profile: ' + this.host + Common.getProfileUrl(user) + ')\n';
                message += 'Url: ' + this.host + Common.getItemUrl(item) + '\n';
                this.send(message + '\n' + statusMessage);
            });
        })
    }

    noticeNewUser(user: IUser) {
        if (!this.bot) {
            return Promise.resolve();
        }
        let message = "Пользователь зарегистрировался!\n";
        message += [user.firstname, user.lastname].join(' ')+ '\n';
        message += (user.email ? 'Email: ' + user.email : 'Через ' + Common.getUserSocial(user)) + '\n';
        this.send(message);
    }

    private authorizeChat(fromId: number, credentials: string) {
        let c = credentials.split(',');
        let email = c[0];
        let password = c[1];
        return new Promise((resolve, reject) => {
            User.findOne({ email }).then((user: IUser) => {
                if (!user || user.role !== 'admin') {
                    this.bot.sendMessage(fromId, 'Пользователь не найден');
                    return reject();
                } else {
                    user.comparePassword(password).then(match => {
                        if (!match) {
                            this.bot.sendMessage(fromId, 'Неверный пароль');
                            return reject();
                        } else {
                            return resolve(user);
                        }
                    });
                }
            });
        });
    }

    private checkAuth(chatId: number) {
        return new Promise((resolve, reject) => {
            Chat.findOne({ chatId }).populate('user').then((chat: IChat) => {
                if (chat) {
                    return resolve(chat);
                } else {
                    this.bot.sendMessage(chatId, 'Пожалуйста залогиньтесь при помощи команды /login  ваш_логин,ваш_пароль');
                    return reject();
                }
            })
        });
    }

    private getStatus() {
        return new Promise((resolve, reject) => {
            let message = '';
            Item.aggregate([
                {
                    $match: {
                        expiredAt: { $gt: moment().toDate() }
                    }
                },
                {
                    $group: {
                        _id: '$status',
                        count: { $sum: 1 }
                    }
                }
            ]).then((res: any[]) => {
                message += 'На данный момент мы имеем:\n';
                [
                    { text: 'Новых объявлений: ', status: 'new' },
                    { text: 'Актуальных объявлений: ', status: 'approved' }
                ].forEach((line: any) => {
                    let filt = res.filter((item: any) => item._id === line.status);
                    if (filt.length) {
                        message += line.text + filt[0].count;
                    } else {
                        message += line.text + 0
                    }
                    message += '\n';
                });
                return resolve(message);
            });
        });
    }
}

import { Server } from '../server/server';
const winston: any = require('winston');
import * as config from 'config';
const expressWinston: any = require('express-winston');
let winstonMongo = require('winston-mongodb').MongoDB;

export class Logger {

    constructor(private server: Server) {

        if (config.get('env') === 'localhost') {
            server.express.use(expressWinston.logger({
                transports: [
                    new winston.transports.Console({
                        //json: true,
                        colorize: true
                    })
                ],
                meta: true, // optional: control whether you want to log the meta data about the request (default to true)
                msg: 'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}', // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
                expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
                colorize: true
            }));

        }
        else {
            server.express.use(expressWinston.logger({
                transports: [
                    new (winston.transports.MongoDB)({
                        db: config.get('mongoDB'),
                        collection: 'logs-requests'
                    })
                ],
                meta: true, // optional: control whether you want to log the meta data about the request (default to true)
            }));
        }

    }

    logErrors() {
        this.server.express.use(expressWinston.errorLogger({
            transports: [
                new (winston.transports.MongoDB)({
                    db: config.get('mongoDB'),
                    collection: 'logs-errors'
                })
            ]
        }));
    }
}

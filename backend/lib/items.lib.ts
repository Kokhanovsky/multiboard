import { Common } from './common';
import Item, { IItem, IPhoto, IYoutubeItem } from '../models/item';
import { ICategory } from '../models/category';
import { IRegion } from '../models/region';
import { ICity } from '../models/city';
import moment = require('moment');
import { LocationLib } from './locationLib';
import { CategoryLib } from './categoryLib';
let mongoose = require('mongoose');

export interface IParamsModels {
    locations: Array<ICity | IRegion>;
    category?: ICategory;
    placemarker?: boolean,
    p?: number,
    q?: string,
    sort?: string
}

export class ItemsLib {

    static getLastItems(params: IParamsModels, limit: number = 5) {
        let filters = ItemsLib.getFilters(params);
        return new Promise((resolve, reject) => {
            Item.find(filters)
            .populate('category location.city location.metro location.district location.region')
            .sort({ 'upAt': -1 })
            .limit(limit)
            .then((items: IItem[]) => {
                if (items && items.length) {
                    return resolve(ItemsLib.makeItems(items));
                }
                return resolve(null)
            })
            .catch(err => reject(err));
        })
    }

    static getItemsList(params: IParamsModels) {
        let filters = ItemsLib.getFilters(params);
        let perPage = 24;

        return new Promise((resolve, reject) => {

            Item.aggregate([
                {
                    $match: filters
                },
                {
                    $project: {
                        "price": 1,
                        "title": 1,
                        "slug": 1,
                        "photos": 1,
                        "videos": 1,
                        "itemNumber": 1,
                        "createdAt": 1,
                        "user": 1,
                        "location": 1,
                        "category": 1,
                        "priceExists": { $cond: { 'if': { $gt: [ "$price", 0 ] }, 'then': true, 'else': false } }
                    }

                },
                {
                    $sort: ItemsLib.getSort(params)
                },
                {
                    $limit: perPage * (params.p - 1) + perPage
                },
                {
                    $skip: perPage * (params.p - 1)
                }
            ]).then((result) => {
                Item.populate(result, 'category location.city location.metro location.district location.region')
                .then((res) => {
                    if (!res) {
                        return resolve(null);
                    }
                    Item.count(filters).then(count => {
                        let result: any = {};
                        result.docs = res;
                        result.docs = this.makeItems(result.docs);
                        result.page = params.p;
                        result.total = count;
                        result.limit = perPage;
                        return resolve(result);
                    });
                })
            }).catch(reject);
        })
    }


    static getSimilarItems(item: IItem) {
        if (item.location && item.location.city) {
            item.location.city.kind = 'city';
        }
        return Promise.all([
            CategoryLib.getCategory(item.category).then((category: ICategory) => {
                return category.ancestors.map(cat => cat.id);
                //return CategoryLib.getCategory(category.ancestors[0].id).then((category: ICategory) => category.ids);
            }),
            LocationLib.getNearbyCities(item.location.city, 250)
        ]).then((res: any) => {

            let catIds = res[0];
            let locationFilter = res[1] ? { 'location.city': { $in: res[1].map(city => city._id.toString()) } } : item.location.region ? { 'location.region': item.location.region._id.toString() } : {};

            let search = (item.tags.join(' ') + ' ' + item.title).toLowerCase().replace(/[^а-яa-z0-9]+/gi, ' ').trim().replace(/\s+/, ' ');

            return Item.find(
                {
                    $and: [
                        { 'status': 'approved' },
                        { 'expiredAt': { $gt: new Date() } },
                        { '_id': { $ne: item._id }},
                        { $text: { $search: search } },
                        locationFilter,
                        {
                            'category': {
                                $in: catIds
                            }
                        }
                    ]
                },
                { score: { $meta: "textScore" } }
            )
            .select('title price slug photos videos itemNumber createdAt user location category')
            .populate('category location.city location.metro location.district location.region')
            .sort({ score: { $meta: 'textScore' } })
            .limit(8)
            .then((items: IItem[]) => {
                return this.makeItems(items);
            });
        });
    }

    static getPhotos(item: IItem) {
        if (!(item.photos || item.videos)) {
            return [];
        }
        let photos = [], videos = [];
        if (item.photos) {
            photos = item.photos.map((photo: IPhoto) => {
                return {
                    description: photo.description,
                    url: "/storage/cache/items/" + (item.user._id ? item.user._id.toString() : item.user) + "/" + photo.file.toString() + "/1280x720.jpg",
                    thumbUrl: "/storage/cache/items/" + (item.user._id ? item.user._id.toString() : item.user) + "/" + photo.file.toString() + "/300x290.jpg"
                };
            });
        }
        if (item.videos) {
            videos = item.videos.map((video: IYoutubeItem) => {
                return {
                    description: video.title,
                    thumbUrl: "https://img.youtube.com/vi/" + video.youtubeId + "/mqdefault.jpg"
                }
            })
        }
        return photos.concat(videos);
    }

    static getMapPoints(params: IParamsModels, limit: number = 500) {
        return new Promise((resolve, reject) => {
            let filters = ItemsLib.getFilters(params);
            filters['location.placemarker'] = { $ne: null };
            Item.find(filters)
            .populate('category location.city user')
            .select('title user location slug itemNumber category photos videos price')
            .sort({ 'upAt': -1 })
            .limit(limit)
            .lean()
            .then((items: IItem[]) => {
                if (items && items.length) {
                    let res = items.map((item: IItem) => {
                        let photos = this.getPhotos(item);
                        return {
                            title: item.title,
                            price: item.price,
                            photo: photos.length ? photos[0].thumbUrl : null,
                            url: Common.getItemUrl(item),
                            placemarker: item.location.placemarker

                        };
                    });
                    return resolve(res);
                }
                return resolve(null)
            })
            .catch(err => reject(err));
        })

    }

    static getMapPointsCount(params: IParamsModels) {
        let filters = ItemsLib.getFilters(params);
        filters['location.placemarker'] = { $ne: null };
        return new Promise((resolve, reject) => {
            Item.count(filters)
            .then(result => resolve(result))
            .catch(err => reject(err))
        });
    }

    static getFilters(params: IParamsModels): any {

        let filters: any = {
            $and: [
                { 'status': 'approved' },
                { 'expiredAt': { $gt: new Date() } }
            ]
        };

        if (params.q) {
            filters.$and.push({ $text: { $search: params.q } });
        }

        if (params.placemarker) {
            filters.$and.push({ 'location.placemarker': { $ne: null } });
        }
        if (params.locations) {
            let cities = [];
            params.locations.forEach(location => {
                cities.push((location.kind === 'city') ? { 'location.city': location._id } : { 'location.region': location._id })
            });
            filters.$and.push({ $or: cities });
        }
        if (params.category) {
            filters.$and.push({ category: { $in: params.category.ids.map(id => mongoose.Types.ObjectId(id)) } });
        }
        return filters;

    }

    static makeItems(items: IItem[]) {

        return items.map((item: IItem) => {
            let images = this.getPhotos(item);
            let location = item.location.city || item.location.region;
            return {
                title: item.title,
                url: Common.getItemUrl(item),
                location: {
                    name: location.name,
                    slug: location.slug
                },
                price: item.price,
                createdAt: item.createdAt,
                photosCount: item.photos && item.photos.length,
                videosCount: item.videos && item.videos.length,
                imagesSrc: images && images.length ? images.map(image => image.thumbUrl) : []
            };
        });
    }

    static getSort(params: IParamsModels) {
        let sort: any = params.q ? { score: { $meta: 'textScore' } } : { createdAt: -1 };
        switch(params.sort)
        {
            case 'price-asc':
                sort = { priceExists: -1, price: 1 };
                break;
            case 'price-desc':
                sort = { price: -1 };
                break;
            case 'date-asc':
                sort = { createdAt: 1 };
                break;
            case 'date-desc':
                sort = { createdAt: -1 };
                break;
        }
        return sort;
    }

}

import * as crypto from "crypto";
import { Request } from "express";
import * as moment from "moment";
import * as _ from "lodash";
import { IItem } from '../models/item';
import { IUser } from '../models/user';


let translit = require("translit")({
    "А": "A",
    "а": "a",
    "Б": "B",
    "б": "b",
    "В": "V",
    "в": "v",
    "Г": "G",
    "г": "g",
    "Д": "D",
    "д": "d",
    "Е": "E",
    "е": "e",
    "Ё": "E",
    "ё": "e",
    "Ж": "Zh",
    "ж": "zh",
    "З": "Z",
    "з": "z",
    "И": "I",
    "и": "i",
    "Й": "Y",
    "й": "y",
    "К": "K",
    "к": "k",
    "Л": "L",
    "л": "l",
    "М": "M",
    "м": "m",
    "Н": "N",
    "н": "n",
    "О": "O",
    "о": "o",
    "П": "P",
    "п": "p",
    "Р": "R",
    "р": "r",
    "С": "S",
    "с": "s",
    "Т": "T",
    "т": "t",
    "У": "U",
    "у": "u",
    "Ф": "F",
    "ф": "f",
    "Х": "Kh",
    "х": "kh",
    "Ц": "Ts",
    "ц": "ts",
    "Ч": "Ch",
    "ч": "ch",
    "Ш": "Sh",
    "ш": "sh",
    "Щ": "Sch",
    "щ": "sch",
    "ь": "",
    "Ы": "Y",
    "ы": "y",
    "ъ": "",
    "Э": "E",
    "э": "e",
    "Ю": "Yu",
    "ю": "yu",
    "Я": "Ya",
    "я": "ya"
});

export class Common {

    static translit(str: string) {
        str = translit(str.toLowerCase()).replace(/[^a-z0-9_\s-]+/g, '');
        return str.replace(/\s+/g, '_');
    }

    static thousandSeparator(str: any) {
        if (!str) {
            return str;
        }
        return str.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1&thinsp;')
    }

    static formatCreatedDate(date: Date) {
        moment.locale("ru");
        let long = 'D MMMM в H:mm';
        if (moment().year() > moment(date).year()) {
            long = 'D MMMM YYYY г. в H:mm';
        }
        return moment(date).calendar(null, {
            lastWeek: long,
            nextWeek: long,
            sameElse: long
        }).toLocaleLowerCase();
    }

    static makeToken(): Promise<string> {
        return new Promise((resolve, reject) => {
            crypto.randomBytes(20, (err: any, buf: any) => {
                if (err) {
                    return reject(err);
                }
                return resolve(buf.toString('hex'));
            });
        });
    }

    static makeHashFromString(str: string) {
        return require('crypto').createHash('md5').update(str).digest("hex");
    }

    static getRandString() {
        return Math.random().toString(36).substr(2, 5);
    }

    static getNetwork(req: Request) {
        let res: any = null;
        if (req.user.facebook) {
            res = {
                network: 'facebook',
                profile: 'https://facebook.com/' + req.user.facebook
            };
        }
        if (req.user.instagram) {
            res = {
                network: 'instagram',
                profile: 'https://instagram.com/' + req.user.instagram
            };
        }
        if (req.user.google) {
            res = {
                network: 'google',
                profile: 'https://plus.google.com/' + req.user.google
            };
        }
        if (req.user.vkontakte) {
            res = {
                network: 'vkontakte',
                profile: 'https://vk.com/id' + req.user.vkontakte
            };
        }
        if (req.user.odnoklassniki) {
            res = {
                network: 'odnoklassniki',
                profile: 'https://ok.ru/profile/' + req.user.odnoklassniki
            };
        }
        return res;
    }

    static getItemUrl(item: IItem) {
        if (!item || !item.location || !(item.location.region || item.location.city)) {
            return '';
        }
        return '/' + (item.location.city ? item.location.city.slug : item.location.region.slug) + '/' + item.category.slug + '/' + item.slug + '_' + item.itemNumber;
    }

    static getProfileUrl(user: IUser) {
        return '/' + (user.nickname ? 'user/' + user.nickname : 'id/' + user.shortId);
    }

    static getUserSocial(user: IUser) {
        if (user.facebook)
            return 'facebook';
        if (user.odnoklassniki)
            return 'odnoklassniki';
        if (user.instagram)
            return 'instagram';
        if (user.vkontakte)
            return 'vkontakte';
        if (user.google)
            return 'google';
        return null;
    }

    static getUserAvatarUrl(user: IUser) {
        return user.avatarBlob ? '/avatar/user/' + user._id + '.jpg' : null;
    }

    static truncate(text: string, n: number, useWordBoundary: boolean = true) {
        let isTooLong = text.length > n,
            s_ = isTooLong ? text.substr(0, n - 1) : text;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '...' : s_;
    }

    static trimChar(charToRemove: string, str: string) {
        if (!str) {
            return str;
        }
        while (str.charAt(0) == charToRemove) {
            str = str.substring(1);
        }

        while (str.charAt(str.length - 1) == charToRemove) {
            str = str.substring(0, str.length - 1);
        }
        return str;
    }

    static replaceUrlParam(url, paramName, paramValue) {
        if (paramValue == null)
            paramValue = '';
        let pattern = new RegExp('\\b(' + paramName + '=).*?(&|$)')
        if (url.search(pattern) >= 0) {
            return url.replace(pattern, '$1' + paramValue + '$2');
        }
        return url + (url.indexOf('?') > 0 ? '&' : '?') + paramName + '=' + paramValue
    }

    static removeUrlParam(sourceURL, key) {
        let rtn = sourceURL.split("?")[0],
            param,
            params_arr = [],
            queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (let i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtn;
    }

    static groupingCond(groupBy, array, index) {
        return (!((index + 1) % groupBy) && (index !== array.length - 1))
    }

}

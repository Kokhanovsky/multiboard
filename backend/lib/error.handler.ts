import { Server } from '../server/server';
import { Request, Response } from 'express';
export class ErrorHandler {

    constructor(private server: Server) {

        server.express.get('*',
            server.authorize.jwt(),
            server.locals.setLocals(),
            (req: Request, Response: any, next: any) => {
                let err: any = new Error();
                err.status = 404;
                next(err);
            });

        server.express.use((err: any, req: Request, res: Response, next: any) => {
            console.log(err);
            // vk 'failureRedirect is not working' patch
            if (err.name === 'VkontakteAuthorizationError') {
                return res.redirect('/login');
            }
            server.locals.setLocals()(req, res, next);
            let status = 500;
            let view = 'errors/error';
            let message = err.message ? err.message : 'Server error';
            let title = 'Server error';
            let authRequired = false;
            let navbar = false;
            switch (err.status) {
                case 401:
                    view = 'errors/401';
                    status = err.status;
                    title = 'Ошибка 401 - Необходима авторизация';
                    message = err.message ? err.message : 'Необходима авторизация';
                    break;
                case 403:
                    view = 'errors/403';
                    status = err.status;
                    message = err.message ? err.message : 'Forbidden';
                    title = 'Ошибка 403 - Доступ запрещен';
                    break;
                case 404:
                    view = 'errors/404';
                    status = err.status;
                    message = err.message ? err.message : 'Страница не найдена';
                    title = 'Ошибка 404 - Страница не найдена';
                    break;
            }
            // choose between html and json output
            if (/text\/html/.test(req.get('accept'))) {
                res.status(status).render(view, {
                    title,
                    status,
                    layout: 'layout'
                });
            } else {
                res.status(status).json({
                    message,
                    status
                });
            }
        });
    }

}

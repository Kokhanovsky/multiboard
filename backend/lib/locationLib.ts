import { Request } from 'express';
import * as config from 'config';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import City, { ICity } from '../models/city';
import Item, { IItem, ILocation } from '../models/item';
import Region, { IRegion } from '../models/region';
let sypex = require('sypexgeo-vyvid');
let geoDb = new sypex.Geo(config.get('sypexDB'));

export class LocationLib {

    constructor() {
    }

    getGeoLocation(req: Request) {
        let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        // to try out ip: /?ip=109.172.66.25
        if (req.query.ip) {
            ip = req.query.ip;
        }
        let geolocation = geoDb.find(ip);
        //console.log(geolocation);
        return new Promise((resolve, reject) => {
            if (!geolocation) {
                return resolve(null);
            } else if (geolocation.region && !geolocation.city) {
                this.getRegionByGeoname(geolocation.region.id).then((region: IRegion) => {
                    if (region) {
                        region.kind = 'region';
                        resolve(region);
                    } else {
                        resolve(null);
                    }
                });
            } else if (geolocation.city) {
                this.getCityByGeoname(geolocation.city.id).then((city: ICity) => {
                    if (city) {
                        city.kind = 'city';
                        resolve(city);
                    } else {
                        this.getRegionByGeoname(geolocation.region.id).then((region: IRegion) => {
                            if (region) {
                                region.kind = 'region';
                                resolve(region);
                            } else {
                                resolve(null);
                            }
                        });
                    }
                })
            } else {
                resolve(null);
            }
        });
    }

    static getLocations(slugs: string[]): Promise<Array<ICity|IRegion>> {
        return new Promise((resolve, reject) => {
            let promises = [];
            slugs.forEach(slug => {
                let promise = new Promise((resolve, reject) => {
                    City.findOne({slug}).lean(true).then((city: ICity) => {
                        if (city) {
                            city.kind = 'city';
                            return resolve(city);
                        }
                        Region.findOne({slug}).lean(true).then((region: IRegion) => {
                            if (region) {
                                region.kind = 'region';
                                return resolve(region);
                            }
                            return reject(new Error('Location not found: ' + slug));
                        }).catch(err => reject(err));
                    }).catch(err => reject(err));
                });
                promises.push(promise);
            });
            Promise.all(promises).then(result => resolve(result)).catch(err => reject(err));
        });
    }

    getRegionByGeoname(geonameId: string) {
        return new Promise((resolve, reject) => {
            Region.findOne({ geonameId }).then((region: IRegion) => {
                if (!region) {
                    return resolve(null);
                }
                resolve(_.extend({ type: 'region' }, region.toObject()));
            }).catch(reject);
        });
    }

    getCityByGeoname(geonameId: string) {
        return new Promise((resolve, reject) => {
            City.findOne({ geonameId }).then((city: ICity) => {
                if (!city) {
                    return resolve(null);
                }
                resolve(_.extend({ type: 'city' }, city.toObject()));
            }).catch(reject);
        });
    }

    getRegionCities(region: string) {
        return new Promise((resolve, reject) => {
            City.find({ region }).then((cities: ICity[]) => {
                if (!cities || !cities.length) {
                    return resolve(null);
                }
                resolve(cities.slice(0, 9));
            }).catch(reject);
        });
    }

    static getNearbyCities(city: ICity | IRegion, distance: number = 500, limit: number = 10, except: boolean = false) {
        if (!city || (city.kind !== 'city')) {
            return Promise.resolve(null);
        }

        let match: any = { $and: [{ 'resItems.status': 'approved' }, { 'resItems.expiredAt': { $gt: new Date() } }] };
        if (except) {
            match.$and.push({ '_id': { $ne: city._id } });
        }

        return City.aggregate([
            {
                $geoNear: {
                    near: { type: "Point", coordinates: city.location.coordinates },
                    distanceField: "distance",
                    maxDistance: 1000 * distance,
                    spherical: true,
                    num: 1000
                }
            },
            {
                $lookup: {
                    "from": "items",
                    "localField": "_id",
                    "foreignField": "location.city",
                    "as": "resItems"
                }
            },
            { $match: match },
            { $unwind: "$resItems" },
            {
                $group: {
                    _id: '$resItems.location.city',
                    count: { $sum: 1 },
                    name: { $first: '$name' },
                    distance: { $first: '$distance' },
                    slug: { $first: '$slug' }
                }
            },
            { $sort: { 'distance': 1 } }
        ]).limit(limit).then(data => {
            return data;
        })
    }

    static getTopCities(limit: number = 10) {

        let query: any[] = [
            { $match: { $and: [{ 'status': 'approved' }, { 'expiredAt': { $gt: new Date() } }] } },
            {
                $group: {
                    _id: '$location.city',
                    count: { $sum: 1 }
                }
            },
            {
                $lookup: {
                    "from": "cities",
                    "localField": "_id",
                    "foreignField": "_id",
                    "as": "city"
                }
            },
            { $unwind: "$city" },
            {
                $project: {
                    "_id": '$city._id',
                    "name": '$city.name',
                    "slug": '$city.slug',
                    "region": '$city.region',
                    "population": '$city.population',
                    "count": 1
                }
            },
            { $sort: { 'population': -1 } }
        ];

        if (limit) {
            query.push({ $limit: limit });
        }

        return Item.aggregate(query).then((cities) => {
            return City.populate(cities, {path: 'region', select: 'name slug', options: { lean: true }});
        });
    }

    static getTopRegions(limit: number = 10) {
        let query: any[] = [
            { $match: { $and: [{ 'status': 'approved' }, { 'expiredAt': { $gt: new Date() } }] } },
            {
                $group: {
                    _id: '$location.region',
                    count: { $sum: 1 }
                }
            },
            {
                $lookup: {
                    "from": "regions",
                    "localField": "_id",
                    "foreignField": "_id",
                    "as": "region"
                }
            },
            { $unwind: "$region" },
            {
                $project: {
                    "_id": '$region._id',
                    "name": '$region.name',
                    "slug": '$region.slug',
                    "population": '$region.population',
                    "count": 1
                }
            },
            { $sort: { 'population': -1 } }
        ];

        if (limit) {
            query.push({ $limit: limit });
        }

        return Item.aggregate(query);
    }

}

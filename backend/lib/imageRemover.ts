import File, { IFile } from '../models/file';
import { IPhoto, IItem } from '../models/item';
import { IUser } from '../models/user';
import * as config from 'config';
import * as moment from 'moment';
import * as del from 'del';
import * as path from 'path';

export class ImageRemover {

    private storageDir: string = config.get('storageDir').toString();

    private temporaryDirName: string = config.get('temporaryDirName').toString();

    constructor() {
    }

    public clearRemovedImages(item: IItem) {
        File.find({ item }).then((files: IFile[]) => {
            let whichHasAnItem = item.photos.map((photo: IPhoto) => photo.file.toString());
            let whichInFiles = files.map((file: IFile) => file._id.toString());
            let toRemove = whichInFiles.filter((inFile: string) => {
                return whichHasAnItem.indexOf(inFile) === -1;
            });
            toRemove.forEach((file: string) => {
                this.delFileCache(file, item.user.toString());
                this.delOriginalFile(file, item.user.toString());
                File.findOne({ _id: file }).remove().exec();
            });
        })
    }

    public deleteFilesByIds(user: string, ids: string[]) {
        return new Promise((resolve, reject) => {
            let fl = File.find({ '_id': { $in: ids }, user });
            fl.then((files: IFile[]) => {
                let promises: Promise<any>[] = [];
                files.forEach((file: IFile) => {
                    promises.push(this.delOriginalFile(file._id.toString(), user));
                    promises.push(this.delFileCache(file._id.toString(), user));
                    promises.push(fl.remove().exec());
                });
                Promise.all(promises).then(() => resolve()).catch(reject);
            }).catch(reject);
        });
    }

    public deleteImage(reqBody: any, user: IUser) {
        return new Promise((resolve, reject) => {
            let userId = user ? user._id.toString() : null;
            let fl = File.findOne({
                _id: reqBody.file.file,
                $or: [{user: userId}, {user: null}]
            });
            fl.then((file: IFile) => {
                if (!file) {
                    return resolve();
                }
                let fileId = file._id.toString();
                if (userId) {
                    this.delOriginalFile(fileId, userId).then(() => console.log('deleted original file'));
                    this.delFileCache(fileId, userId).then(() => console.log('deleted cached file'));
                }
                this.delTmpOriginal(fileId).then(() => console.log('deleted tmp original file'));
                this.delTmpCache(fileId).then(() => console.log('deleted tmp cached file'));
                fl.remove().exec().then(() => console.log('removed the file from db'));
            }).catch(reject);
        });
    }

    private delOriginalFile(fileId: string, userId: string): Promise<any> {
        return del(path.join(this.storageDir, 'items', userId, fileId + '.jpg'));
    }

    private delFileCache(fileId: string, userId: string): Promise<any> {
        return del(path.join(this.storageDir, 'cache/items', userId, fileId));
    }

    private delTmpOriginal(fileId: string): Promise<any> {
        let filename = path.resolve('./' + path.join(this.storageDir, 'items', this.temporaryDirName, fileId + '.jpg'));
        return del(filename);
    }

    private delTmpCache(fileId: string): Promise<any> {
        let dir = path.resolve('./' + path.join(this.storageDir, 'cache', 'items', this.temporaryDirName, fileId));
        return del(dir);
    }



}


import { NavbarController } from '../pages/components/navbar/navbar.controller';
import { Server } from '../server/server';
import { Response, Request } from 'express';
import { Common } from './common';
import { LocationLib } from './locationLib';
import * as moment from 'moment';

export class Locals {

    constructor(private server: Server) {
    }

    setLocals() {

        return (req: Request, res: Response, next: any) => {
            let location = new LocationLib();
            location.getGeoLocation(req).then((loc: any) => {

                    res.locals = {

                        moment,

                        location: loc,

                        authRequired: !!res.locals.authRequired,

                        user: this.getUser(req),

                        Common,

                        navbar: () => {
                            let navbarCtrl = new NavbarController(req, loc);
                            return navbarCtrl.render();
                        }

                    };

                    next();


                });
        }
    }

    getUser(req: Request) {
        if (!req.user) {
            return null;
        }
        let user: any = {
            username: req.user.firstname,
            role: req.user.role,
            verified: req.user.verified || !req.user.email,
            id: req.user._id.toString()
        };
        if (req.user.email) {
            user.email = req.user.email;
        }
        if (req.user.nickname) {
            user.nickname = req.user.nickname;
        }
        let network = Common.getNetwork(req);
        if (network) {
            user.social = network;
        }
        return user;
    }

}

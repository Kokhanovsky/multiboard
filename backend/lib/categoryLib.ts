import { Server } from '../server/server';
import Category, { ICategory } from '../models/category';
import Item, { IItem } from '../models/item';
import { IParamsModels, ItemsLib } from './items.lib';
import * as _ from 'lodash';

export interface ICategoryAncestor {
    id: string;
    name: string;
    slug: string;
}

export class CategoryLib {

    categoriesTree: any;
    subcategories: any[] = [];
    category: ICategory;
    filters: any;

    constructor(private server: Server) {
    }

    getSubcategories(params: IParamsModels) {
        this.category = params.category;
        let p = _.extend({}, params);
        delete p.category;
        this.filters = ItemsLib.getFilters(p);
        return this.getFullCategories().then((subcategories: any[]) => {
            return subcategories && subcategories.filter(sub => sub.counts > 0).map(sub => {
                return { name: sub.name, slug: sub.slug, counts: sub.counts }
            })
        });
    }

    // it accepts objectId, slug or model
    static getCategory(category: ICategory | string): Promise<ICategory> {
        return new Promise((resolve, reject) => {
            if (!category) {
                return resolve(undefined);
            }
            let filter: any = {};
            if ((typeof category) === 'string') {
                if ((category as string).match(/^[0-9a-fA-F]{24}$/)) {
                    filter['_id'] = category; // By ObjectID
                } else {
                    filter['slug'] = category; // By Slug
                }
                Category.findOne(filter).then((category: any) => {
                    if (!category) {
                        return Promise.reject(new Error('Category not found'));
                    }
                    return Promise.all([getCatChildren(category), getAncestors(category as ICategory)]).then(res => {
                        (category as ICategory).ids = res[0];
                        (category as ICategory).ancestors = res[1];
                        return resolve(category);
                    }).catch(err => reject(err));
                }).catch(err => reject(err));
            } else {
                let promises = [];
                let cat: ICategory = category as ICategory;
                if (!cat.ids) {
                    promises.push(getCatChildren(cat));
                } else {
                    promises.push(Promise.resolve(cat.ids));
                }
                if (!cat.ancestors) {
                    promises.push(getAncestors(cat));
                } else {
                    promises.push(Promise.resolve(cat.ancestors));
                }
                return Promise.all(promises).then(res => {
                    cat.ids = res[0];
                    cat.ancestors = res[1];
                    return resolve(category);
                }).catch(err => reject(err));
            }
        });
        function getCatChildren(cat: any): Promise<any> {
            let catIds: string[] = [cat._id.toString()];
            return cat.getChildren({}).then((children: any) => {
                if (!children || !children.length) {
                    return Promise.resolve(catIds);
                }
                return Promise.resolve(catIds.concat(children.map((category: ICategory) => category.id)));
            });
        }

        function getAncestors(category: ICategory): Promise<ICategory[]> {
            let cat: ICategoryAncestor[] = [{
                id: category.id,
                name: category.name,
                slug: category.slug
            }];
            return new Promise((resolve, reject) => {
                return (category as any).getAncestors((err: any, docs: ICategory[]) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!docs || !docs.length) {
                        return resolve(cat);
                    }
                    let arr = docs.map((category: ICategory) => {
                        return {
                            id: category.id,
                            name: category.name,
                            slug: category.slug
                        }
                    });
                    resolve(arr.concat(cat));
                });
            });
        }
    }

    private getFullCategoriesTree() {
        return new Promise((resolve, reject) => {
            if (this.server.categoriesTree) {
                return resolve(JSON.parse(JSON.stringify(this.server.categoriesTree)));
            }
            Category.GetFullArrayTree((err: any, tree: any) => {
                if (err) {
                    reject(err);
                }
                this.server.categoriesTree = JSON.parse(JSON.stringify(tree));
                this.categoriesTree = tree;
                return resolve(tree);
            });
        });
    }

    private getFullCategories() {
        return new Promise((resolve, reject) => {
            this.getFullCategoriesTree().then((tree: any[]) => {
                this.getCategoriesCount().then((counts: any) => {
                    this.calcSubcategories(tree, counts, null);
                    if (!this.category) {
                        this.subcategories = [].concat(tree);
                    }
                    return resolve(this.subcategories);
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        });
    }

    private calcSubcategories(n: any, counts: any, parentNode: any) {
        n.map((node: any) => {
            if (parentNode) {
                node.parent = parentNode;
            }
            if (node.counts === undefined) {
                node.counts = 0;
            }
            if (counts[node.id]) {
                this.recalcCount(node, counts[node.id]);
            }
            if (this.category && this.category.id === node.id) {
                this.subcategories = node.children;
            }
            if (node.children) {
                this.calcSubcategories(node.children, counts, node);
            }
        });
    }

    private recalcCount(node: any, count: number) {
        node.counts += count;
        if (node.parent) {
            this.recalcCount(node.parent, count);
        }
    }

    private getCategoriesCount() {
        return new Promise((resolve, reject) => {
            Item.aggregate([
                { $match: this.filters },
                { $group: { _id: '$category', count: { $sum: 1 } } }
            ]).then((items: IItem[]) => {
                let res: any = {};
                items.forEach((item: any) => {
                    res[item._id.toString()] = item.count;
                });
                resolve(res);
            }).catch(err => reject(err));
        })
    }
}

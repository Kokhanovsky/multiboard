import * as moment from 'moment';
import Item from '../models/item';

interface IPeriodItemsCount {
    date: Date;
    count: number;
}

export class Stat {

    constructor() {
    }

    // utc offset - in minutes
    // period - in days
    static getStat(utcOffset: number = 180, period: number = 7) {
        return Item.aggregate([
            {
                $match: {
                    $and: [
                        { status: 'approved' },
                        { createdAt: { $gt: new Date(moment().add(-period, 'days')) } }
                    ]
                }
            },
            {
                $group: {
                    _id: {
                        $let: {
                            vars: {
                                date: { $add: ['$createdAt', utcOffset * 60 * 1000] }
                            },
                            in: {
                                $dayOfYear: "$$date"
                            }
                        }
                    },
                    count: { $sum: 1 },
                    last: { $max: "$createdAt" }
                }
            },
            { $sort: { _id: -1 } },
            { $limit: period },
            { $project: { createdAt: "$last", count: 1 } }
        ]).then((res: any[]) => {
            let currentDayOfYear = parseInt(moment().format('DDD'));
            let days: IPeriodItemsCount[] = [];
            for (let i = currentDayOfYear - period + 1; i <= currentDayOfYear; i++) {
                let filtered = res.filter(item => item._id === i)[0];
                days.push({
                    date: moment().subtract(currentDayOfYear - i, 'days').toDate(),
                    count: filtered ? filtered.count : 0
                });
            }
            return days;
        });
    }


}

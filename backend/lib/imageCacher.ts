import { Request, Response } from 'express';
import * as config from 'config';
import * as fs from 'fs-extra';
import * as path from 'path';

let sharp = require('sharp');

interface IRequestData {
    userId: string;
    fileName: string;
    width: number;
    height: number;
}

export class ImageCacher {

    private quality: number = 85;
    private storageDir: string = config.get('storageDir').toString();
    private allowedResolutions: Array<Array<number>> = [
        [80, 80],
        [100, 100],
        [150, 150],
        [200, 200],
        [300, 290],
        [500, 500],
        [800, 600],
        [1280, 720],
        [1920, 1080]
    ];
    private requestData: IRequestData;

    constructor(private req: Request, private res: Response, private next: any) {
    }

    checkRoute(path: string) {
        const regex = /^\/(.+)\/(.+)\/([0-9]{2,4})x([0-9]{2,4})\.jpg$/g;
        let m: any;
        if ((m = regex.exec(path)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            this.requestData = {
                userId: m[1],
                fileName: m[2],
                width: m[3] * 1,
                height: m[4] * 1
            };
            return this.checkResolution() ? this.requestData : null;
        }
        return null;
    }

    getImage() {
        return new Promise((resolve, reject) => {
            if (!this.checkRoute(this.req.path)) {
                return this.next(new Error());
            }
            this.getOriginalFile().then((originalFilename: string) => {
                this.getCacheFile().then((cacheFilename: string) => {
                    let shrp = sharp(originalFilename);
                    shrp.resize(this.requestData.width, this.requestData.height);
                    shrp.max().withoutEnlargement().jpeg({quality: this.quality}).toFile(cacheFilename)
                    .then(() => resolve()).catch(reject);
                }).catch(reject);
            }).catch(reject);
        });
    }

    private checkResolution(): boolean {
        let allowed = false;
        this.allowedResolutions.forEach((resolution: number[]) => {
            if ((resolution[0] == this.requestData.width) && (resolution[1] == this.requestData.height)) {
                allowed = true;
                return;
            }
        });
        return allowed;
    }

    private getOriginalFile() {
        let filename = path.join(this.storageDir, 'items', this.requestData.userId, this.requestData.fileName + '.jpg')
        return new Promise((resolve, reject) => {
            fs.stat(filename, (err, stat) => {
                if (err) {
                    reject(err)
                } else {
                    resolve(filename);
                }
                resolve(filename);
            });
        });
    }

    private getCacheFile(): Promise<string> {
        return new Promise((resolve, reject) => {
            let dir = path.join(this.storageDir, 'cache/items', this.requestData.userId, this.requestData.fileName);
            fs.ensureDir(dir, (err) => {
                if (err) {
                    return reject(err);
                }
                let fileName = path.join(dir, this.requestData.width + 'x' + this.requestData.height + '.jpg');
                return resolve(fileName);
            });
        });
    }

}


export default (server, chai) => {

    describe('GET pages', () => {

        it('it should render homepage', function(done) {
            chai.request(server.express)
            .get('/')
            .end((err, res) => {
                res.should.have.status(200);
                //res.body.should.be.a('array');
                //res.body.length.should.be.eql(0);
                done();
            });
        });

        it('it should render agreement', function(done) {
            chai.request(server.express)
            .get('/agreement')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        it('it should render item page', (done) => {
            chai.request(server.express)
            .get('/rostov-na-donu/uslugi/predlozhenie_uslug/krasota_zdorove/my_vas_pokhudeem_1250')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        it('it should render city page', (done) => {
            chai.request(server.express)
            .get('/moskva')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

        it('it should not access admin moderate page', (done) => {
            chai.request(server.express)
            .get('/adm-service/moderate')
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
        });

        it('it should not access admin users page', (done) => {
            chai.request(server.express)
            .get('/adm-service/users')
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
        });

        it('it should render region page', (done) => {
            chai.request(server.express)
            .get('/rostovskaya_oblast')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
        });

    });

}

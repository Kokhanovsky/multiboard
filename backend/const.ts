import * as path from 'path';

module webpackConst {
    export const publicPath = path.join(__dirname, 'public');
    export const nodeModulesPath = path.join(__dirname, '../node_modules');
    export const buildPathName = '/build/';
    export const buildPath = path.join(__dirname, 'public', buildPathName);
    export const frontendBundleFilename = 'bundle.js';
    export const adminBundleFilename = 'bundle-admin.js';
    export const frontendEntry = path.join(__dirname, '../frontend/app', 'app.ts');
    export const adminEntry = path.join(__dirname, '../frontend/app-admin', 'app.ts');
    export const devPort = 8100;
}
//console.log(webpackConst);
export default webpackConst;

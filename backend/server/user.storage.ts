import * as express from 'express';
import { Request, Response } from 'express';
import { Server } from './server';
import * as config from 'config';
import { ImageCacher } from '../lib/imageCacher';

export default (server: Server) => {

    // user storage, protected static folder
    server.express.use('/storage/cache/items/',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
        let iCacher = new ImageCacher(req, res, next);
        iCacher.getImage()
            .then(() => next())
            .catch(err => {
                err.status = 404;
                next(err)
            });
    });

    // user storage, protected static folder
    server.express.use('/storage/user/', server.authorize.jwt('user'), (req: Request, res: Response, next: any) => {
        const regex = /^\/(.*?)\//g;
        let m: any;
        while ((m = regex.exec(req.path)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }
            if (m[1] === req.user._id.toString()) {
                return next();
            }
        }
        let err: any = new Error();
        err.status = 403;
        return next(err);
    });

    server.express.use('/storage', express.static(config.get('storageDir').toString()));

}

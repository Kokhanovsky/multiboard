import { Request, Response } from 'express';
import { Server } from '../../server/server';
import City, { ICity } from '../../models/city';
import Item, { IItem } from '../../models/item';
import { IRegion, default as Region } from '../../models/region';
import moment = require('moment');
import { Common } from '../../lib/common';
import { LocationLib } from '../../lib/locationLib';

let sm = require('sitemap');

export class SitemapController {

    private sitemap;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.sitemap = sm.createSitemap({
            hostname: 'https://multiboard.ru',
            cacheTime: 600000
        });
    }

    items() {
        return Item.find({ status: 'approved' })
            .populate('category location.city location.region')
            .then((items: IItem[]) => {
                items.forEach((item: IItem) => this.sitemap.add({ url: Common.getItemUrl(item) }));
                return items;
            });
    }

    citiesPodat(additem: boolean = false) {
        return new Promise((resolve, reject) => {
            City.find({}).then((cities: ICity[]) => {
                if (!cities) {
                    return reject(new Error('Cities not found'));
                }
                cities.forEach((city: ICity) => this.sitemap.add({ url: (additem ? '/podat_obyavlenie/' : '/') + city.slug }));
                resolve();
            })
        })
    }

    regionsPodat(additem: boolean = false) {
        return new Promise((resolve, reject) => {
            Region.find({}).then((regions: IRegion[]) => {
                if (!regions) {
                    return reject(new Error('Regions not found'));
                }
                regions.forEach((region: IRegion) => this.sitemap.add({ url: (additem ? '/podat_obyavlenie/' : '/') + region.slug }));
                resolve();
            })
        })
    }

    renderSitemapItems() {
        this.items().then(() => {
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    renderSitemapCities() {
        LocationLib.getTopCities(null).then((cities: ICity[]) => {
            cities.forEach((city: ICity) => this.sitemap.add({ url: '/' + city.slug }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    renderSitemapRegions() {
        LocationLib.getTopRegions(null).then((regions: IRegion[]) => {
            regions.forEach((region: IRegion) => this.sitemap.add({ url: '/' + region.slug }));
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    renderSitemapAdditemCities() {
        this.citiesPodat(true).then(() => {
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }

    renderSitemapAdditemRegions() {
        this.regionsPodat(true).then(() => {
            this.sitemap.toXML((err, xml) => {
                if (err) {
                    return this.next(err);
                }
                this.res.header('Content-Type', 'application/xml');
                this.res.send(xml);
            });
        });
    }
}

import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { SitemapController } from './sitemap.controller';

export default (server: Server) => {

    server.express.get('/sitemap-items.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.renderSitemapItems();
        });

    server.express.get('/sitemap-cities.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.renderSitemapCities();
        });

    server.express.get('/sitemap-regions.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.renderSitemapRegions();
        });

    server.express.get('/sitemap-cities-add.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.renderSitemapAdditemCities();
        });

    server.express.get('/sitemap-regions-add.xml',

        (req: Request, res: Response, next: any) => {
            let ctrl = new SitemapController(req, res, next, server);
            ctrl.renderSitemapAdditemRegions();
        });

}

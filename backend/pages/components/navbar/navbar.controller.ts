import { Request } from 'express';
import * as fs from 'fs-extra';
import * as url from 'url';
let ejs: any = require('ejs');

// read ejs template as string
let templateString = fs.readFileSync(require.resolve('./navbar.template.ejs'), 'utf-8');

interface IMenuItem {
    title: string;
    url: string;
    attr?: string;
    active?: boolean;
    auth?: boolean;
    noAuth?: boolean;
    admin?: boolean;
}

export class NavbarController {

    private menuItems: IMenuItem[] = [
        { title: 'Админ', url: '/adm-service/moderate', auth: true, admin: true },
        { title: 'Мои объявления', url: '/user-items', attr: 'hide-xs', auth: true },
        { title: 'Профиль', url: '/profile', attr: 'hide-xs hide-sm hide-md', auth: true },
        { title: 'Вход', url: '/login', noAuth: true }
    ];

    constructor(private req: Request, private location: any) {
        this.setActive();
        this.filterItems();
    }

    render() {
        return ejs.render(templateString, {
            location: this.location,
            menuItems: this.menuItems,
            user: this.req.user
        });
    }

    private setActive() {
        this.menuItems.forEach((menuItem: IMenuItem) => menuItem.active = url.parse(this.req.originalUrl).pathname === menuItem.url);
    }

    private filterItems() {
        this.menuItems = this.menuItems.filter((item: IMenuItem) => {
            if (!this.req.user) { // not auth user
                return !item.auth;
            } else {
                return (!item.admin || (this.req.user.role === 'admin')) && !item.noAuth;
            }
        });
    }

}

import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { VerificationController } from './verification.controller';

export default (server: Server) => {

    server.express.get('/user/verification/token/:token', (req: Request, res: Response, next: any) => {

        let ctrl = new VerificationController(req, res, next, server);
        ctrl.validateToken()
            .then((decoded: any) => res.redirect('/user/verification'))
            .catch(() => res.redirect('/user/verification-error'));

    });

    server.express.get('/user/verification',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new VerificationController(req, res, next, server);
            ctrl.doVerification()
                .then(() => ctrl.renderVerificationPage())
                .catch(() => res.redirect('/user/verification-error'));

        });

    server.express.get('/user/verification/resend',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new VerificationController(req, res, next, server);
            ctrl.renderVerificationResendPage();

        });

    server.express.post('/api/user/resend/verification', (req: Request, res: Response, next: any) => {

        let ctrl = new VerificationController(req, res, next, server);
        ctrl.resendVerification()
            .then(() => server.sendJson(res, { 'email': 'sent' }))
            .catch(next);

    });

    server.express.get('/user/verification-error',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new VerificationController(req, res, next, server);
            ctrl.renderVerificationErrorPage();

        });


}

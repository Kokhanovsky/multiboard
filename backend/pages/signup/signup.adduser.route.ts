import * as express from "express";
import { Server } from '../../server/server';
import { SignUpController } from './signup.controller';

export default (server: Server) => {

    server.express.post('/api/signup', (req: express.Request, res: express.Response, next: any) => {

        let signUpCtrl = new SignUpController(req, res, next, server);
        signUpCtrl.addUser()
            .then(() => server.sendJson(res, {'status': 'ok'}))
            .catch(next);

    });

}

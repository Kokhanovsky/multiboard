import * as config from 'config';
import { Request, Response } from 'express';
import { SendEmail } from '../../lib/send.email';
import { Server } from '../../server/server';
import { IUser } from '../../models/user';
import User from '../../models/user';
import * as jwt from 'jsonwebtoken';
import { EmailVerification } from '../../lib/email.verification';

let reCAPTCHA = require('recaptcha2');

let recaptcha = new reCAPTCHA({
    siteKey: config.get('recaptchaSiteKey'),
    secretKey: config.get('recaptchaSecretKey')
});

let tpl = require.resolve('./templates/verification.template.ejs');
let tplError = require.resolve('./templates/verification-error.template.ejs');
let tplResend = require.resolve('./templates/verification-resend.template.ejs');

export class VerificationController {

    secret: string = config.get('verificationSecret').toString();
    host: string = config.get('host').toString();
    private cookieName: string = 'verification_token';
    private mail: SendEmail;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.mail = new SendEmail();
    }

    renderVerificationPage() {
        this.res.render(tpl, {
            title: 'Email подтвержден'
        });
    }

    renderVerificationErrorPage() {
        this.res.clearCookie(this.cookieName);
        this.res.render(tplError, {
            title: 'Ошибка активации Email'
        });
    }

    renderVerificationResendPage() {
        this.res.render(tplResend, {
            title: 'Активация Email',
            recaptchaSiteKey: config.get('recaptchaSiteKey')
        });
    }

    doVerification(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.validateToken()
                .then((user: IUser) => {
                    user.verificationToken = '';
                    user.verified = true;
                    this.res.clearCookie(this.cookieName);
                    user.save()
                        .then(() => resolve(true))
                        .catch(reject);
                })
                .catch(reject);
        });
    }

    validateToken(): Promise<IUser> {
        let base64Token = this.req.params.token || this.req.cookies[this.cookieName];
        this.res.cookie(this.cookieName, base64Token);
        let token = new Buffer(base64Token, 'base64').toString();
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.secret, (err: any, decoded: any) => {
                if (err) {
                    return reject(err);
                }
                User.findOne({ email: decoded.email }).then((user: any) => {
                    if (!user) {
                        return reject(new Error('Email не найден'));
                    } else {
                        user.compareVerificationToken(decoded.token).then((res: any) => {
                            if (!res) {
                                return reject(new Error('Ошибка токена'));
                            } else {
                                resolve(user);
                            }
                        }).catch(reject);
                    }
                }).catch(reject);
            });
        });
    }

    resendVerification() {
        let email = this.req.body.email;
        let emailVerify = new EmailVerification();
        return new Promise((resolve, reject) => {
            recaptcha.validate(this.req.body.captcha)
                .then(() => {
                    User.findOne({ email }).then((user: any) => {
                        if (!user) {
                            return reject(new Error('Email не найден'));
                        }
                        if (user.verified) {
                            return reject(new Error('Email уже подтвержден'));
                        }
                        // make verification token
                        emailVerify.verify(user).then(() => {
                            user.save().then(resolve).catch(reject);
                        }).catch(reject);

                    }).catch(reject);
                })
                .catch((errorCodes: any) => reject(new Error('Неверная Captcha')));
        });

    }

}

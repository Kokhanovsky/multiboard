import * as express from "express";
import { Server } from '../../server/server';
import { SignUpController } from './signup.controller';

export default (server: Server) => {

    server.express.post('/api/validate/email', server.authorize.jwt(), (req: express.Request, res: express.Response, next: any) => {

        let signUpCtrl = new SignUpController(req, res, next, server);
        signUpCtrl.validateEmail();

    });

}

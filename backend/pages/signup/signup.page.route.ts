import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { SignUpController } from './signup.controller';


export default (server: Server) => {

    server.express.get('/signup',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let signUpCtrl = new SignUpController(req, res, next, server);
            signUpCtrl.renderPage();

        });

}

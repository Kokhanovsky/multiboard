    import { Request, Response } from "express";
import { Server } from '../../server/server';
import { ItemController } from './item.controller';

export default (server: Server) => {

    server.express.get(/\/([a-z0-9_-]+)\/([a-z0-9\/_-]+)\/([a-z0-9_-]+)_([0-9]+)/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ItemController(req, res, next, server);
            ctrl.renderPage();
        });

    server.express.post('/api/item/contacts',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ItemController(req, res, next, server);
            ctrl.sendContacts();
        });

}

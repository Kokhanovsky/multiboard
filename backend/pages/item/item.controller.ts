import { Request, Response } from 'express';
import { Server } from '../../server/server';
import Item from "../../models/item";
import {IItem} from "../../models/item";
import { Common } from "../../lib/common";
import { ICategory } from "../../models/category";
import { ItemsLib } from '../../lib/items.lib';
let tpl = require.resolve('./item.template.ejs');
let striptags = require('striptags');

interface IImage {
    url: string;
    thumbUrl: string;
    caption?: string;
}

interface IBreadcrumb {
    title: string;
    url: string;
}

interface ILocation {
    title: string;
    url: string;
}

interface IItemCustom extends IItem {
    images: IImage[];
    categories: ICategory[];
    breadcrumbs: IBreadcrumb[];
    locations: ILocation[];
    userProfileUrl: string;
}

export class ItemController {

    locationSlug: string;
    categorySlug: string;
    itemSlug: string;
    itemNumber: number;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.locationSlug = req.params[0];
        this.categorySlug = req.params[1];
        this.itemSlug = req.params[2];
        this.itemNumber = req.params[3];
    }

    renderPage() {
        let item: IItem;

        this.getItem()
        .then((res: IItem) => {
            item = res;
            return ItemsLib.getSimilarItems(item);
        })
        .then((similarItems: IItem[]) => {


            // social share scripts
            let jsScripts: string[] = [
                '<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>',
                '<script src="//yastatic.net/share2/share.js"></script>'
            ];
            // youtube api
            if (item.videos && item.videos.length) {
                jsScripts = jsScripts.concat([
                    '<script src="https://www.youtube.com/iframe_api"></script>',
                ]);
            }
            let metaDescription = Common.truncate(striptags(item.description), 160);
            let metaKeywords = item.tags;
            this.res.render(tpl, {
                item,
                jsScripts,
                metaDescription,
                metaKeywords,
                title: item.title,
                price: Common.thousandSeparator(item.price),
                createdAt: Common.formatCreatedDate(item.createdAt),
                items: similarItems,
                jsVars: [{ items: JSON.stringify(similarItems) }]
            });
        }).catch(err => this.next(err));
    }

    sendContacts() {
        if (!this.req.body && !this.req.body.itemId) {
            return this.next(new Error('Empty request'));
        }
        let itemId = this.req.body.itemId;
        Item.findById(itemId).then((item: IItem) => {
            if (!item) {
                return this.next(new Error('Item not found'));
            }
            this.server.sendJson(this.res, {
                phone: item.phone,
                contactPerson: item.contactPerson
            });
        }).catch(this.next);
    }

    private getItem() {
        let err404: any = new Error();
        err404.status = 404;
        return new Promise((resolve, reject) => {
            Item.findOne({itemNumber: this.itemNumber})
                .populate('category user location.city location.metro location.district location.region')
                .then((item: IItemCustom) => {
                    if (!item) {
                        this.next(err404);
                        return reject();
                    }
                    let locationSlug = item.location.city ? item.location.city.slug : item.location.region.slug;
                    if (!((item.category.slug === this.categorySlug) || (item.category.oldSlug === this.categorySlug)) || (locationSlug !== this.locationSlug)) {
                        this.next(err404);
                        return reject();
                    }
                    if (item.category.slug != this.categorySlug) {
                        this.res.redirect(301, Common.getItemUrl(item));
                    }
                    if (item.slug !== this.itemSlug) {
                        this.res.redirect(Common.getItemUrl(item));
                        return resolve();
                    }
                    if (item.status === 'denied') {
                        this.res.status(404);
                    }
                    item.images = this.getPhotos(item);
                    let categories: ICategory[] = [item.category];
                    (item.category as any).getAncestors((err: any, docs: ICategory[]) => {
                        if (err) {
                            return reject(err);
                        }
                        if (docs && docs.length) {
                            (docs as any).push(item.category);
                            categories = docs;
                        }
                        item.categories = categories;
                        item.breadcrumbs = this.getBreadcrumbs(item);
                        item.locations = this.getLocations(item);
                        item.userProfileUrl = Common.getProfileUrl(item.user);
                        return resolve(item);
                    });
                })
                .catch(reject);
        });
    }

    private getPhotos(item: IItem) {
        if (!item.photos || !item.photos) {
            return [];
        }
        return item.photos.map((photo: any) => {
            return {
                description: photo.description,
                url: "/storage/cache/items/" + item.user.id + "/" + photo.file.toString() + "/1280x720.jpg",
                thumbUrl: "/storage/cache/items/" + item.user.id + "/" + photo.file.toString() + "/100x100.jpg"
            };
        });
    }

    private getBreadcrumbs(item: IItemCustom) {
        let res: IBreadcrumb[] = [];
        let url: string = '/' + (item.location.city ? item.location.city.slug : item.location.region.slug);
        let cityBreadcrumb: IBreadcrumb = {
            title: 'Объявления в ' + (item.location.city ? item.location.city.namePrepositional : item.location.region.namePrepositional),
            url
        };
        res.push(cityBreadcrumb);
        item.categories.forEach((category: ICategory) => {
            res.push({
                title: category.name,
                url: url + '/' + category.slug
            })
        });
        return res;
    }

    private getLocations(item: IItemCustom) {
        let locations: ILocation[] = [];
        if (item.location.region) {
            locations.push({
                title: item.location.region.name,
                url: '/' + item.location.region.slug
            });
        }

        if (item.location.city) {
            locations.push({
                title: item.location.city.name,
                url: '/' + item.location.city.slug
            });
        }

        if (item.location.metro) {
            locations.push({
                title: 'метро ' + item.location.metro.name,
                url: '/' + item.location.city.slug + '/metro/' + item.location.metro.slug
            });
        }

        if (item.location.district) {
            locations.push({
                title: 'район ' + item.location.district.name,
                url: '/' + item.location.city.slug + '/district/' + item.location.district.slug
            });
        }

        return locations;
    }

}

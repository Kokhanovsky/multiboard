import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { LocationLib } from '../../lib/locationLib';
import moment = require('moment');

let tpl = require.resolve('./big-cities.template.ejs');

export class BigCitiesController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        let title = 'Крупные города России - доска бесплатных объявлений - подать объявление бесплатно в городах России.';
        let metaDescription = 'Multibard.ru - бесплатные объявления России. Крупные города России.';
        let metaKeywords = [
            'доска объявлений россия',
            'крупные города россии',
            'доска объявлений'
        ];
        LocationLib.getTopCities(null).then(cities => {
            this.res.render(tpl, {
                title,
                metaDescription,
                metaKeywords,
                cities
            });
        });
    }

}

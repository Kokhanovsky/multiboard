import * as express from 'express';
import { Server } from '../../server/server';
import { BigCitiesController } from './big-cities.controller';
import { LocationLib } from '../../lib/locationLib';

export default (server: Server) => {

    server.express.get('/krupnye_goroda_rossii',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: express.Request, res: express.Response, next: any) => {

            let ctrl = new BigCitiesController(req, res, next, server);
            ctrl.renderPage();

        });

    server.express.get('/api/big-cities',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: express.Request, res: express.Response, next: any) => {

            LocationLib.getTopCities(2000).then(cities => {
                server.sendJson(res, cities);
            }).catch(err => next(err));

        });

}

import { Server } from "../server/server";

let routes = [
    'about/about.route.js',
    'additem/additem.route.js',
    'additem-region/additem-region.route.js',
    'additem/photo.route.js',
    'additem/geocoder.route.js',

    'admin/moderate/moderate.route.js',
    'admin/users/users.route.js',
    'admin/seo-article/seo-article.route.js',

    'api/categories.route.js',
    'api/location.route.js',
    'api/points.route.js',

    'authstatus.route.js',
    'home/home.route.js',
    'home/stat.route.js',
    'big-cities/big-cities.route.js',
    'agreement/agreement.route.js',
    'favorites/favorites.route.js',
    'item/item.route.js',
    'login/login.page.route.js',
    'login/login.route.js',
    'logout/logout.route.js',
    'logout.route.js',

    'sitemap/sitemap.route.js',
    'robots-txt/robots-txt.route.js',

    //'others/cities-geo.route.js',
    'others/update-id-number.route.js',
    //'others/adduser.admin.route.js',
    //'others/adduser.route.js',
    //'others/categories.route.js',
    //'others/regions.route.js',
    //'others/expired.route.js',
    //'others/items-to-users.route.js',
    //'others/item-user.route.js',

    'profile/avatar-upload.route.js',
    'profile/profile.page.route.js',
    'profile/profile.route.js',
    'reset-password/reset-password.page.route.js',
    'reset-password/reset.route.js',
    'reset-password/update-password.page.route.js',
    'reset-password/update-password.route.js',
    'signup/email.validate.route.js',
    'signup/signup.adduser.route.js',
    'signup/signup.page.route.js',
    'signup/verification.route.js',
    'social/facebook.route.js',
    'social/google.route.js',
    'social/instagram.route.js',
    'social/odnoklassniki.route.js',
    'social/vkontakte.route.js',
    'user-items/user-items.route.js',
    'user-page/user-page.route.js',

    // location is important because of routes priority
    'find/find.route.js'
];

export default (server: Server) => {
    routes.forEach((route: string) => require(`${__dirname}/../pages/` + route).default(server));
}

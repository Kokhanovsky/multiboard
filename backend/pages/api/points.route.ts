import * as express from 'express';
import { Server } from '../../server/server';
import { ItemsLib, IParamsModels } from '../../lib/items.lib';
import { FindParams } from '../find/find.params';

export default (server: Server) => {

    server.express.post('/api/points',
        (req: express.Request, res: express.Response, next: any) => {
            let findParams = new FindParams(req);
            findParams.getParamsModels().then((params: IParamsModels) => {
                // req.body.bounds
                return ItemsLib.getMapPoints(params).then((items: any) => {
                    server.sendJson(res, items);
                })
            }).catch(err => next(err));
        });

}

import * as express from "express";
import { Server } from "../../server/server";
import City, { ICity } from "../../models/city";
import Region from "../../models/region";
import Metro, { IMetro } from "../../models/metro";
import District, { IDistrict } from "../../models/district";
import * as _ from "lodash";

export default (server: Server) => {
    server.express.get('/api/cities-top',
        (req: express.Request, res: express.Response, next: any) => {
            City.find({population: {$gt: 300000}}, (err: any, cities: ICity[]) => {
                if (err) {
                    return next(err);
                }
                server.sendJson(res, transformCity(cities));
            }).sort({population: -1}).limit(18).sort({name: 1}).catch(next);
        });

    server.express.get('/api/regions-grouped',
        (req: express.Request, res: express.Response, next: any) => {
            Region.find({}, (err: any, region: any) => {
                if (err) {
                    return next(err);
                }
                let regions = _.groupBy(region, (region: any) => region.name.substr(0, 1));
                server.sendJson(res, regions);
            }).select({'name': 1, 'slug': 1, 'lat': 1, 'lng': 1, 'population': 1}).sort({name: 1}).catch(next);
        });

    server.express.post('/api/cities',
        (req: express.Request, res: express.Response, next: any) => {
            City.find({region: req.body.regionId}, (err: any, cities: ICity[]) => {
                if (err) {
                    return next(err);
                }
                let citiesTransf = transformCity(cities);
                let citiesGrouped = _.groupBy(citiesTransf, (city: any) => city.name.substr(0, 1));
                server.sendJson(res, citiesGrouped);
            }).sort({name: 1}).catch(next);
        });

    server.express.post('/api/metros',
        (req: express.Request, res: express.Response, next: any) => {
            Metro.find({city: req.body.cityId}, (err: any, metros: IMetro[]) => {
                if (err) {
                    return next(err);
                }
                server.sendJson(res, transformMetros(metros));
            }).sort({name: 1}).catch(next);
        });

    server.express.post('/api/districts',
        (req: express.Request, res: express.Response, next: any) => {
            District.find({city: req.body.cityId}, (err: any, districts: IDistrict[]) => {
                if (err) {
                    return next(err);
                }
                server.sendJson(res, transformDistricts(districts));
            }).sort({name: 1}).catch(next);
        });

}

function transformCity(cities: ICity[]) {
    let result: any[] = [];
    cities.forEach((c: ICity) => {
        result.push({
            _id: c._id,
            name: c.name,
            lat: c.lat,
            lng: c.lng,
            population: c.population,
            districts: c.districts.length,
            metros: c.metros.length
        });
    });
    return result;
}
function transformMetros(metros: IMetro[]) {
    let result: any[] = [];
    metros.forEach((m: IMetro) => {
        result.push({
            _id: m._id,
            name: m.name
        });
    });
    return result;
}
function transformDistricts(districts: IDistrict[]) {
    let result: any[] = [];
    districts.forEach((d: IDistrict) => {
        result.push({
            _id: d._id,
            name: d.name
        });
    });
    return result;
}

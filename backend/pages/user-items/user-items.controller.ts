import { Request, Response } from 'express';
import { Server } from '../../server/server';
import Item, { IItem } from '../../models/item';
import * as _ from 'lodash';
import moment = require('moment');

let tpl = require.resolve('./user-items.template.ejs');


export class UserItemsController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        this.getItems().then((items: IItem[]) => {
            this.res.render(tpl, {
                title: 'Мои объявления',
                items
            });
        });

    }

    getItems() {

        return Item.paginate({ 'user': this.req.user.id, 'title': new RegExp(this.req.query.titleFilter, 'i') }, {
            page: this.req.query.page,
            limit: 10,
            lean: true,
            sort: '-createdAt',
            populate: 'category location.city location.metro location.district location.region'
        }).then((res: any) => {
            res = _.extend({'current_date': moment()}, res);
            return res;
        });

    }

    prolong() {
        return new Promise((resolve, reject) => {
            if (!this.req.body || !this.req.body.itemIds || !this.req.body.itemIds.length) {
                return reject(new Error('Error input params'));
            }
            Item.find({user: this.req.user.id, _id: {$in: this.req.body.itemIds}}).then((items: IItem[]) => {
                if (!items) {
                    return reject(new Error('No suitable items'));
                }
                items.forEach((item: IItem) => {
                    if (item.expiredAt < moment().toDate()) {
                        item.upAt = moment().toDate();
                    }
                    item.expiredAt = moment().add(60, 'day').toDate();
                    item.save();
                });
                return resolve();
            });
        })
    }

}

import { Request, Response } from "express";
import { Server } from '../../server/server';
import { UserItemsController } from './user-items.controller';

export default (server: Server) => {

    server.express.get('/user-items',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.renderPage();
        });

    server.express.get('/api/user-items',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.getItems().then((data: any) => {
                server.sendJson(res, data);
            }).catch(next);
        });

    server.express.post('/api/user-items/prolong',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UserItemsController(req, res, next, server);
            ctrl.prolong().then(() => {
                server.sendJson(res, {});
            }).catch(next);
        });

}

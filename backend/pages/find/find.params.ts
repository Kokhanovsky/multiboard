import { Request } from 'express';
import { LocationLib } from '../../lib/locationLib';
import { CategoryLib } from '../../lib/categoryLib';
import { IParamsModels } from '../../lib/items.lib';
import { Common } from '../../lib/common';

interface IParams {
    locations: string[];
    q?: string;
    category?: string;
    p?: number;
    sort?: string;
}

export class FindParams {

    private params: IParams;

    constructor(private req: Request) {

        if (Object.keys(req.body).length) {
            this.params = {
                locations: req.body.locations,
                p: req.body.p ? Math.abs(req.body.p * 1) : 1,
                category: req.body.category ? req.body.category : null,
                q: req.body.q,
                sort: req.body.sort
            };
        }

        if (Object.keys(this.req.params).length || Object.keys(req.query).length) {
            this.params = {
                locations: this.parseLocationQueryParams(),
                p: req.query.p ? Math.abs(req.query.p * 1) : 1,
                q: req.query.q,
                category: Common.trimChar('/', req.params[1]),
                sort: req.query.sort
            };
        }

    }

    getParamsModels(): Promise<any> {
        let params: IParams = this.params;
        let promises: Array<Promise<any>> = [LocationLib.getLocations(params.locations)];
        promises.push(CategoryLib.getCategory(params.category));
        return Promise.all(promises).then(res => {
            let params: IParamsModels = {
                locations: res[0],
                category: res[1],
                p: this.params.p,
                q: this.params.q,
                sort: this.params.sort
            };
            return params;
        });
    }

    private parseLocationQueryParams(): string[] {
        let locations = [this.req.params[0]];
        let locQuery = this.parseUrlQueryParam('locations');
        if (locQuery) locations = locations.concat(locQuery);
        return locations;
    }

    private parseUrlQueryParam(name: string): string[] {
        let param: any = this.req.query[name];
        if (!param) {
            return null;
        }
        if (typeof param === 'string') {
            return [param];
        } else if (param.length) {
            return param;
        }
    }

}

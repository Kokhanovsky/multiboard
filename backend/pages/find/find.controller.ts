import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { ItemsLib, IParamsModels } from '../../lib/items.lib';
import { Common } from '../../lib/common';
import { CategoryLib } from '../../lib/categoryLib';
import { FindParams } from './find.params';
import { FindContent, ISeoContent } from './find.content';
import { LocationLib } from '../../lib/locationLib';
import { ICity } from '../../models/city';
import { IRegion } from '../../models/region';

let tpl = require.resolve('./find.template.ejs');

export class FindController {

    findParams: FindParams;
    categoryLib: CategoryLib;

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.findParams = new FindParams(req);
        this.categoryLib = new CategoryLib(server);
    }

    renderPage() {
        let params: IParamsModels;
        this.findParams.getParamsModels()
        .then((p: IParamsModels) => {
            params = p;
            return Promise.all([
                this.categoryLib.getSubcategories(params),
                ItemsLib.getItemsList(params),
                FindContent.getSeo(params),
                LocationLib.getNearbyCities(params.locations[0], 300, 10, true),
                ItemsLib.getMapPointsCount(params),
                ItemsLib.getItemsList(params)
            ]);
        })
        .then((res: any) => {
            let items = res[1];
            let seo: ISeoContent = ((params.p === 1) || !params.p) && res[2];

            if (!items || !items.docs || !items.docs.length) {
                this.res.status(404);
            }

            let breadcrumbs = FindContent.getBreadcrumbs(params);

            this.res.render(tpl, {
                title: FindContent.getTitle(params, seo),
                titleHead: FindContent.getTitleHead(params, seo),
                metaDescription: FindContent.getMetaDescription(params, seo),
                metaKeywords: FindContent.getMetaKeywords(params, seo),
                breadcrumbs,
                search: FindContent.getSearch(params),
                seo,
                loc: params.locations[0],
                nearByCities: res[3],
                category: params.category,
                subcategories: res[0],
                items,
                mapPointsCount: params.locations.length === 1 ? res[4] : 0,
                Common,
                originalUrl: this.req.originalUrl,
                footer: 'find'
            });

        }).catch(err => {
            let err404: any = new Error();
            err404.status = 404;
            this.next(err404);
        });
    }

    find() {
        return this.findParams.getParamsModels().then(params => {
            return Promise.all([
                this.categoryLib.getSubcategories(params),
                ItemsLib.getItemsList(params),
                FindContent.getSeo(params),
                ItemsLib.getMapPoints(params)
            ]).then(res => {
                return {
                    title: FindContent.getTitle(params, res[2]),
                    titleHead: FindContent.getTitleHead(params, res[2]),
                    breadcrumbs: FindContent.getBreadcrumbsArray(params),
                    subcategories: res[0],
                    items: res[1],
                    mapPoints: res[3],
                    category: params.category && {slug: params.category.slug, name: params.category.name},
                    locations: params.locations && params.locations.map((location: ICity | IRegion) => {
                        return {
                            slug: location.slug,
                            name: location.name,
                            coordinates: location.location.coordinates,
                            population: location.population,
                            namePrepositional: location.namePrepositional
                        }
                    })
                };
            })
        });
    }

}

import { Request, Response } from "express";
import { Server } from '../../server/server';
import { FindController } from './find.controller';

export default (server: Server) => {

    server.express.post('/api/find',
        (req: Request, res: Response, next: any) => {
            let ctrl = new FindController(req, res, next, server);
            ctrl.find().then(data => {
                server.sendJson(res, data)
            }).catch(err => next(err));
        });

    server.express.get(/^\/([a-z0-9_-]+)\/{0,1}$/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new FindController(req, res, next, server);
            ctrl.renderPage();
        });

    server.express.get(/^\/([a-z0-9_-]+)\/([a-z0-9\/_-]+)$/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new FindController(req, res, next, server);
            ctrl.renderPage();
        });


}

import { Request } from 'express';
import { IParamsModels } from '../../lib/items.lib';
import { ICategory } from '../../models/category';
import { Common } from '../../lib/common';
import * as _ from 'lodash';
import { SeoArticleController } from '../admin/seo-article/seo-article.controller';
import Seo, { ISeo } from '../../models/seo';

interface IBreadcrumb {
    title: string;
    url: string;
}

export interface ISeoContent {
    title: string;
    headerTitle: string;
    metaDescription: string;
    metaKeywords: string[];
    content: string;
}

export class FindContent {

    static getSeo(params: IParamsModels): Promise<ISeoContent> {
        return Seo.findOne(SeoArticleController.getFilters(params))
        .select('metaDescription metaKeywords title headerTitle content')
        .then((seoArticle: ISeo)=> {
            return seoArticle;
        });
    }

    static getTitle(params: IParamsModels, seo?: ISeoContent): string {
        if (seo && seo.title) {
            return seo.title;
        }
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locPrep = params.locations.map(location => location.namePrepositional).join(separator);
        let catName = params.category && params.category.name;
        let res = catName ? 'Бесплатные объявления в ' + locPrep + ', ' + catName : 'Бесплатные объявления в ' + locPrep;
        if (params.q) {
            res = _.capitalize(params.q) + ' - ' + res;
        }
        return res;
    }

    static getTitleHead(params: IParamsModels, seo?: ISeoContent): string {
        if (seo && seo.headerTitle) {
            return seo.headerTitle;
        }
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locName = params.locations.map(location => location.name).join(separator);
        let catName = params.category && params.category.name;
        let res = catName ? 'Объявления ' + locName + ' ' + catName : 'Объявления ' + locName;
        if (params.q) {
            res = _.capitalize(params.q) + ' - ' + res;
        }
        return res.replace(/-/g, '&#8209;');
    }

    static getMetaDescription(params: IParamsModels, seo?: ISeoContent): string {
        if (seo && seo.metaDescription) {
            return seo.metaDescription;
        }
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locPrep = params.locations.map(location => location.namePrepositional).join(separator);
        let catName = params.category && params.category.name;
        let res = catName ? 'Бесплатные объявления в ' + locPrep + ' - ' + catName : 'Бесплатные объявления в ' + locPrep;
        if (params.q) {
            res = _.capitalize(params.q) + ' - ' + res;
        }
        return res;
    }

    static getMetaKeywords(params: IParamsModels, seo?: ISeoContent): string[] {
        if (seo && seo.metaKeywords) {
            return seo.metaKeywords;
        }
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locPrep = params.locations.map(location => location.namePrepositional).join(separator);
        let locName = params.locations.map(location => location.name).join(separator);
        let catName = params.category && params.category.name;
        let metaKeywords = [];
        if (params.q) {
            metaKeywords.push(params.q);
        }
        if (catName) {
            metaKeywords.push('объявления в ' + locPrep +
                ' ' + catName);
        }
        metaKeywords.push('бесплатные объявления ' + locName);
        if (catName) {
            metaKeywords.push(catName);
            metaKeywords.push('объявления ' + catName);
        }
        metaKeywords.push('бесплатные объявления в ' + locPrep);
        metaKeywords.push('объявления в ' + locPrep);
        return metaKeywords;
    }

    static getSearch(params) {
        let res = [];
        if (params.q) {
            res.push('q=' + params.q);
        }
        if (params.locations && params.locations.length > 1) {
            res.concat(params.locations.slice(1).map(location => 'location=' + location));
        }
        return res.length ? '?' + res.join('&') : '';
    }

    static getBreadcrumbs(params: IParamsModels): string {
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locPrep = params.locations.map(location => location.namePrepositional).join(separator);
        let res: IBreadcrumb[] = [];
        let q = (params.q ? '?q=' + params.q : '');
        let url = '/' + params.locations[0].slug;
        res.push({
            title: 'Объявления в ' + locPrep,
            url: url + q
        });
        if (params.category) {
            if (params.category.ancestors && params.category.ancestors.length) {
                params.category.ancestors.forEach((category: ICategory) => {
                    res.push({
                        title: category.name,
                        url: url + '/' + category.slug + q
                    })
                });
            } else {
                res.push({
                    title: params.category.name,
                    url: url + '/' + params.category.slug + q
                });
            }
        }
        return res.map(breadcrumb => '<a href="' + breadcrumb.url + '">' + breadcrumb.title + '</a>').join(' / ');
    }

    static getBreadcrumbsArray(params: IParamsModels) {
        let separator = params.locations.length === 2 ? ' и ' : ', ';
        let locPrep = params.locations.map(location => location.namePrepositional).join(separator);
        let res: any[] = [];
        res.push({
            title: 'Объявления в ' + locPrep,
            slug: params.locations[0].slug,
            type: 'city'
        });
        if (params.category) {
            if (params.category.ancestors && params.category.ancestors.length) {
                params.category.ancestors.forEach((category: ICategory) => {
                    res.push({
                        title: category.name,
                        slug: category.slug,
                        type: 'category'
                    })
                });
            } else {
                res.push({
                    title: params.category.name,
                    slug: params.category.slug,
                    type: 'category'
                });
            }
        }
        return res;
    }
}

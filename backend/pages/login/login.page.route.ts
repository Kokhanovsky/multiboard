import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { LoginController } from './login.controller';

export default (server: Server) => {

    server.express.get('/login',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new LoginController(req, res, next, server);
            ctrl.renderPage();

        });

}

import { Request, Response } from 'express';
import { Server } from '../../server/server';

let tpl = require.resolve('./login.template.ejs');

export class LoginController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        this.res.render(tpl, {
            title: 'Вход'
        });
    }

}

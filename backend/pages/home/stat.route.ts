import * as express from "express";
import { Server } from '../../server/server';
import { Stat } from '../../lib/stat.lib';

export default (server: Server) => {

    server.express.post('/api/stat',
        (req: express.Request, res: express.Response, next: any) => {
            let offset = req.body.utcOffset * 1;
            let days = req.body.days;
            if ((Math.abs(offset) > 720) || (days > 90)) {
                return next(new Error('Wrong parameters'));
            }
            Stat.getStat(req.body.utcOffset, req.body.days)
            .then(data => server.sendJson(res, data))
            .catch(err => next(err));
        });

}

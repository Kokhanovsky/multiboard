import { Response } from 'express';
import { LocationLib } from '../../lib/locationLib';
import { ItemsLib, IParamsModels } from '../../lib/items.lib';
import { IRegion } from '../../models/region';
import { ICity } from '../../models/city';
let tpl = require.resolve('./home.template.ejs');

export class HomeController {

    constructor(private res: Response, private next: any) {
    }

    renderPage() {
        let title = 'Доска бесплатных объявлений России. Подать объявление бесплатно о продаже, дать частное объявление на сайте бесплатных online-объявлений. Встроить Youtube видео.';
        let metaDescription = 'Multibard.ru - доска бесплатных объявлений России. Здесь можно подать объявление с фото и видео в любом городе и регионе России.';
        let metaKeywords = [
            'доска объявлений россия',
            'подать объявление',
            'доска объявлений',
            'размещение объявлений',
            'дать объявление',
            'встроить видео',
            'добавить видео на сайт',
            'сайт объявлений',
            'объявления продажа',
            'объявления с фото',
            'объявления с видео',
            'продать',
            'объявления продаю',
            'частные объявления',
            'сайт объявления',
            'поиск объявлений',
            'объявления',
            'бесплатные объявления'
        ];
        let jsScripts: string[] = [
            '<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>',
            '<script src="//yastatic.net/share2/share.js"></script>'
        ];
        let location: ICity | IRegion = this.res.locals.location;
        let params: IParamsModels = {
            locations: location ? [location] : null
        };
        let promises = [
            LocationLib.getTopCities(),
            LocationLib.getTopRegions(),
            ItemsLib.getLastItems(params),
            ItemsLib.getMapPointsCount(params),
            LocationLib.getNearbyCities(location)
        ];
        Promise.all(promises).then((d: any) => {
            this.res.render(tpl, {
                jsScripts,
                title,
                location,
                metaDescription,
                metaKeywords,
                topCities: d[0],
                topRegions: d[1],
                lastItems: d[2],
                mapPointsCount: d[3],
                nearByCities: d[4],
            });
        }).catch(err => this.next(err));
    }

}

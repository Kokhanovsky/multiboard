import { Request, Response } from 'express';
import { Server } from '../../server/server';

let tpl = require.resolve('./about.template.ejs');

export class AboutController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        this.res.render(tpl, {
            title: 'О проекте'
        });
    }

}

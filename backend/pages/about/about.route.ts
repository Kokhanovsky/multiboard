import { Request, Response } from "express";
import { Server } from '../../server/server';
import { AboutController } from './about.controller';

export default (server: Server) => {

    server.express.get('/about',
                        server.authorize.jwt(),
                        server.locals.setLocals(),
                        (req: Request, res: Response, next: any) => {

        let ctrl = new AboutController(req, res, next, server);
        ctrl.renderPage();

    });

}

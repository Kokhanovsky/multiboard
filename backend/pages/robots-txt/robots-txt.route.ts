import { Request, Response } from "express";
import { Server } from '../../server/server';
import * as fs from 'fs-extra';
import * as config from 'config';

// just for local debugging
let robotsLocal = fs.readFileSync(require.resolve('./robots-local.txt')).toString();

// dev robots
let robotsDev = fs.readFileSync(require.resolve('./robots-dev.txt')).toString();

// prod robots
let robotsProd = fs.readFileSync(require.resolve('./robots-prod.txt')).toString();

export default (server: Server) => {
    server.express.get('/robots.txt',
                        server.authorize.jwt(),
                        server.locals.setLocals(),
                        (req: Request, res: Response, next: any) => {
        let env = config.get('env').toString();
        res.header({'Content-Type': 'text/plain; charset=utf-8'});
        switch(env) {
            case 'localhost':
                res.send(robotsLocal);
                break;
            case 'development':
                res.send(robotsDev);
                break;
            case 'production':
                res.send(robotsProd);
                break;
            default:
                res.status(404).send('Not found');
        }
    });
}

import { Request, Response } from 'express';
import { Server } from '../../server/server';
import { ResetPasswordController } from './reset-password.controller';

export default (server: Server) => {

    server.express.get('/reset/password',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ResetPasswordController(req, res, next, server);
            ctrl.renderPage();

        });

}

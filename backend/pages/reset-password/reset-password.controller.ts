import { Request, Response } from 'express';
import { Server } from '../../server/server';
import * as jwt from 'jsonwebtoken';
import * as config from 'config';
import User from '../../models/user';
import { SendEmail } from '../../lib/send.email';
import { IUser } from '../../models/user';
import { Common } from '../../lib/common';

let reCAPTCHA = require('recaptcha2');

let recaptcha = new reCAPTCHA({
    siteKey: config.get('recaptchaSiteKey'),
    secretKey: config.get('recaptchaSecretKey')
});

let tpl = require.resolve('./templates/reset-password.template.ejs');
let tplUpdate = require.resolve('./templates/update-password.template.ejs');
let tplUpdateError = require.resolve('./templates/update-error.template.ejs');
let tplResetEmail = require.resolve('./templates/reset-password.email.template.ejs');
let tplUpdatePasswordEmail = require.resolve('./templates/password-changed.email.template.ejs');

export class ResetPasswordController {

    secret: string = config.get('passwordResetSecret').toString();
    host: string = config.get('host').toString();
    private mail: SendEmail;
    private cookieName: string = 'reset_pass_token';

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
        this.mail = new SendEmail();
    }

    renderPage() {
        this.res.render(tpl, {
            title: 'Сброс пароля',
            recaptchaSiteKey: config.get('recaptchaSiteKey')
        });
    }

    renderUpdatePassPage() {
        this.res.render(tplUpdate, {
            title: 'Измените свой пароль'
        });
    }

    renderUpdateErrorPage() {
        this.res.clearCookie(this.cookieName);
        this.res.render(tplUpdateError, {
            title: 'Ошибка восстановления пароля'
        });
    }

    resetPassword() {
        let email = this.req.body.email;
        return new Promise((resolve, reject) => {
            recaptcha.validate(this.req.body.captcha)
                .then(() => {
                    User.findOne({ email }).then((user: any) => {
                        if (!user) {
                            return reject(new Error('Email не найден'));
                        } else {
                            this.getRestoreUrl(user).then((url: string) => {
                                // send email here
                                this.mail.send(email, 'Восстановление пароля', tplResetEmail, {
                                    user: user.firstname,
                                    url
                                }).then(resolve).catch(reject);

                            }).catch(reject);
                        }
                    }).catch(reject);
                })
                .catch((errorCodes: any) => reject(new Error('Wrong captcha')));
        });

    }

    updatePassword() {
        let password = this.req.body.password;
        return new Promise((resolve, reject) => {
            this.validateToken().then((user: IUser) => {
                user.password = password;
                user.save()
                    .then(() => {
                        // send transactional email
                        this.mail.send(user.email, 'Пароль изменен', tplUpdatePasswordEmail, {
                            user: user.firstname
                        }).then(() => console.log('Password changed email has been sent'));
                        this.res.clearCookie(this.cookieName);
                        return resolve();
                    })
                    .catch(reject);
            }).catch(reject);
        });
    }

    validateToken(): Promise<IUser> {
        let base64Token = this.req.params.token || this.req.cookies[this.cookieName];
        this.res.cookie(this.cookieName, base64Token);
        let token = new Buffer(base64Token, 'base64').toString();
        return new Promise((resolve, reject) => {
            jwt.verify(token, this.secret, (err: any, decoded: any) => {
                if (err) {
                    return reject(err);
                }
                User.findOne({ email: decoded.email }).then((user: any) => {
                    if (!user) {
                        return reject(new Error('Email не найден'));
                    } else {
                        user.compareToken(decoded.token).then((res: any) => {
                            if (!res) {
                                return reject(new Error('Token validation error'));
                            } else {
                                return resolve(user);
                            }
                        }).catch(reject);
                    }
                }).catch(reject);
            });
        });
    }

    private getRestoreUrl(user: IUser): Promise<string> {
        return new Promise((resolve, reject) => {
            Common.makeToken().then((token: string) => {
                user.resetPasswordToken = token;
                user.save().catch(reject);
                let base64 = new Buffer(this.makeJwt(user.email, token)).toString('base64');
                return resolve(this.host + '/reset/password/token/' + base64);
            }).catch(reject);
        });
    }

    private makeJwt(email: string, token: string) {
        let expires = 60 * 60 * 4; // 4 hours
        //let expires = 60;
        return jwt.sign({ email, token }, this.secret, { expiresIn: expires });
    }

}

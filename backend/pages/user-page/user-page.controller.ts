import { Request, Response } from 'express';
import { Server } from '../../server/server';
import User, { IUser } from '../../models/user';
import Item, { IItem } from '../../models/item';
import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { Common } from '../../lib/common';

let tpl = require.resolve('./user-page.template.ejs');

export class UserPageController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage(params: any) {
        this.getUser(params).then((user: IUser) => {
            this.getUserItems(user).then((items: IItem[]) => {
                this.res.render(tpl, {
                    title: [user.firstname, user.lastname].join(' '),
                    userProfile: user,
                    items
                });
            });

        }).catch((err: any) => this.next(err));
    }

    private getUser(params: any) {
        return new Promise((resolve, reject) => {
            let where: any;
            if (params.nickname) {
                where = { nickname: params.nickname };
            }
            if (params.shortId) {
                where = { shortId: params.shortId };
            }
            if (!where) {
                return reject(new Error('Ошибка запроса'));
            }
            User.findOne(where).then((user: IUser) => {
                if (!user) {
                    let error: any = new Error();
                    error.status = 404;
                    error.message = 'Пользователь не найден';
                    return reject(error);
                }
                return resolve(user);
            })
            .catch(reject);
        });
    }

    private getUserItems(user: IUser) {
        let err404: any = new Error();
        err404.status = 404;
        return new Promise((resolve, reject) => {
            let filters: any = { user };

            Item.paginate(filters, {
                page: 1,
                limit: 10,
                lean: true,
                sort: '-createdAt',
                populate: 'category location.city location.metro location.district location.region'
            })

            .then((result: any) => {
                let items: IItem[] = [];
                if (!result || !result.docs.length) {
                    return resolve(null);
                }
                result.docs = result.docs.map((item: IItem) => {
                    return _.extend({
                        url: Common.getItemUrl(item),
                    }, item);
                });
                return resolve(result);
            }).catch(reject);
        })
    }

}

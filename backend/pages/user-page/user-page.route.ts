import { Request, Response } from "express";
import { Server } from '../../server/server';
import { UserPageController } from './user-page.controller';

export default (server: Server) => {

    server.express.get('/user/:nickname([a-z0-9-]+)',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => activate(req, res, next, server));

    server.express.get('/id/:shortId([a-zA-Z0-9-_]+)',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => activate(req, res, next, server));

    function activate(req: Request, res: Response, next: any, server: Server) {
        let params ={
            nickname: req.params.nickname,
            shortId: req.params.shortId
        };
        let ctrl = new UserPageController(req, res, next, server);
        ctrl.renderPage(params);
    }
}

import { Request, Response } from "express";
import { Server } from "../../../server/server";
import { ModerateController } from './moderate.controller';

export default (server: Server) => {

    server.express.get('/adm-service/moderate',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            res.render(require.resolve('./moderate.ejs'), {
                title: 'Модерация',
                layout: 'layout-admin.ejs'
            });

        });

    server.express.get('/api/moderate',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ModerateController(req, res, next, server);
            ctrl.getItems().then((data: any) => {
                server.sendJson(res, data);
            }).catch(next);
        });

    server.express.post('/api/moderate/status',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new ModerateController(req, res, next, server);
            ctrl.setStatus().then((data: any) => {
                server.sendJson(res, {status: 'ok'});
            }).catch(next);
        });

}

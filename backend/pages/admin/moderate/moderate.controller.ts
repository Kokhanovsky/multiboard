import { Request, Response } from 'express';
import { Server } from '../../../server/server';
import Item, { IItem } from '../../../models/item';
import * as _ from 'lodash';
import moment = require('moment');
import { Common } from '../../../lib/common';

export class ModerateController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    getItems() {

        let filters = {
            'title': new RegExp(this.req.query.titleFilter, 'i')
        };

        if (this.req.query.typeFilter !== 'all') {
            filters = _.extend(filters,  {status: this.req.query.typeFilter});
        }

        return Item.paginate(filters, {
            page: this.req.query.page,
            limit: 10,
            lean: true,
            sort: '-createdAt',
            populate: 'category location.city location.metro location.district location.region user'
        }).then((res: any) => {
            if (!res.docs) {
                return null;
            }
            res.docs = res.docs.map((item: any) => {
                item.user = {
                    id: item.user._id,
                    url: Common.getProfileUrl(item.user),
                    avatar: Common.getUserAvatarUrl(item.user),
                    online: item.user.lastActivity && (item.user.lastActivity > moment().add(-15, 'minutes').toDate()),
                    fullname: [item.user.firstname, item.user.lastname].join(' ')
                };
                return item;
            });
            res = _.extend({ 'current_date': moment() }, res);
            return res;
        });

    }

    setStatus() {
        return new Promise((resolve, reject) => {
            if (!this.req.body || !this.req.body.itemId || ! this.req.body.status) {
                return reject(new Error('Empty params'));
            }
            Item.findById(this.req.body.itemId).then((item: IItem) => {
                if (!item) {
                    return reject(new Error('Item not found'));
                }
                item.status = this.req.body.status;
                item.save();
                resolve();
            });
        })
    }

}

import { Request, Response } from "express";
import { Server } from "../../../server/server";
import { SeoArticleController } from './seo-article.controller';

export default (server: Server) => {

    server.express.post('/api/seo-article',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new SeoArticleController(req, res, next, server);
            ctrl.saveArticle().then((data: any) => {
                server.sendJson(res, {status: 'ok'});
            }).catch(next);
        });

    server.express.post('/api/seo-article-get',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new SeoArticleController(req, res, next, server);
            ctrl.getArticle().then((data: any) => {
                server.sendJson(res, data);
            }).catch(next);
        });

}

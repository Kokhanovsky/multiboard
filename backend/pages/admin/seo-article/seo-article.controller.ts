import { Request, Response } from 'express';
import { Server } from '../../../server/server';
import Seo, { ISeo } from '../../../models/seo';
import { FindParams } from '../../find/find.params';
import { IParamsModels } from '../../../lib/items.lib';

export class SeoArticleController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    static getFilters(params: IParamsModels) {
        let filters = {};
        filters['$and'] = [];
        if (params.locations) {
            filters['$and'].push({'params.locations': params.locations.map(loc => loc._id)});
        } else {
            filters['$and'].push({'params.locations': {$exists: false}});
        }
        if (params.category) {
            filters['$and'].push({'params.category': params.category._id});
        } else {
            filters['$and'].push({'params.category': {$exists: false}});
        }
        if (params.q) {
            filters['$and'].push({'params.q': params.q});
        } else {
            filters['$and'].push({'params.q': {$exists: false}});
        }
        return filters;
    }

    getArticle() {
        return new Promise((resolve, reject) => {
            let findParams = new FindParams(this.req);
            findParams.getParamsModels().then(params => {
                Seo.findOne(SeoArticleController.getFilters(params)).then(res => resolve(res));
            }).catch(err => reject(err));
        });
    }

    saveArticle() {
        let reqBody = this.req.body;
        return new Promise((resolve, reject) => {
            let findParams = new FindParams(this.req);
            findParams.getParamsModels().then(params => {
                let p: any = {};
                if (params.locations) {
                    p.locations = params.locations.map(loc => loc._id);
                }
                if (params.category) {
                    p.category = params.category._id;
                }
                if (params.q) {
                    p.q = params.q;
                }
                Seo.findOne(SeoArticleController.getFilters(params)).then((seoArticle: ISeo)=> {
                    if (!seoArticle) {
                        seoArticle = new Seo({
                            title: reqBody.title,
                            headerTitle: reqBody.headerTitle,
                            content: reqBody.content,
                            metaKeywords: reqBody.metaKeywords,
                            metaDescription: reqBody.metaDescription,
                            params: p
                        });
                    } else {
                        seoArticle.title = reqBody.title;
                        seoArticle.headerTitle = reqBody.headerTitle;
                        seoArticle.metaDescription = reqBody.metaDescription;
                        seoArticle.metaKeywords = reqBody.metaKeywords;
                        seoArticle.content = reqBody.content;
                    }
                    seoArticle.save()
                        .then((seo: ISeo) => resolve())
                        .catch(err => reject(err));
                }).catch(err => reject(err));
            }).catch(err => reject(err));
        })
    }

}

import { Request, Response } from 'express';
import { Server } from '../../../server/server';
import * as _ from 'lodash';
import User, { IUser } from '../../../models/user';
import { Common } from '../../../lib/common';
import Item, { IItem } from '../../../models/item';
import moment = require('moment');
import * as mongoose from 'mongoose';
export class UsersController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    getUsers() {
        let page = this.req.query.page ? this.req.query.page : 1;

        let filters: any = {};

        if (this.req.query.searchFilter) {
            let search = new RegExp(this.req.query.searchFilter, 'i');
            filters = _.extend(filters, {
                $or: [
                    { firstname: search },
                    { lastname: search },
                    { nickname: search },
                    { email: search }
                ]
            });
        }

        if (this.req.query.typeFilter !== 'all') {
            filters = _.extend(filters, { role: this.req.query.typeFilter });
        }

        return User.paginate(filters, {
            page,
            limit: 10,
            lean: true,
            sort: '-createdAt',
            populate: 'items'
        }).then((res: any) => {
            if (res.docs && res.docs.length) {
                this.getItemsCount(res.docs);
                res.docs = res.docs.map((user: IUser) => {
                    return {
                        _id: user.id,
                        nickname: user.nickname,
                        fullname: [user.firstname, user.lastname].join(' '),
                        shortId: user.shortId,
                        email: user.email,
                        verified: user.verified,
                        items: {
                            'new': user.items.filter((item: IItem) => item.status === 'new').length,
                            'approved': user.items.filter((item: IItem) => item.status === 'approved').length,
                            'denied': user.items.filter((item: IItem) => item.status === 'denied').length
                        },
                        social: Common.getUserSocial(user),
                        online: user.lastActivity && (user.lastActivity > moment().add(-15, 'minutes').toDate()),
                        hasAvatar: typeof user.avatarBlob !== 'undefined',
                        lastActivity: user.lastActivity,
                        createdAt: user.createdAt
                    };
                });
            }
            return res;
        });
    }

    getItemsCount(users: IUser[]) {
        let ids = users.map((user: IUser) => mongoose.Types.ObjectId(user.id));
        Item.aggregate([
            { $match: { user: { $in: ids } } },
            {
                $group: {
                    _id: '$user',
                    count: { $sum: 1 }
                }
            }
        ]).then((items: IItem[]) => {
            console.log(items);
        });

    }

}

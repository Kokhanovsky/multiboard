import { Request, Response } from "express";
import { Server } from "../../../server/server";
import { UsersController } from './users.controller';

export default (server: Server) => {

    server.express.get('/adm-service/users',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response) => {
            res.render(require.resolve('./users.ejs'), {
                title: 'Пользователи',
                layout: 'layout-admin.ejs'
            });

        });

    server.express.get('/api/admin/users',
        server.authorize.jwt('admin'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new UsersController(req, res, next, server);
            ctrl.getUsers().then((data: any) => {
                server.sendJson(res, data);
            }).catch(next);
        });

}

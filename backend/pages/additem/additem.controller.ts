import { ImageRemover } from '../../lib/imageRemover';
import { Request, Response } from 'express';
import { Server } from '../../server/server';
import Item, { IItem, IPhoto, ILocation } from '../../models/item';
import City, { ICity } from '../../models/city';
import File, { IFile } from '../../models/file';
import * as mongoose from 'mongoose';
import { Common } from '../../lib/common';
import Region, { IRegion } from '../../models/region';
import * as moment from 'moment';
import * as fs from 'fs-extra';
import * as config from 'config';
import * as path from 'path';
import * as del from 'del';

let tpl = require.resolve('./add-item.template.ejs');

export class AddItemController {

    private temporaryDirName: string = config.get('temporaryDirName').toString();

    private storageDir: string = config.get('storageDir').toString();

    constructor(private req: Request,
                private res: Response,
                private next: any,
                private server: Server) {
    }

    renderPage() {
        let title: string;
        let id: number = null;
        let jsScripts = [
            '<script src="https://www.youtube.com/iframe_api"></script>'
        ];
        if (this.req.params && this.req.params.itemNumber) {
            title = 'Редактировать объявление';
            id = this.req.params.itemNumber;
            this.getItem().then(() => {
                this.res.render(tpl, {
                    jsScripts,
                    title,
                    id
                });
            }).catch(this.next);
        } else {
            title = 'Добавить объявление';
            this.res.render(tpl, {
                jsScripts,
                title,
                id
            });
        }
    }

    add() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            this.getLocation().then((location: any) => {
                let item: IItem = new Item({
                    user: this.req.user.id,
                    contactPerson: reqItem.contactPerson,
                    companyName: reqItem.companyName,
                    title: reqItem.title,
                    phone: reqItem.phone,
                    price: reqItem.price,
                    category: reqItem.category,
                    description: reqItem.description,
                    photos: reqItem.photos,
                    videos: reqItem.videos,
                    tags: reqItem.tags,
                    location,
                    upAt: moment().toDate(),
                    expiredAt: moment().add(60, 'day')
                });
                (mongoose as any).Promise = global.Promise;
                item.save().then((item: IItem) => {
                    this.server.telegram.noticeNewItem(item, this.req.user);
                    this.addPhotos(item._id, reqItem.photos).then(() => console.log('Photos added'));
                    Item.populate(item, 'location.city location.region category').then(item => resolve(Common.getItemUrl(item)));
                });
            }).catch(reject);
        })
    }

    edit() {
        let reqItem: any = this.req.body;
        return new Promise((resolve, reject) => {
            this.getLocation().then((location: any) => {
                Item.findOne(this.getFilter(reqItem._id)).then((item: IItem) => {
                    if (!item) {
                        reject();
                        let err: any = new Error('The item not found');
                        err.status = 404;
                        return this.next(err);
                    }
                    item.contactPerson = reqItem.contactPerson;
                    item.companyName = reqItem.companyName;
                    item.title = reqItem.title;
                    item.phone = reqItem.phone;
                    item.price = reqItem.price;
                    item.category = reqItem.category;
                    item.description = reqItem.description;
                    item.photos = reqItem.photos;
                    item.videos = reqItem.videos;
                    item.tags = reqItem.tags;
                    item.location = location;
                    item.save().then((item: IItem) => {
                        let imRemover = new ImageRemover();
                        imRemover.clearRemovedImages(item);
                        this.addPhotos(item._id, reqItem.photos);
                        Item.populate(item, 'location.city location.region category').then(item => {
                            resolve(Common.getItemUrl(item));
                        });
                    });
                });
            });
        });
    }

    deleteItem() {
        let itemId: any = this.req.body.itemId;
        return new Promise((resolve, reject) => {
            Item.findOneAndRemove((this.getFilter(itemId))).then((item: IItem) => {
                if (item) {
                    console.log('The item has been deleted');
                    let imRemover = new ImageRemover();
                    imRemover.deleteFilesByIds(this.req.user.id, item.photos.map((photo: IPhoto) => photo.file.toString()));
                    return resolve();
                } else {
                    return reject(new Error('An item not found'));
                }
            }).catch(reject);
        });
    }

    getItem() {
        let itemNumber = (this.req.query.id || this.req.params.itemNumber);
        return new Promise((resolve, reject) => {
            Item.findOne(this.getFilter(null, itemNumber))
            .populate('category')
            .populate('location.city')
            .populate('location.metro')
            .populate('location.district')
            .populate('location.region')
            .then((item: any) => {
                if (!item) {
                    let error: any = new Error('Item not found');
                    error.status = 404;
                    return reject(error);
                }
                let resItem: any = item.toObject();
                resItem.category = resItem.category._id;
                resItem.photos = resItem.photos.map((photo: any) => {
                    return {
                        item: item.id,
                        file: photo.file,
                        user: item.user,
                        description: photo.description
                    };
                });
                return resolve(resItem);
            })
            .catch(reject);
        });
    }

    private getRegion(loc: any) {
        return new Promise((resolve, reject) => {
            if (loc.city && loc.city._id) {
                City.findOne({ _id: loc.city._id }, (err: any, city: ICity) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(city.region.toString());
                })
            } else if (loc.region && loc.region._id) {
                Region.findOne({ _id: loc.region._id }, (err: any, region: IRegion) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(region._id);
                })
            } else
                reject(new Error('Region not found'));

        });
    }

    private addPhotos(item: mongoose.Schema.Types.ObjectId, photos: any[]) {
        return Promise.all([this.updatePhotosItemId(item, photos), this.movePhotosToUserFolder(photos)]);
    }

    private updatePhotosItemId(item: mongoose.Schema.Types.ObjectId, photos: any[]) {
        if (!photos || !photos.length) {
            return;
        }
        let phs = photos.map((photo: IPhoto) => photo.file);
        File.find({ $and: [ {'_id': { $in: phs } }, { 'user': this.req.user.id } ]}).then((files: IFile[]) => {
            files.forEach((file: IFile) => {
                file.item = item;
                file.save();
            })
        });
    }

    private movePhotosToUserFolder(photos: any[]) {
        let promises: Promise<any>[] = [];
        photos && photos.forEach((photo: IPhoto) => {
            if (photo.user) {
                return;
            }
            promises.push(new Promise((resolve, reject) => {
                let fileName = photo.file + '.jpg';
                let fromFile = path.resolve('./' + path.join(this.storageDir, 'items', this.temporaryDirName, fileName));
                let toDir = path.resolve('./' + path.join(this.storageDir, 'items', this.req.user.id));
                let toFile = path.resolve('./' + path.join(this.storageDir, 'items', this.req.user.id, fileName));
                let cacheDir = path.resolve('./' + path.join(this.storageDir, 'cache', 'items', this.temporaryDirName, photo.file.toString()));
                fs.ensureDir(toDir, err => {
                    if (!err) {
                        fs.rename(fromFile, toFile, err => { if (err) console.log(err) });
                        // clear cache
                        del(cacheDir, err => { if (err) console.log(err) });
                        resolve();
                    } else {
                        reject(err);
                    }
                });
            }));
        });
        return Promise.all(promises);
    }

    private getLocation() {
        let reqItem: any = this.req.body;
        let loc = reqItem.location as any;
        return new Promise((resolve, reject) => {
            this.getRegion(loc).then((region: any) => {
                let location: ILocation = {
                    region,
                    city: loc.city && loc.city._id ? loc.city._id : null,
                    metro: loc.metro && loc.metro._id ? loc.metro._id : null,
                    district: loc.district && loc.district._id ? loc.district._id : null,
                    placemarker: loc.placemarker
                };
                resolve(location);
            }).catch(reject);
        });
    }

    private getFilter(itemId: string, itemNumber?: string) {
        let filter: any = { user: this.req.user.id };
        if (itemId) {
            filter._id = itemId;
        }
        if (itemNumber) {
            filter.itemNumber = itemNumber;
        }
        if (this.req.user && this.req.user.role === 'admin') {
            delete filter.user;
        }
        return filter;
    }

}

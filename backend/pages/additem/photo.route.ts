import { Request, Response } from "express";
import { Server } from "../../server/server";
import * as multer from "multer";
import { PhotoController } from "./photo.controller";
import { ImageRemover } from "../../lib/imageRemover";

export default (server: Server) => {

    let storage = multer.memoryStorage();
    let upload = multer({storage, limits: {fileSize: 1024 * 1024 * 15}});

    server.express.post(
        '/api/additem/photo/upload',
        server.authorize.jwt(),
        upload.array('files', 15),
        (req: Request, res: Response, next: any) => {
            let ctrl = new PhotoController(req, res, next, server);
            ctrl.uploadPhotos()
                .then(files => server.sendJson(res, files))
                .catch(next);
        });

    server.express.delete(
        '/api/additem/photo',
        server.authorize.jwt(),
        (req: Request, res: Response, next: any) => {
            let imRemover = new ImageRemover();
            imRemover.deleteImage(req.body, req.user).then(() => {
                server.sendJson(res, {status: 'ok'});
            }).catch(next);
        });

}

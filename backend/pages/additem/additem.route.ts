import { Request, Response } from "express";
import { Server } from "../../server/server";
import { AddItemController } from "./additem.controller";

export default (server: Server) => {

    server.express.get(['/additem/:itemNumber?', '/board_add'],
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemController(req, res, next, server);
            ctrl.renderPage();
        });

    server.express.post(
        '/api/item',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemController(req, res, next, server);
            ctrl.add().then(url => {
                server.sendJson(res, { url });
            }).catch(next);
        });

    server.express.put(
        '/api/item',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemController(req, res, next, server);
            ctrl.edit().then(url => {
                server.sendJson(res, { url });
            }).catch(next);
        });

    server.express.delete(
        '/api/item',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemController(req, res, next, server);
            ctrl.deleteItem().then(() => {
                server.sendJson(res, { 'status': 'ok' });
            }).catch(next);
        });

    server.express.get('/api/item/edit',
        server.authorize.jwt('user'),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemController(req, res, next, server);
            ctrl.getItem().then((item: any) => {
                server.sendJson(res, item)
            }).catch(next);
        });

}

import { Request, Response } from "express";
import { Server } from "../../server/server";
import File, { IFile } from "../../models/file";
import * as config from "config";
import * as fs from "fs-extra";

let path = require('path');
let sharp = require('sharp');

interface IUploadedFile {
    dir: string;
    buffer: Buffer;
    id: string;
}

export class PhotoController {

    private temporaryDirName: string = config.get('temporaryDirName').toString();

    private storageDir: string = config.get('storageDir').toString();

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    uploadPhotos() {
        let reqFiles: any = this.req.files;
        return new Promise((resolve, reject) => {
            if (!reqFiles.length) {
                return reject(new Error('Empty request'));
            }
            this.getPhotoFileNames(reqFiles).then((files: IUploadedFile[]) => {
                let promises: Promise<boolean>[] = [];
                files.forEach((file: IUploadedFile) => {
                    promises.push(this.processImage(file));
                });
                Promise.all(promises).then(() => {
                    resolve(files.map(file => {
                        return {file: file.id, userId: (this.req.user ? this.req.user.id : null), item: null};
                    }))
                }).catch(reject);
            }).catch(reject);
        });
    }

    private processImage(file: IUploadedFile): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let shrp = sharp(file.buffer);
            let filePath = path.join(file.dir, file.id + '.jpg');
            shrp.resize(1200, 900);
            shrp.max().withoutEnlargement()
                .jpeg({quality: 85})
                .toFile(filePath)
                .then(() => {
                    resolve();
                }).catch(reject);
        });
    }

    // create dir if not exists, save file to db and return array with file names and dir
    private getPhotoFileNames(files: any[]): Promise<IUploadedFile[]> {
        return new Promise((resolve, reject) => {
            let tmp = path.resolve('./' + path.join(this.storageDir, 'items', this.temporaryDirName));
            fs.ensureDir(tmp, err => {
                if (err) {
                    return reject(err);
                }
                let promises: Promise<any>[] = [];
                files.forEach((file: any) => {
                    let fileDB = new File({user: (this.req.user ? this.req.user.id : null), item: null});
                    promises.push(fileDB.save().then((inserted: IFile) => {
                        return {
                            dir: tmp,
                            id: inserted._id,
                            buffer: file.buffer
                        };
                    }).catch(reject));
                });
                Promise.all(promises).then((res: any) => {
                    resolve(res);
                }).catch(reject);
            })
        });
    }

}


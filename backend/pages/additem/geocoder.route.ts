import { Request, Response } from "express";
import { Server } from "../../server/server";
let MultiGeocoder = require('multi-geocoder');

export default (server: Server) => {

    server.express.post(
        '/api/additem/geocoder',
        (req: Request, res: Response, next: any) => {

            let geocoder = new MultiGeocoder({ provider: 'yandex', coordorder: 'latlong' });
            let provider = geocoder.getProvider();
            provider.getText = function (point) {
                return point.address;
            };
            geocoder.geocode([{ address: req.body.query }])
            .then(function (gcode) {
                let coordinates = gcode.result.features[0].geometry.coordinates;
                let text = gcode.result.features[0].properties.metaDataProperty.GeocoderMetaData.Address.formatted;
                server.sendJson(res, {coordinates, text});
            });
        });

}

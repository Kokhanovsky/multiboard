import { Request, Response } from "express";
import { Server } from "../../server/server";
let tpl = require.resolve('./favorites.template.ejs');

export class FavoritesController {

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        let title = 'Избранное';
        this.res.render(tpl, {
            title
        });
    }

}

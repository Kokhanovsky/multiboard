import * as express from "express";
import { Server } from '../../server/server';
import { FavoritesController } from './favorites.controller';

export default (server: Server) => {

    // use server.authorize.jwt() to refresh jwt token and cookies
    // use server.authorize.jwt('user') to check user permissions
    // user server.authorize.jwt('admin') to check admin permissions
    server.express.get('/favorites',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: express.Request, res: express.Response, next: any) => {

            let ctrl = new FavoritesController(req, res, next, server);
            ctrl.renderPage();

        });

}

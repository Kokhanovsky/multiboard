import { Request, Response } from 'express';
import { Server } from '../../server/server';
import User, { IUser } from '../../models/user';
import * as config from 'config';

import passport = require('passport');
let InstagramStrategy = require('passport-instagram').Strategy;

interface IInstagramConfig {
    clientID: string;
    clientSecret: string;
    callbackURL: string;
}

// Routes
export default function (server: Server) {

    let instagramConfig: IInstagramConfig = config.get('instagramAuth') as any;
    let host = config.get('host');

    passport.use(new InstagramStrategy({
            clientID: instagramConfig.clientID,
            clientSecret: instagramConfig.clientSecret,
            callbackURL: host + instagramConfig.callbackURL
        },

        // instagram will send back the token and profile
        (token: string, refreshToken: string, profile: any, done: any) => {

            process.nextTick(() => {

                User.findOne({ instagram: profile.username }).then((user: IUser) => {
                    if (user) {
                        done(null, user)
                    } else {
                        let firstname = profile.username;
                        let lastname = '';
                        let user = new User({
                            email: profile.email,
                            firstname,
                            lastname,
                            role: 'user',
                            instagram: profile.username
                        });
                        user.save((err: any) => {
                            if (err) {
                                done(err);
                            }
                            server.telegram.noticeNewUser(user);
                            done(null, user);
                        });
                    }
                }).catch((err: any) => done(err));

            });
        }));

    server.express.get('/auth/instagram', (req: any, res: any, next: any) => {
        if (req.query.redir) {
            res.cookie('social-redir', req.query.redir);
        }
        next();
    }, passport.authenticate('instagram'));

    server.express.get(instagramConfig.callbackURL,
        passport.authenticate('instagram', { failureRedirect: '/login' }),
        (req: Request, res: Response) => {
            server.authorize.setToken(req.user, req, res);
            let url = req.cookies['social-redir'] ? req.cookies['social-redir'] : '/profile';
            res.clearCookie('social-redir');
            res.redirect(url);
        }
    );

}

import * as express from "express";
import { Server } from '../../server/server';
import Item from "../../models/item";
import { IItem } from '../../models/item';
import * as moment from 'moment';
import User from '../../models/user';
import { IUser } from '../../models/user';

export default (server: Server) => {

    server.express.get('/api/items/items-to-users',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            User.find({}).then((users: IUser[]) => {
                users.map((user: IUser) => {

                    Item.find({userId: user.id}).then((items: IItem[]) => {
                        user.items = items;
                        user.save();
                    });

                });
            })
        });

}

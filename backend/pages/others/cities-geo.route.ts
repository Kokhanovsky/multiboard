import * as express from 'express';
import { Server } from '../../server/server';
import City, { ICity } from '../../models/city';
import Region, { IRegion } from '../../models/region';

export default (server: Server) => {

    server.express.get('/api/cities-geo',
        (req: express.Request, res: express.Response, next: any) => {
            City.find({}).then((cities: ICity[]) => {
                cities.map((city: ICity) => {
                    if (!city.lng) {
                        console.log(city)
                    }
                    city.location.type = 'Point';
                    city.location.coordinates = [city.lng, city.lat];
                    city.save();
                });
                res.json({ 'cities': cities });
            })
        });

    server.express.get('/api/regions-geo',
        (req: express.Request, res: express.Response, next: any) => {
            Region.find({}).then((regions: IRegion[]) => {
                regions.map((region: IRegion) => {
                    if (!region.lng) {
                        console.log(region);
                    }
                    region.location.type = 'Point';
                    region.location.coordinates = [region.lng, region.lat];
                    region.save();
                });
                res.json({ 'regions': regions });
            })
        });

}

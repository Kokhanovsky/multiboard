import * as express from "express";
import { Server } from '../../server/server';
import Region from '../../models/region';
import City from '../../models/city';
import Metro from '../../models/metro';
import District from '../../models/district';
import * as fs from 'fs';
import * as mongoose from 'mongoose';
import IPromise = Q.IPromise;
import {IMetro} from "../../models/metro";
import {IDistrict} from "../../models/district";
import {Common} from "../../lib/common";

function dropCollections() {
    ['regions', 'cities', 'metros', 'districts'].forEach((collection: string) => {
        mongoose.connection.db.dropCollection(collection, (err: any) => {
            if (err) {
                console.log('Something went wrong while deleting ' + collection);
            }
        });
    });
}

function addChildren(items: any[], type: string) {
    return new Promise((resolve, reject) => {
        if (!items || !items.length) {
            resolve([]);
        }
        let promises: IPromise<any>[] = [];
        items.forEach((item: any) => {
            let obj: any;
            if (type === 'District') {
                obj = new District({ name: item.name, slug: Common.translit(item.name) }) as any;
            }
            if (type === 'Metro') {
                obj = new Metro({ name: item.name, slug: Common.translit(item.name) }) as any;
            }
            promises.push(new Promise((resolve, reject) => {
                obj.save((err: any, obj: any) => {
                    if (err) {
                        return reject(err);
                    }
                    return resolve(obj.id);
                })
            }));
        });
        resolve(Promise.all(promises));
    });
}

export default (server: Server) => {

    server.express.get('/api/add/regions',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            /*dropCollections();
            let citiesUniq: any[] = [];
            fs.readFile('./data/regions.json', 'utf-8', (err: any, data: any) => {
                let array: any[] = JSON.parse(data);
                array.forEach((item: any) => {
                    let region = new Region({
                        name: item.name,
                        slug: Common.translit(item.name),
                        namePrepositional: item.namePrepositional,
                        population: item.population,
                        geonameId: item.geonameId,
                        lat: item.lat,
                        lng: item.lng
                    }) as any;
                    region.save((err: any, region: any) => {
                        if (item.children) {
                            item.children.forEach((cityItem: any) => {
                                addChildren(cityItem.metro, 'Metro').then((metros: any[]) => {
                                    addChildren(cityItem.district, 'District').then((districts: any[]) => {
                                        let citySlug = '';
                                        if (citiesUniq.indexOf(cityItem.name) === -1) {
                                            citiesUniq.push(cityItem.name);
                                            citySlug = Common.translit(cityItem.name);
                                        } else {
                                            citySlug = Common.translit(region.name + ' ' + cityItem.name);
                                            console.log(citySlug);
                                        }
                                        let city = new City({
                                            regionId: region.id,
                                            name: cityItem.name,
                                            slug: citySlug,
                                            namePrepositional: cityItem.namePrepositional,
                                            population: cityItem.population,
                                            geonameId: cityItem.geonameId,
                                            lat: cityItem.lat,
                                            lng: cityItem.lng,
                                            districts,
                                            metros
                                        }) as any;

                                        city.save((err: any, city: any) => {
                                            if (err) {
                                                return;
                                            }
                                            let metroIds = metros.map((item) => mongoose.Types.ObjectId(item));
                                            let districtIds = districts.map((item) => mongoose.Types.ObjectId(item));
                                            Metro.find({'_id': {$in: metroIds}}, (err: any, metros: IMetro[]) => {
                                                metros.forEach((metro: IMetro) => {
                                                    metro.city = city._id;
                                                    metro.save();
                                                });
                                            });
                                            District.find({'_id': {$in: districtIds}}, (err: any, districts: IDistrict[]) => {
                                                districts.forEach((district: IDistrict) => {
                                                    district.cityId = city._id;
                                                    district.save();
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        }
                    });
                });
                res.json({status: 'ok'});
            });
*/        });
}

import * as express from 'express';
import { Server } from '../../server/server';
import Item, { IItem, IPhoto } from '../../models/item';
import File, { IFile } from '../../models/file';
import City, { ICity } from '../../models/city';
import District, { IDistrict } from '../../models/district';
import Metro, { IMetro } from '../../models/metro';

export default (server: Server) => {

    server.express.get('/api/item-user',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            Item.find({}).then((items: IItem[]) => {
                items.map((item: IItem) => {
                   // if (!item.userId) {
                        item.user = item.userId as any;
                        //item.userId = undefined;
                    //}
                    if (item.photos && item.photos.length) {
                        item.photos.forEach((photo: IPhoto) => {
                            if (!photo.fileId) {
                                return;
                            }
                            photo.file = photo.fileId as any;
                            photo.fileId = undefined;
                        });
                    }
                    item.save();
                });
            });
            File.find({}).then((files: IFile[]) => {
                files.forEach((file: IFile) => {
                    if (!file.itemId || !file.userId) {
                        return;
                    }
                    file.item = file.itemId;
                    file.itemId = undefined;
                    file.user = file.userId;
                    file.userId = undefined;
                    file.save();
                })
            });
            City.find({}).then((cities: ICity[]) => {
                cities.forEach((city: ICity) => {
                    if (!city.regionId) {
                        return;
                    }
                    city.region = city.regionId;
                    city.regionId = undefined;
                    city.save();
                })
            });
            District.find({}).then((districts: IDistrict[]) => {
                districts.forEach((district: IDistrict) => {
                    if (!district.cityId) {
                        return;
                    }
                    district.city = district.cityId;
                    district.cityId = undefined;
                    district.save();
                })
            });
/*            Metro.find({}).then((metros: IMetro[]) => {
                metros.forEach((metro: IMetro) => {
                    if (!metro.cityId) {
                        return;
                    }
                    metro.city = metro.cityId;
                    metro.cityId = undefined;
                    metro.save();
                })
            });*/
        });

}

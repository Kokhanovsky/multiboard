import * as express from "express";
import { Server } from '../../server/server';
import City from '../../models/city';
import { ICity } from '../../models/city';
import Region from '../../models/region';
import { IRegion } from '../../models/region';
import Category from '../../models/category';
import { ICategory } from '../../models/category';
import District from '../../models/district';
import { IDistrict } from '../../models/district';
import Metro from '../../models/metro';
import { IMetro } from '../../models/metro';

export default (server: Server) => {

    server.express.get('/api/update-id-number',
        (req: express.Request, res: express.Response, next: any) => {
            let promises: Array<Promise<any>> = [];


            // CITY
            City.find({}).then((cities: ICity[]) => {
                let id = 1;
                cities.forEach((city: ICity) => {
                    city.cityNumber = id;
                    city.save().then(() => promises.push(Promise.resolve(true)));
                    id++;
                });
            });

            // REGION
            Region.find({}).then((regions: IRegion[]) => {
                let id = 1;
                regions.forEach((region: IRegion) => {
                    region.regionNumber = id;
                    region.save().then(() => promises.push(Promise.resolve(true)));
                    id++;
                });
            });

            // CATEGORY
            Category.find({}).then((categories: ICategory[]) => {
                let id = 1;
                categories.forEach((category: ICategory) => {
                    category.categoryNumber = id;
                    category.save().then(() => promises.push(Promise.resolve(true)));
                    id++;
                });
            });

            // DISTRICT
            District.find({}).then((districts: IDistrict[]) => {
                let id = 1;
                districts.forEach((district: IDistrict) => {
                    district.districtNumber = id;
                    district.save().then(() => promises.push(Promise.resolve(true)));
                    id++;
                });
            });

            // METRO
            Metro.find({}).then((metros: IMetro[]) => {
                let id = 1;
                metros.forEach((metro: IMetro) => {
                    metro.metroNumber = id;
                    metro.save().then(() => promises.push(Promise.resolve(true)));
                    id++;
                });
            });

            Promise.all(promises).then(() => {
                server.sendJson(res, {ok: true});
            }).catch(err => next(err));

        });

}

import * as express from "express";
import { Server } from '../../server/server';
import Category from '../../models/category';
import * as fs from 'fs';
import * as mongoose from 'mongoose';
import {Common} from "../../lib/common";
import { ICategory } from "../../models/category";

function dropCollections() {
    ['categories'].forEach((collection: string) => {
        mongoose.connection.db.dropCollection(collection, (err: any) => {
            if (err) {
                console.log('Something went wrong while deleting ' + collection);
            }
        });
    });
}

export default (server: Server) => {

    function addCat(data: any, parent: any) {
        data.forEach((item: any) => {
            let slug = Common.translit(item.name);
            let cat = new Category({ name: item.name, slug }) as any;
            if (parent) {
                parent.appendChild(cat, (err: any, data: any) => {
                    cat.save().then(() => {
                        if (item.rubrics) {
                            addCat(item.rubrics, cat);
                        }
                    });
                });
            } else {
                cat.save().then(() => {
                    if (item.rubrics) {
                        addCat(item.rubrics, cat);
                    }
                });
            }
        });
    }

    server.express.get('/api/add/categories',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            /*dropCollections();
            fs.readFile('./data/avito.json', 'utf-8', (err: any, data: any) => {
                let array: any[] = JSON.parse(data);
                addCat(array, null);
                res.json(array);
            });*/
        });

    server.express.get('/api/add/categories/update-slug',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            Category.find({}).then((categories: any) => {
                categories.map((category: any) => {
                    category.getAncestors((err: any, docs: ICategory[]) => {
                        let arr;
                        category.oldSlug = Common.translit(category.name);
                        if (!docs) {
                            category.slug = Common.translit(category.name);
                        } else {
                            arr = docs.map((d: any) => Common.translit(d.name));
                            arr.push(Common.translit(category.name));
                            category.slug = arr.join('/');
                        }
                        category.save();
                    });
                });
                res.json(categories)
            })
        });

    server.express.get('/api/categories/get',
        (req: express.Request, res: express.Response, next: any) => {
            res.json({'status': 'protected'});
            /*Category.GetFullTree((err: any, tree: any) => {
                res.json(tree);
            });*/

        });

}

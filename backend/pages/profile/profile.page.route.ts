import { Request, Response }  from 'express';
import { Server } from '../../server/server';
import { ProfileController } from './profile.controller';

export default (server: Server) => {

    server.express.get('/profile',
        server.authorize.jwt('user'),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {

            let ctrl = new ProfileController(req, res, next, server);
            ctrl.renderPage();

        });

}

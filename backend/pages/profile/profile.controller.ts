import { Request, Response } from 'express';
import { Server } from '../../server/server';
import User from '../../models/user';
import * as _ from 'lodash';
import { IUser } from '../../models/user';
import { EmailVerification } from '../../lib/email.verification';
import { Common } from '../../lib/common';
import * as config from 'config';
import * as del from 'del';

import * as fs from 'fs-extra';

let path = require('path');
let sharp = require('sharp');

let tpl = require.resolve('./profile.template.ejs');

export class ProfileController {

    private storageDir: string = config.get('storageDir').toString();

    constructor(private req: Request, private res: Response, private next: any, private server: Server) {
    }

    renderPage() {
        this.res.render(tpl, {
            title: 'Профиль пользователя'
        });
    }

    getUser() {
        return new Promise((resolve, reject) => {
            if (!this.req.user) {
                return reject(new Error('Can\'t get user'));
            }
            let src: any = this.req.user.toObject();
            let user: any = {
                id: src._id,
                firstname: src.firstname,
                lastname: src.lastname,
                nickname: src.nickname,
                verified: src.verified,
                hasAvatar: src.avatarBlob ? true : false,
                role: src.role,
                host: config.get('host').toString()
            };
            if (this.req.user.email) {
                user.email = this.req.user.email;
            }
            let network = Common.getNetwork(this.req);
            if (network) {
                user.social = network;
            }
            return resolve(user);
        });
    }

    uploadAvatar() {
        let reqFiles: any = this.req.files;
        return new Promise((resolve, reject) => {
            if (!reqFiles.length || !reqFiles[0].buffer) {
                return reject(new Error('Empty request'));
            }
            this.getCurrUser().then((user: IUser) => {
                let shrp = sharp(reqFiles[0].buffer);
                shrp.resize(600, 600);
                this.getAvatarFileName(user).then((filename: string) => {
                    shrp.max().withoutEnlargement().jpeg({quality: 85}).toFile(filename)
                    .then(() => {
                        resolve();
                    }).catch(reject);
                })
                .catch(reject);
            }).catch(reject);
        });
    }

    // save avatar buffer to mongodb
    avatarSave() {
        let avatar = this.req.file.buffer;
        return new Promise((resolve, reject) => {
            sharp(avatar)
            .max()
            .jpeg({quality: 85})
            .toBuffer()
            .then((outputBuffer: Buffer) => {
                this.getCurrUser().then((user: IUser) => {
                    user.avatarBlob = outputBuffer;
                    user.save().then(resolve).catch(reject);
                }).catch(reject);
            })
            .catch(reject);
        });
    }

    avatarDelete() {
        return new Promise((resolve, reject) => {
            this.getCurrUser().then((user: IUser) => {
                User.update({ _id: user._id }, { $unset: { avatarBlob: 1 } })
                .then(() => {
                    this.getAvatarFileName(user).then(filename => {
                        del(filename).then(() => {
                            console.log('User avatar has been deleted');
                        }).catch(reject);
                    }).catch(reject);
                    resolve();
                })
                .catch(reject);
            }).catch(reject);
        });
    }

    putUser(req: Request) {
        return new Promise((resolve, reject) => {
            this.getCurrUser().then((user: IUser) => {
                if (req.body.firstname) {
                    user.firstname = req.body.firstname;
                }
                if (_.isString(req.body.lastname)) {
                    user.lastname = req.body.lastname;
                }
                user.save()
                .then(resolve)
                .catch(reject);
            })
            .catch(reject);
        });
    }

    addNickname(req: Request) {
        return new Promise((resolve, reject) => {

            this.getCurrUser().then((user: IUser) => {
                if (user.nickname) {
                    return reject(new Error('Никнейм уже был выбран'));
                }
                if (req.body.nickname) {
                    let nickname = req.body.nickname.toLowerCase();
                    User.findOne({ nickname }).then((found: IUser) => {
                        if (found) {
                            return reject(new Error('Такой никнейм уже зарегистрирован'));
                        }
                        user.nickname = nickname;
                        user.save()
                        .then(resolve)
                        .catch(reject);
                    }).catch(reject);
                } else {
                    return reject(new Error('Empty request'));
                }
            })
            .catch(reject);
        });
    }

    changePassword(req: Request) {
        return new Promise((resolve, reject) => {
            this.getCurrUser().then((user: IUser) => {
                if (!req.body.currentPassword) {
                    return reject(new Error('Empty password'));
                }
                user.comparePassword(req.body.currentPassword)
                .then((match: boolean) => {
                    if (!match) {
                        return reject(new Error('Wrong current password'));
                    }
                    user.password = req.body.newPassword;
                    user.save()
                    .then(resolve)
                    .catch(reject);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    }

    changeEmail(req: Request) {
        let emailVerify = new EmailVerification();
        return new Promise((resolve, reject) => {
            this.getCurrUser().then((user: IUser) => {
                user.comparePassword(req.body.password)
                .then((match: boolean) => {
                    if (!match) {
                        return reject(new Error('Неверный пароль'));
                    }
                    user.email = req.body.email;
                    emailVerify.verify(user).then(() => {
                        user.save().then(resolve).catch(reject);
                    }).catch(reject);
                })
                .catch(reject);
            })
            .catch(reject);
        });
    }

    private getCurrUser() {
        return new Promise((resolve, reject) => {
            if (!this.req.user) {
                return reject(new Error('Can\'t get user'));
            }
            User.findById(this.req.user._id).then((user: IUser) => {
                if (!user) {
                    return reject(new Error('Can\'t get user from DB'));
                }
                return resolve(user);
            })
            .catch(reject);
        });
    }

    private getAvatarFileName(user: IUser): Promise<string> {
        return new Promise((resolve, reject) => {
            let dir = path.join(this.storageDir, '/user', user._id.toString());
            fs.ensureDir(dir, (err) => {
                if (err) {
                    return reject(err);
                }
                return resolve(path.join(dir, 'avatar.jpg'));
            })
        });
    }
}


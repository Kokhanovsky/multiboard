import { Request, Response } from 'express';
import { Server } from '../server/server';

export default function (server: Server) {

    server.express.get('/api/authstatus',
        server.authorize.jwt('user'),
        (req: Request, res: Response) => {

            server.sendJson(res, { success: true });

        });

}

import * as express from "express";
import { Server } from '../../server/server';
let tpl = require.resolve('./agreement.template.ejs');

export default (server: Server) => {

    server.express.get('/agreement',
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: express.Request, res: express.Response, next: any) => {
            let title = 'Соглашение об использовании сайта';
            res.render(tpl, {
                title
            });
        });

}

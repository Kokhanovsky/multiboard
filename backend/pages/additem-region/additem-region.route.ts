import { Request, Response } from "express";
import { Server } from "../../server/server";
import { AddItemRegionController } from "./additem-region.controller";

export default (server: Server) => {
//
    server.express.get(/^\/podat_obyavlenie(\/([a-z0-9_-]+)(\/[a-z0-9\/_-]+)?)?\/?$/,
        server.authorize.jwt(),
        server.locals.setLocals(),
        (req: Request, res: Response, next: any) => {
            let ctrl = new AddItemRegionController(req, res, next);
            ctrl.renderPage();
        });
}

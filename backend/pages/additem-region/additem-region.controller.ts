import { Request, Response } from 'express';
import { LocationLib } from '../../lib/locationLib';
import { CategoryLib, ICategoryAncestor } from '../../lib/categoryLib';
import { Common } from '../../lib/common';
import * as config from 'config';
import { ICity } from '../../models/city';
import { IRegion } from '../../models/region';
import { ICategory } from '../../models/category';

let tpl = require.resolve('./additem-region.template.ejs');

export class AddItemRegionController {

    locationSlug: string = null;
    categorySlug: string = null;
    widgetId: any;

    constructor(private req: Request,
                private res: Response,
                private next: any) {
        this.locationSlug = req.params[1] ? Common.trimChar('/', req.params[1]) : null;
        this.categorySlug = req.params[2] ? Common.trimChar('/', req.params[2]) : null;
    }

    renderPage() {
        let promises = [];
        if (this.locationSlug) {
            promises.push(LocationLib.getLocations([this.locationSlug]));
        } else {
            promises.push(Promise.resolve(null));
        }
        if (this.categorySlug) {
            promises.push(CategoryLib.getCategory(this.categorySlug));
        } else {
            promises.push(Promise.resolve(null));
        }

        Promise.all(promises).then(res => {
            let locations = res[0];
            let cat = res[1];

            let jsScripts = ['<script src="https://www.youtube.com/iframe_api"></script>'];
            let title = 'Подать объявление';
            let subTitle = 'Подать бесплатное объявление';
            let loc: any = null;
            let metaKeywords = [
                'подать объявление',
                'разместить объявление',
                'частные объявления'
            ];
            if (locations && (locations.length === 1)) {
                title += ' в ' + locations[0].namePrepositional;
                subTitle += ' в ' + locations[0].namePrepositional;
                let locData = {
                    _id: locations[0]._id,
                    name: locations[0].name,
                    lat: locations[0].location && locations[0].location.coordinates && locations[0].location.coordinates[1],
                    lng: locations[0].location && locations[0].location.coordinates && locations[0].location.coordinates[0],
                    population: locations[0].population
                };
                loc = {};
                if (locations[0].kind === 'city') {
                    loc.city = locData;
                } else {
                    loc.region = locData;
                }
            }
            if (cat) {
                title += ' - Multiboard.ru - ' + cat.name;
                subTitle += ' в категорию ' + cat.ancestors.map((category: ICategoryAncestor) => category.name).join(' - ');
            }
            let render = {
                metaKeywords,
                jsScripts,
                title,
                subTitle,
                cat,
                loc,
                recaptchaSiteKey: config.get('recaptchaSiteKey')
            };
            this.res.render(tpl, render);

        }).catch(err => this.next(err))
    }

}

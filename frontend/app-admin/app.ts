import * as angular from "angular";

import 'angular-material';
import 'angular-messages';
import 'angular-cookies';
import 'angular-recaptcha';
import 'angular-toastr';
import 'trix';
import 'angular-trix';
import 'angular-drag-and-drop-lists';
import 'ng-mask';
import 'angular-paging';
import 'angular-chart.js';
import 'chart.js';

import 'ng-idle';
import 'ng-file-upload';
import 'angular-sanitize';
import 'angular-local-storage';
import 'hammerjs';


// import css styles
import '../app/cssmodules/index.sass';
import '../../node_modules/trix/dist/trix.css';

// import components
import componentsModuleName from '../app/components/components';
import admComponentsModuleName from './components/components';

// import all directives
import directivesModuleName from '../app/directives/directives';

// import libs
import libModuleName from '../app/lib/lib';

import { runBlock } from './app.run';

angular.module('app', [
    'ngMaterial',
    'ngMessages',
    'ngIdle',
    'toastr',
    'ngCookies',
    'angularTrix',
    'dndLists',
    'ngMask',
    'bw.paging',
    'ngSanitize',
    'LocalStorageModule',
    'chart.js',
    componentsModuleName,
    admComponentsModuleName,
    directivesModuleName,
    libModuleName])
.run(runBlock);

angular.bootstrap(document, ['app'], {
    strictDi: true
});

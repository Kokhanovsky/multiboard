import * as angular from 'angular';
import userItemsComponentOptions from './moderate.component';
import { ModerateService } from './moderate.service';

let moderateModule: ng.IModule = angular.module('moderate', []);

moderateModule.component('moderate', userItemsComponentOptions);
moderateModule.service('ModerateService', ModerateService);

let moderateModuleName: string = moderateModule.name;

export default moderateModuleName;



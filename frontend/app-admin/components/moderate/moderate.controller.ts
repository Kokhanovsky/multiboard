import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { ModerateService, IItemCustom } from "./moderate.service";
import { CommonService } from '../../../app/lib/common.service';

class ModerateController {

    userItems: IItemCustom[] = [];
    page: number = 1;
    total: number;
    filterOptions: any = [
        {
            title: 'на модерации',
            value: 'new'
        },
        {
            title: 'принятые',
            value: 'approved'
        },
        {
            title: 'отклоненные',
            value: 'denied'
        },
        {
            title: 'все',
            value: 'all'
        }
    ];
    _titleFilter: string;
    _typeFilter: string = 'new';
    perPage: number = 10;
    loading: boolean = true;
    selected: string[] = [];
    _selectAll: boolean = false;
    someChecked: boolean = false;

    set titleFilter(value: string) {
        this._titleFilter = value;
        this.page = 1;
        this.getItems();
    }

    get titleFilter() {
       return this._titleFilter;
    }

    set typeFilter(value: string) {
        this._typeFilter = value;
        this.page = 1;
        this.getItems();
    }

    get typeFilter() {
        return this._typeFilter;
    }

    /** @ngInject */
    constructor(private ModerateService: ModerateService,
                private $rootScope: any,
                private $mdDialog: any) {
        this.getItems();
    }

    getItems() {
        this.$rootScope.loading = true;
        this.ModerateService.getUserItems(this.page, this._titleFilter, this._typeFilter).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.userItems = [];
                return;
            }
            this.userItems = res.items;
            this.highlightFilter();
            this.total = res.total;
        })
        .catch(err => { console.log('There were errors while loading user items', err) })
        .then(() => { this.$rootScope.loading = false })
    }

    getPage() {
        setTimeout(() => {
            this.getItems();
        })
    }

    deleteItem(itemId: string) {
        let confirm = this.$mdDialog.confirm()
            .parent(document.body)
            .clickOutsideToClose(true)
            .title('Удалить объявление?')
            .textContent('Пожалуйста, выберите ответ Да или Нет.')
            .ok('Да')
            .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            this.ModerateService.deleteItem(itemId).then(() => {
                this.userItems = this.userItems.filter((item: IItemCustom) => item.id !== itemId);
            });
        });
    }

    prolong(itemId?: string) {
        let ids = itemId ? [itemId] : this.selected;
        let content =  (ids.length > 1 ? ids.length + ' ' : '') + CommonService.declOfNum(ids.length, [
            'Объявление будет продлено',
            'объявления будут продлены',
            'объявлений будут продлены']) + ' на 60 дней';

        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .textContent(content)
        .ok('Продлить')
        .cancel('Отменить');
        this.$mdDialog.show(confirm).then(() => {
            this.ModerateService.activate(ids).then(() => {

                let alert = this.$mdDialog.alert()
                .parent(document.body)
                .clickOutsideToClose(true)
                .textContent(CommonService.declOfNum(ids.length, [
                        'Срок размещения объявления успешно продлен',
                        'Сроки размещения объявлений успешно продлены',
                        'Сроки размещения объявлений успешно продлено']))
                .ok('Ok');
                this.$mdDialog.show(alert);

                this.getPage();
            }).catch(() => {
                alert('Возникла ошибка при продлении объявлений, повторите позже');
            });
        });
    }

    private highlightFilter(): IItemCustom[] {
        if (!this._titleFilter) {
            return this.userItems;
        }
        let res = angular.copy(this.userItems);
        this.userItems = res.map((item: IItemCustom) => {
                item.title = item.title.replace(new RegExp('('+this._titleFilter+')', 'gi'),
                    '<span class="highlighted">$1</span>');
            return item;
        });
    }

}

export default ModerateController;

import * as angular from 'angular';
import { CommonService } from '../../../app/lib/common.service';
import { IItem } from '../../../../backend/models/item';
import { ICategory } from '../../../../backend/models/category';
import { ICity } from '../../../../backend/models/city';
import { IRegion } from '../../../../backend/models/region';
import { IUser } from '../../../../backend/models/user';

import IPromise = angular.IPromise;

export interface IItemCustom {
    id: string;
    title: string;
    status: string;
    region: IRegion;
    category: ICategory;
    city: ICity;
    description: string;
    url: string;
    price: string;
    selected?: boolean;
    itemNumber: number;
    user: any;
    photo: string;
    createdAt: string;
}

interface IItemUser extends IItem {
    user: IUser;
}
export class ModerateService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getUserItems(page: number, titleFilter: string, typeFilter: string): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/moderate',
                params: { page, titleFilter, typeFilter }
            })
            .then((response: any): any => {
                if (!response.data.docs || !response.data.docs.length) {
                    return null;
                }
                let items = response.data.docs.map((item: IItemUser) => {
                    let res = {
                        id: item._id,
                        title: item.title,
                        region: item.location.region,
                        category: item.category,
                        city: item.location.city,
                        description: item.description,
                        status: item.status,
                        itemNumber: item.itemNumber,
                        user: item.user,
                        price: CommonService.thousandSeparator(item.price),
                        url: CommonService.getItemUrl(item),
                        createdAt: CommonService.formatToHuman(item.createdAt),
                        photosCount: item.photos && item.photos.length,
                        videosCount: item.videos && item.videos.length,
                        photos: CommonService.getItemThumbnails(item.photos, item.videos, item.user.id),
                        tags: item.tags
                    };
                    return res;
                });
                let total = response.data.total;
                return {items, total};
            });
    }

    deleteItem(itemId: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/item',
                data: { itemId }
            });
    }

    activate(itemIds: string[]) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/user-items/prolong',
                data: { itemIds }
            });
    }

}

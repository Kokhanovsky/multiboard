import ModerateController from './moderate.controller';
import './moderate.sass';

let moderateComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./moderate.html'),
    controller:  ModerateController
};

export default moderateComponent;

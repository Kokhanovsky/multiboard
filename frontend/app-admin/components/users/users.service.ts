import * as angular from 'angular';
import { CommonService } from '../../../app/lib/common.service';

import IPromise = angular.IPromise;

export interface IUserCustom {
    id: string;
    fullname: string;
    nickname: string;
    social: string;
    email: string;
    verified: string;
    url: string;
    hasAvatar: string;
    createdAt: string;
    online: boolean;
    items: number[];
    lastActivity: string;
}

export class UsersService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getUsers(page: number, typeFilter: string, searchFilter: string): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/admin/users',
                params: { page, typeFilter, searchFilter }
            })
            .then((response: any): any => {
                if (!response.data.docs || !response.data.docs.length) {
                    return null;
                }
                let items = response.data.docs.map((user: any) => {
                    let res: IUserCustom = {
                        id: user._id,
                        fullname: user.fullname,
                        nickname: user.nickname,
                        hasAvatar: user.hasAvatar,
                        email: user.email,
                        online: user.online,
                        social: user.social,
                        verified: user.verified,
                        items: user.items,
                        url: CommonService.getProfileUrl(user),
                        createdAt: CommonService.formatToHuman(user.createdAt),
                        lastActivity: user.lastActivity ? CommonService.formatToHuman(user.lastActivity) : null
                    };
                    return res;
                });
                let total = response.data.total;
                return {items, total};
            });
    }

}

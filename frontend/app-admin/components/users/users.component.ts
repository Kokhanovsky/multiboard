import UsersController from './users.controller';
import './users.sass';

let usersComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./users.html'),
    controller:  UsersController
};

export default usersComponent;

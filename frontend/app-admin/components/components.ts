import * as angular from "angular";
import categoryModuleName from '../../app/components/category/category';
import moderateModuleName from '../components/moderate/moderate';
import statusModuleName from '../../app/components/status/status';
import filtersButtonsModuleName from '../components/filter-buttons/filter-buttons';
import usersModuleName from '../components/users/users';
import itemImageModuleName from '../../app/components/item-image/item-image';

let componentsModule: ng.IModule = angular.module('app.components', [
    categoryModuleName,
    moderateModuleName,
    statusModuleName,
    filtersButtonsModuleName,
    itemImageModuleName,
    usersModuleName
]);

let componentsModuleName: string = componentsModule.name;

export default componentsModuleName;

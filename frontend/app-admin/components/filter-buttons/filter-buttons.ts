import * as angular from 'angular';

import filterButtonsComponentOptions from './filter-buttons.component';

let filterButtonsModule: ng.IModule = angular.module('filterButtons', []);

filterButtonsModule.component('filterButtons', filterButtonsComponentOptions);

let filterButtonsModuleName: string = filterButtonsModule.name;

export default filterButtonsModuleName;



import filterButtonsController from './filter-buttons.controller';
import './filter-buttons.sass';

let filterButtonsComponent: ng.IComponentOptions = {
    bindings: {
        value: '=',
        options: '<'
    },
    template: require('./filter-buttons.html'),
    controller:  filterButtonsController
};

export default filterButtonsComponent;

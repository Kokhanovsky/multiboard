import * as angular from "angular";
import IHttpService = angular.IHttpService;

class FilterButtonsController {

    value: string;
    options: any[];

    /** @ngInject */
    constructor() {

    }

    $onInit() {
        if (!this.value) {
            this.value = this.options[0].value;
        }
    }

    setFilter(val: string) {
        this.value = val;
    }


}

export default FilterButtonsController;

import * as angular from "angular";
import * as moment from "moment";

import 'angular-material';
import 'angular-messages';
import 'angular-cookies';
import 'angular-recaptcha';
import 'angular-toastr';
import 'trix';
import 'angular-trix';
import 'angular-drag-and-drop-lists';
import 'ng-mask';
import 'angular-paging';
import 'angular-youtube-embed'
import 'ng-idle';
import 'ng-file-upload';
import 'angular-sanitize';
import 'angular-local-storage';
import 'angular-chart.js';
import 'chart.js';
import 'angular-scroll';


import 'angular-yandex-map/ya-map-2.1.min';
import 'ng-img-crop/compile/minified/ng-img-crop.js';

import 'hammerjs';


// import css styles
import './cssmodules/index.sass';
import '../../node_modules/ng-img-crop/source/scss/ng-img-crop.scss';
import '../../node_modules/trix/dist/trix.css';

// import all components
import componentsModuleName from './components/components';

// import all directives
import directivesModuleName from './directives/directives';

// import libs
import libModuleName from './lib/lib';

import { runBlock } from './app.run';

moment.locale("ru");

angular.module('app', [
    'ngMaterial',
    'ngMessages',
    'ngIdle',
    'vcRecaptcha',
    'toastr',
    'ngCookies',
    'ngFileUpload',
    'ngImgCrop',
    'angularTrix',
    'dndLists',
    'ngMask',
    'bw.paging',
    'ngSanitize',
    'ngSanitize',
    'youtube-embed',
    'yaMap',
    'thatisuday.ng-image-gallery',
    'LocalStorageModule',
    'duScroll',
    'chart.js',
    componentsModuleName,
    directivesModuleName,
    libModuleName])
.run(runBlock);

angular.bootstrap(document, ['app'], {
    strictDi: true
});

import IPromise = angular.IPromise;

export class AvatarUploadService {

    /** @ngInject */
    constructor(private Upload: any) {
    }

    upload(files: Array<angular.angularFileUpload.IFileUploadConfigFile>): angular.angularFileUpload.IUploadPromise<any> {
        return this.Upload.upload({
            url: '/api/user/avatar/upload',
            arrayKey: '',
            data: {
                files
            },
            method: 'POST'
        });
    }

    uploadAvatar(file: string): angular.angularFileUpload.IUploadPromise<any> {
        return this.Upload.upload({
            url: '/api/user/avatar/save',
            arrayKey: '',
            data: {
                avatar: this.Upload.dataUrltoBlob(file)
            },
            method: 'POST'
        });
    }
}

import SignUpController from './signup.controller';
import './signup.sass';

let signUpComponent: ng.IComponentOptions = {
    bindings: {
        recaptchaSiteKey: '<',
        showComponent: '='
    },
    template: require('./signup.html'),
    controller:  SignUpController
};

export default signUpComponent;

import * as angular from "angular";
import signUpComponentOptions from './signup.component';
import { SignUpService } from './signup.service';

let signUpModule: ng.IModule = angular.module('signUpModule', []);

signUpModule.component('stSignUp', signUpComponentOptions);
signUpModule.service('SignUpService', SignUpService);

let signUpModuleName: string = signUpModule.name;

export default signUpModuleName;

import { FindService } from '../find/find.service';

interface ISubcategory {
    name: string;
    slug: string;
    counts: number;
}

class SearchFiltersController {

    focus: boolean = false;
    query: string;
    searchFilterShow: boolean;
    subcategories: ISubcategory[];

    /** @ngInject */
    constructor(private FindService: FindService,
                private $location: ng.ILocationService,
                private $rootScope: ng.IRootScopeService) {
        let search = this.$location.search();
        this.query = (search && search.q) ? search.q : '';
        this.FindService.findParams.q = this.query;
    }

    $onInit() {
        this.$rootScope.$on('cardsLoaded', (event: any, result: any) => {
            this.subcategories = this.FindService.subcategories;
            this.searchFilterShow = this.subcategories && this.subcategories.length > 0;
        });
    }

    setCategory(slug: string) {
        this.FindService.findParams.category = slug;
        delete this.FindService.findParams.p;
        this.FindService.buildUrl();
    }



}

export default SearchFiltersController;

import * as angular from "angular";

import searchFiltersComponentOptions from './search-filters.component';

let searchFiltersModule: ng.IModule = angular.module('searchFiltersModule', []);

searchFiltersModule.component('searchFilters', searchFiltersComponentOptions);

let searchFiltersModuleName: string = searchFiltersModule.name;

export default searchFiltersModuleName;



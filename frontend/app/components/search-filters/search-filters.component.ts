import SearchFiltersController from './search-filters.controller';
import './search-filters.sass';

let searchFiltersComponent: angular.IComponentOptions = {
    bindings: {
        searchFilterShow: '='
    },
    template: require('./search-filters.html'),
    controller:  SearchFiltersController
};

export default searchFiltersComponent;

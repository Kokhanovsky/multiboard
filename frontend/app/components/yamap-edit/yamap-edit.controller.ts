import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { YamapEditService } from './yamap-edit.service';
import IScope = angular.IScope;

class yamapEditController {

    public model: ng.INgModelController;
    address: string;
    yamapEditLocation: any;
    mapCenter: number[] = [37.64, 55.76];
    markerInfo: string;
    zoom: number = 11;
    map: any;
    ymaps: any;
    placemark: any;

    /** @ngInject */
    constructor(private YamapEditService: YamapEditService, private $scope: IScope) {

    }

    afterMapInit(map: any) {
        this.ymaps = window["ymaps"];
        this.map = map;
        this.map.behaviors.disable('scrollZoom');
        this.$scope.$watch(() => this.yamapEditLocation, val => {
            if (!val) {
                this.mapCenter = [
                    37.64,
                    55.76
                ]
            } else if (!val.placemarker) {
                let zoom = 8;
                if (!val.city && val.region) {
                    this.mapCenter = [val.region.lng, val.region.lat];
                    this.map.setZoom(zoom);
                } else {
                    this.mapCenter = [val.city.lng, val.city.lat];
                    zoom = 11;
                    if (val.city.population < 500000)
                        zoom = 12;
                    if (val.city.population < 150000)
                        zoom = 13;
                    if (val.city.population < 20000)
                        zoom = 14;


                }
                this.map.setZoom(zoom);
            } else {
                this.createPlacemark([val.placemarker.lat, val.placemarker.lng]);
                this.mapCenter = [val.placemarker.lat, val.placemarker.lng];
                this.markerInfo = val.placemarker.caption;
                this.map.setZoom(16);
            }
        });
    }

    search() {
        let searchLine = [];
        searchLine.push('Россия');
        if (this.yamapEditLocation.region) {
            searchLine.push(this.yamapEditLocation.region.name);
        }
        if (this.yamapEditLocation.city) {
            searchLine.push(this.yamapEditLocation.city.name);
        }
        if (this.address) {
            searchLine.push(this.address);
        }
        this.YamapEditService.search(searchLine.join(', ')).then(res => {
            this.mapCenter = [res.coordinates[1], res.coordinates[0]];
            this.map.setZoom(16);
            this.createPlacemark(this.mapCenter);
            this.markerInfo = res.text;
            this.setValue(this.mapCenter, res.text);
        })
    }

    removePlacemark() {
        if (this.placemark) {
            this.map.geoObjects.remove(this.placemark);
            this.markerInfo = '';
        }
        this.setValue(null);
    }

    returnToMarker() {
        this.map.setCenter(this.placemark.geometry.getCoordinates());
        this.map.setZoom(16);
    }

    mapClick($event: any) {
        let coords = $event.get('coords');
        this.createPlacemark(coords);
        this.setCaptionCoords(coords);
        this.setValue(coords);
    }

    private createPlacemark(coords: number[]) {
        if (this.placemark) {
            this.map.geoObjects.remove(this.placemark);
        }
        this.placemark = new this.ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: coords
                },
                properties: {
                    //iconContent: 'Метка',
                    balloonContent: 'Метку можно переместить'
                }
            },
            {
                draggable: true
            });

        // execute on placemark change position (drag)
        this.placemark.events.add("dragend", (event: any) => {
            let coords = this.placemark.geometry.getCoordinates();
            this.setCaptionCoords(coords);
            this.setValue(coords);
        });

        this.map.geoObjects.add(this.placemark);

    }

    private setCaptionCoords(coords: number[]) {
        this.markerInfo = 'Координаты метки: ' + coords[0].toFixed(4) + ', ' + coords[1].toFixed(4);
        return this.markerInfo;
    }

    private setValue(coords: number[], caption?: string) {
        if (!coords) {
            this.model.$setViewValue(null);
        } else {
            this.model.$setViewValue({
                lat: coords[0],
                lng: coords[1],
                caption
            });
        }
    }

}

export default yamapEditController;

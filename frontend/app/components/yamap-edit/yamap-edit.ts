import * as angular from "angular";
import yamapEditComponentOptions from './yamap-edit.component';
import { YamapEditService } from './yamap-edit.service';

let yamapEditModule: ng.IModule = angular.module('yamapEditModule', []);

yamapEditModule.component('yamapEdit', yamapEditComponentOptions);
yamapEditModule.service('YamapEditService', YamapEditService);

let yamapEditModuleName: string = yamapEditModule.name;

export default yamapEditModuleName;



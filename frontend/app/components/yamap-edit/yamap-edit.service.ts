import IPromise = angular.IPromise;

export class YamapEditService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    search(query) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/additem/geocoder',
                data: { query }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

}

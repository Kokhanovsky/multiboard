import YamapEditController from './yamap-edit.controller';
import './yamap-edit.sass';
let yamapEditComponent: ng.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    bindings: {
        yamapEditLocation: '<'
    },
    template: require('./yamap-edit.html'),
    controller:  YamapEditController
};

export default yamapEditComponent;

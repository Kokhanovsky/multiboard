import StatusController from './status.controller';
import './status.sass';

let statusComponent: ng.IComponentOptions = {
    bindings: {
        itemId: '<',
        showCaption: '<',
        confirm: '<',
        value: '='
    },
    template: require('./status.html'),
    controller:  StatusController
};

export default statusComponent;

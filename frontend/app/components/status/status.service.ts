import * as angular from 'angular';

import IPromise = angular.IPromise;


export class StatusService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }


    setStatus(itemId: string, status: string) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/moderate/status',
                data: { itemId, status }
            });
    }

}

import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { StatusService } from "./status.service";

class StatusController {

    itemId: string;
    value: string;
    confirm: boolean;
    showCaption: boolean;

    /** @ngInject */
    constructor(private StatusService: StatusService, private $mdDialog) {
    }

    setStatus(val: string) {
        if (val === this.value) {
            return;
        }
        if (this.confirm) {
            let confirm = this.$mdDialog.confirm()
            .parent(document.body)
            .clickOutsideToClose(true)
            .title('Подтвердите смену статуса')
            .textContent('Пожалуйста, выберите ответ Да или Нет.')
            .ok('Да')
            .cancel('Нет');
            this.$mdDialog.show(confirm).then(() => {
                this.doStatusChange(val);
            }, () => {
                console.log('Cancel');
            });
        } else {
            this.doStatusChange(val);
        }
    }

    doStatusChange(val: string) {
        let oldVal = this.value;
        this.value = val;
        this.StatusService.setStatus(this.itemId, val).then(() => {
            console.log('success');
        }).catch(() => this.value = oldVal)

    }

}

export default StatusController;

import * as angular from "angular";
import meterComponentOptions from './meter.component';

let meterModule: ng.IModule = angular.module('meterModule', []);

meterModule.component('meter', meterComponentOptions);

let meterModuleName: string = meterModule.name;

export default meterModuleName;



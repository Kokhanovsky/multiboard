import MeterController from './meter.controller';
import './meter.sass';

let meterComponent: ng.IComponentOptions = {
    bindings: {
    },
    template: require('./meter.html'),
    controller:  MeterController
};

export default meterComponent;

import * as angular from 'angular';
import {VideoEditService} from './video-edit.service';

interface IYoutubeItem {
    title: string,
    youtubeId: string
}

class VideoEditController {

    model: ng.INgModelController;
    maxItems: number;
    youtubeItems: IYoutubeItem[] = [];

    /** @ngInject */
    constructor(private $mdDialog: any, private $scope: ng.IScope) {
        $scope.$watch(() => this.model.$viewValue, (val: IYoutubeItem[]) => {
            if (val) {
                this.youtubeItems = val;
            }
        }, true);
        $scope.$watch(() => this.youtubeItems, (val: IYoutubeItem[]) => {
            this.model.$setViewValue(val);
        }, true);
    }

    remove(ytItem: IYoutubeItem) {
        this.youtubeItems = this.youtubeItems.filter((yt: IYoutubeItem) => yt.youtubeId !== ytItem.youtubeId);
    }

    showAdd() {
        this.$mdDialog.show({
            controller: AddVideoController,
            controllerAs: '$ctrl',
            template: require('./templates/video-add-popup.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((youtubeItem: IYoutubeItem) => {
            if (this.youtubeItems.map((ytItem: IYoutubeItem) => ytItem.youtubeId).indexOf(youtubeItem.youtubeId) === -1) {
                this.youtubeItems.push(youtubeItem);
            }
        }, () => {
            console.log('cancel');
        });
    }

}

class AddVideoController {

    _youtubeUrl: string;
    serverError: string;
    youtubeUrlInput: any;
    loading: boolean = false;
    success: boolean = false;

    set youtubeUrl(value: string) {
        if (this.youtubeUrlInput) {
            this.youtubeUrlInput.$setValidity("wrongUrl", true);
        }
        this._youtubeUrl = value;
    }

    get youtubeUrl() {
        return this._youtubeUrl;
    }


    /** @ngInject */
    constructor(private $mdDialog: any,
                private youtubeEmbedUtils: any,
                private VideoEditService: VideoEditService) {
    }

    add(youtubeUrlInput: any) {
        this.youtubeUrlInput = youtubeUrlInput;
        let youtubeId = this.youtubeEmbedUtils.getIdFromURL(this.youtubeUrl);
        if (youtubeId === this.youtubeUrl) {
            this.youtubeUrlInput.$setValidity("wrongUrl", false);
            return;
        }
        this.VideoEditService.getTitle(youtubeId).then((title: any) => {
            this.$mdDialog.hide({
                youtubeId,
                title
            });
        })
    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default VideoEditController;

import VideoEditController from './video-edit.controller';
import './video-edit.sass';

let VideoEditComponent: ng.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    bindings: {
        maxItems: '<'
    },
    template: require('./video-edit.html'),
    controller:  VideoEditController
};

export default VideoEditComponent;

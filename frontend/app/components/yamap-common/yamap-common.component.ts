import YamapCommonController from './yamap-common.controller';
import './yamap-common.sass';
let yamapCommonComponent: ng.IComponentOptions = {
    bindings: {
        yamapCommonCenter: '<',
        yamapCommonPopulation: '<',
        yamapCommonPoints: '<'
    },
    template: require('./yamap-common.html'),
    controller:  YamapCommonController
};

export default yamapCommonComponent;

import * as angular from "angular";
import yamapCommonComponentOptions from './yamap-common.component';
import { YamapCommonService } from './yamap-common.service';

let yamapCommonModule: ng.IModule = angular.module('yamapCommonModule', []);

yamapCommonModule.component('yamapCommon', yamapCommonComponentOptions);
yamapCommonModule.service('YamapCommonService', YamapCommonService);

let yamapCommonModuleName: string = yamapCommonModule.name;

export default yamapCommonModuleName;



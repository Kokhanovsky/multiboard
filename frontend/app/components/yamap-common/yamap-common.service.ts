import IPromise = angular.IPromise;

export class YamapCommonService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getPointsByCity(city: string) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/points/',
                data: { locations: [city] }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

}

import * as angular from "angular";
import IHttpService = angular.IHttpService;
//import { YamapCommonService } from './yamap-common.service';
import { CommonService } from '../../lib/common.service';
import IScope = angular.IScope;

class yamapCommonController {

    yamapCommonCenter: number[];
    yamapCommonPopulation: number;
    yamapCommonPoints: any[];
    zoom: number = 11;
    map: any;
    ymaps: any;
    placemarkCollection: any;

    /** @ngInject */
    constructor(private $rootScope: ng.IRootScopeService) {
        $rootScope.$watch(() => this.yamapCommonPoints, () => {
            if (this.map) {
                this.placemarkCollection.removeAll();
                this.fillMap(this.map);
            }
        }, true);
    }

    afterMapInit(map: any) {
        this.fillMap(map);
    }

    private fillMap(map: any) {
        this.ymaps = window["ymaps"];
        this.map = map;
        this.map.behaviors.disable('scrollZoom');
        this.map.setCenter(this.yamapCommonCenter);
        let zoom = 10;
        let ppl = this.yamapCommonPopulation;
        if (ppl < 2000000)
            zoom = 11;
        if (ppl < 500000)
            zoom = 12;
        if (ppl < 150000)
            zoom = 13;
        if (ppl < 20000)
            zoom = 14;
        this.map.setZoom(zoom);

        this.placemarkCollection = new this.ymaps.GeoObjectCollection({}, {
            preset: "islands#blueCircleIcon"
        });

        this.map.geoObjects.add(this.placemarkCollection);

        // bounds change event
        this.map.events.add('boundschange', (event: any) => {
            //this.getPoints();
        });

        this.createPlacemarks(this.yamapCommonPoints);

    }

    private createPlacemarks(data: any[]) {
        data && data.forEach((item: any) => {
            let coordinates = [item.placemarker.lat, item.placemarker.lng];

            let placemarker = new this.ymaps.Placemark(coordinates, {

            }, {
                iconLayout: 'default#image',
                iconImageHref: '/images/placemark.png',
                iconImageSize: [20, 20],
                iconImageOffset: [-10, -10]
            });

            placemarker.events
            .add('click', e => {
                let address = item.placemarker.caption ? '<div class="address">' + item.placemarker.caption + '</div>' : '';
                let photo = item.photo ? '<div class="photo"><a href="' + item.url + '" target="_self"><img src="' + item.photo +'"/></a></div>' : '';
                let price = item.price ? ' <span class="price">' + CommonService.thousandSeparator(item.price) + ' р.</span>' : '';
                this.map.balloon.open(coordinates, {
                    content: '<div class="baloon-content">' +
                    '<div class="wrapper">' + photo +
                    '<div class="content">' +
                    '<a href="' + item.url + '" target="_self">' + CommonService.truncate(item.title, 70) +'</a>' + price + address +
                    '</div>' +
                    '</div>' +
                    '</div>' },
                    {
                        closeButton: true,
                        maxWidth: 300
                    });
            })
            .add('mouseenter', e => {
                e.get('target').options.set('iconImageHref', '/images/placemark-green.png');
            })
            .add('mouseleave', e => {
                e.get('target').options.set('iconImageHref', '/images/placemark.png');
            });
            this.placemarkCollection.add(placemarker);

        });
    }
}

export default yamapCommonController;

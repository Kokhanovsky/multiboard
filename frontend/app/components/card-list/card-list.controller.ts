import { ICard } from "../card/card.controller";
import * as angular from 'angular';
import { FindService, IPagination } from '../find/find.service';

interface ICustomDocument extends ng.IDocumentService {
    scrollTop: any;
    scrollToElement: any;
}

enum TScrollTo {
    none,
    top,
    cards
}

class CardListController {

    cards: ICard[];
    pagination: IPagination;
    mapPoints: any[];
    loading: boolean = false;
    firstLoad: boolean = true;
    scrollTo: TScrollTo = TScrollTo.top;
    sort: string;

    /** @ngInject */
    constructor(private $location: ng.ILocationService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService,
                private FindService: FindService,
                private $document: ICustomDocument) {
    }

    $onInit() {
        this.FindService.getWindowCards();
        this.$rootScope.$on('cardsRequest', (event: any, result: any) => this.loading = true);
        this.$rootScope.$on('cardsLoaded', (event: any, result: any) => {
            this.cards = this.FindService.cards;
            this.pagination = this.FindService.pagination;
            this.mapPoints = this.FindService.mapPoints;
            if (this.scrollTo === TScrollTo.cards) {
                let toElement = angular.element(document.getElementById('cards-scroll'));
                this.$document.scrollToElement(toElement, 10, 500);
            }
            this.scrollTo = TScrollTo.none;
        });

        this.$rootScope.$on('cardsError', () => {
            this.cards = [];
            this.pagination = { page: 1, pages: 1, limit: 1, total: 0 };
        });

        this.$rootScope.$on('cardsFinally', (event: any, result: any) => {
            this.loading = false;
            this.firstLoad = false;
        });


        this.$timeout(() => {
            this.sort = this.FindService.findParams.sort ? this.FindService.findParams.sort : 'best-match';
        });
    }

    doSort() {
        this.FindService.findParams.sort = (this.sort === 'best-match' ? '' : this.sort);
        delete this.FindService.findParams.p;
        this.FindService.buildUrl();
    }

    getCards(doScroll = false) {
        this.$timeout(() => {
            this.FindService.findParams.p = this.pagination.page;
            this.scrollTo = doScroll ? TScrollTo.cards : TScrollTo.none;
            this.FindService.buildUrl();
        });
    }

}

export default CardListController;

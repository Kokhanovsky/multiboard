import CardListController from './card-list.controller';

let cardListComponent: ng.IComponentOptions = {
    bindings: {
        cardListHeader: '<'
    },
    template: require('./card-list.html'),
    controller:  CardListController
};

export default cardListComponent;

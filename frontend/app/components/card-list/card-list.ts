import * as angular from "angular";
import cardListComponentOptions from './card-list.component';

import './card-list.sass';

let cardListModule: ng.IModule = angular.module('cardListModule', []);

cardListModule.component('cardList', cardListComponentOptions);

let cardListModuleName: string = cardListModule.name;

export default cardListModuleName;

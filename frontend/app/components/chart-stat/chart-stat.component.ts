import ChartStatController from './chart-stat.controller';
import './chart-stat.sass';

let chartStatComponent: ng.IComponentOptions = {
    bindings: {
    },
    template: require('./chart-stat.html'),
    controller:  ChartStatController
};

export default chartStatComponent;

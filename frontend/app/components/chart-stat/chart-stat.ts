import * as angular from 'angular';
import chartStatComponentOptions from './chart-stat.component';
import { ChartStatService } from './chart-stat.service';

let chartStatModule: ng.IModule = angular.module('chartStatModule', []);

chartStatModule.component('chartStat', chartStatComponentOptions);
chartStatModule.service('ChartStatService', ChartStatService);

let chartStatModuleName: string = chartStatModule.name;

export default chartStatModuleName;



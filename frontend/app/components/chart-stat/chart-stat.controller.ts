import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { ChartStatService } from "./chart-stat.service";

class ChartStatController {
    labels: string[];
    today: string;
    yesterday: string;
    period: string;
    data: any[];

    /** @ngInject */
    constructor(private ChartStatService: ChartStatService) {
        this.getStat(7);
    }

    getStat(period: number) {
        this.ChartStatService.getStat(period).then(res => {
            this.today = res.stat.today;
            this.yesterday = res.stat.yesterday;
            this.period = res.stat.period;
            this.labels = res.chart.labels;
            this.data = res.chart.data
        });
    }

}

export default ChartStatController;

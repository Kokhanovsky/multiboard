import * as angular from 'angular';
import * as moment from 'moment';
import { CommonService } from "../../lib/common.service";


export class ChartStatService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }


    getStat(days: number) {
        let utcOffset = moment.parseZone(new Date()).utcOffset();
        return this.$http(
            {
                method: 'POST',
                url: '/api/stat',
                data: { utcOffset, days }
            }).then((res: any) => {
                return this.parseResponse(res.data);
            });
    }

    parseResponse(data: any[]) {
        return {
            stat: {
                today: data[data.length - 1].count,
                yesterday: data[data.length - 2].count,
                period: 'За ' + data.length + CommonService.declOfNum(data.length, [' день', ' дня', ' дней']) + ': ' + data.reduce((a: number, b: any) => a + b['count'], 0)
            },
            chart: {
                labels: data.map(item => moment(new Date(item.date)).format('D MMM')),
                data: data.map(item => item.count)
            }
        };
    }

}

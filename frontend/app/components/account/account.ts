import * as angular from "angular";
import accountComponentOptions from './account.component';

let accountModule: ng.IModule = angular.module('accountModule', []);

accountModule.component('account', accountComponentOptions);

let accountModuleName: string = accountModule.name;

export default accountModuleName;



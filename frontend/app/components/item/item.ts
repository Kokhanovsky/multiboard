import * as angular from "angular";
import { ItemService } from './item.service';
import ItemController from './item.controller';
import './item.sass';

let itemModule: ng.IModule = angular.module('itemModule', []);

itemModule.controller('itemCtrl', ItemController);
itemModule.service('ItemService', ItemService);

let itemModuleName: string = itemModule.name;

export default itemModuleName;



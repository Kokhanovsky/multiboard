import * as angular from "angular";
import { ItemService } from './item.service';
import item from '../../../../backend/models/item';

interface IImage {
    url: string;
    thumbUrl: string;
}

interface IContacts {
    phone: string;
    contactPerson: string;
}

class ItemController {

    images: IImage[] = [];
    selectedIndex: number = 0;
    contacts: IContacts;
    geoObject: any;
    placemarkerCoordinates: any = [];

    /** @ngInject */
    constructor(private $timeout: ng.ITimeoutService,
                private $mdDialog: any,
                private $scope: ng.IScope,
                private ItemService: ItemService,
                private $window: ng.IWindowService) {

        this.$timeout(() => {
            this.preloadImages();
        });
    }

    $onInit() {
        this.$scope.$watch(() => this.placemarkerCoordinates, val => {
            this.geoObject = {
                geometry: {
                    type: 'Point',
                    coordinates: val
                }
            }
        });
    }

    preloadImages() {
        this.images.forEach((image: IImage) => {
            let img = new Image();
            img.src = image.url;
        });
    }

    next() {
        if (this.selectedIndex >= (this.images.length - 1)) {
            this.selectedIndex = 0;
        } else {
            this.selectedIndex++;
        }
    }

    prev() {
        if (this.selectedIndex <= 0) {
            this.selectedIndex = this.images.length - 1;
        } else {
            this.selectedIndex--;
        }
    }

    removeItem(itemId: string) {
        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .title('Удалить объявление?')
        .textContent('Выберите ответ Да или Нет.')
        .ok('Да')
        .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            this.ItemService.removeItem(itemId).then(() => {
                this.$window.location.href = '/user-items';
            });
        });
    }

    showContacts(itemId: string) {
        let self = this;
        this.$mdDialog.show({
            locals: { itemId, ItemController: self },
            controller: ShowContactsController,
            controllerAs: '$ctrl',
            template: require('./show.contacts.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        });
    }



}

class ShowContactsController {

    contacts: IContacts;

    /** @ngInject */
    constructor(private $mdDialog: any,
                private itemId: string,
                private ItemController: ItemController,
                private ItemService: ItemService) {

        ItemService.getContacts(itemId).then((contacts: IContacts) => {
            this.contacts = contacts;
            ItemController.contacts = contacts;
        });

    }

    cancel() {
        this.$mdDialog.cancel();
    }

}

export default ItemController;

import IPromise = angular.IPromise;

export class ItemService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getContacts(itemId: string) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/item/contacts',
                data: { itemId }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    removeItem(itemId: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/item',
                data: { itemId }
            });
    }
}

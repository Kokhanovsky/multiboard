import * as angular from 'angular';
import itemImageComponentOptions from './item-image.component';
let itemImageModule: ng.IModule = angular.module('itemImageModule', []);

itemImageModule.component('itemImage', itemImageComponentOptions);

let itemImageModuleName: string = itemImageModule.name;

export default itemImageModuleName;



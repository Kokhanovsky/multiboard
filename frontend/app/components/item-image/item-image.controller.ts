import * as angular from "angular";
import IHttpService = angular.IHttpService;

class ItemImageController {

    href: string;
    imageSrc: string[];
    photosCount: number;
    videosCount: number;
    activeSlide: number = 0;
    autoplay: boolean;
    rightArrDisabled: boolean = false;
    leftArrDisabled: boolean = true;
    interval: any;
    width: number;
    height: number;

    /** @ngInject */
    constructor(private $scope: ng.IScope, private $interval: ng.IIntervalService, private $timeout: ng.ITimeoutService) {
        $scope.$watch(() => this.autoplay, (autoplay: boolean) => {
            if (!this.imageSrc || !this.imageSrc.length) {
                return;
            }
            if (autoplay) {
                this.interval = $interval(() => {
                    this.next(true)
                }, 2500);
            } else {
                if (this.interval) $interval.cancel(this.interval);
            }
        })
    }

    $onInit() {
        //this.preloadImages();
    }

    preloadImages() {
        if (!this.imageSrc || !this.imageSrc.length) {
            return;
        }
        this.imageSrc.forEach(src => {
            let img: any = new Image();
            img.src = src;
        });
    }

    next(inf?: boolean) {
        if (this.activeSlide < this.imageSrc.length - 1) {
            this.activeSlide = this.activeSlide + 1;
        } else if (inf) {
            this.activeSlide = 0;
        }
        this.leftArrDisabled = false;
        this.rightArrDisabled = (this.activeSlide >= this.imageSrc.length - 1);
    }

    prev() {
        if (this.activeSlide > 0) {
            this.activeSlide = this.activeSlide - 1
        }
        this.leftArrDisabled = this.activeSlide <= 0;
        this.rightArrDisabled = false;
    }

}

export default ItemImageController;

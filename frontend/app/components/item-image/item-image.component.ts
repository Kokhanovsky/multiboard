import ItemImageController from './item-image.controller';
import './item-image.sass';

let itemImageComponent: ng.IComponentOptions = {
    bindings: {
        href: '<',
        imageSrc: '<',
        photosCount: '<',
        videosCount: '<',
        autoplay: '<',
        width: '<',
        height: '<'
    },
    template: require('./item-image.html'),
    controller:  ItemImageController
};

export default itemImageComponent;

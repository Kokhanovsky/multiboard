import IPromise = angular.IPromise;
import {IItemData} from "./additem.controller";

export class AddItemService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(itemData: IItemData) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/item',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    edit(itemData: any) {
        return this.$http(
            {
                method: 'PUT',
                url: '/api/item',
                data: itemData
            }).then((res: any) => res.data.url);
    }

    getItem(id: number) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/item/edit',
                params: { id }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

}

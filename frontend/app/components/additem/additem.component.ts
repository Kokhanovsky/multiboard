import AddItemController from './additem.controller';

let addItemComponent: ng.IComponentOptions = {
    bindings: {
        editId: '<'
    },
    template: require('./additem.html'),
    controller:  AddItemController
};

export default addItemComponent;

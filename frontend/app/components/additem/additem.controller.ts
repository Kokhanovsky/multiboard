import {AddItemService} from './additem.service';
import * as angular from 'angular';
import * as _ from 'lodash';

interface ICity {
    id: string;
    name: string;
}

interface IMetro {
    id: string;
    name: string;
}

interface IDistrict {
    id: string;
    name: string;
}

interface ILocation {
    city: ICity;
    metro?: IMetro;
    district?: IDistrict;
}

interface IPhoto {
    id: string;
    user?: string;
    item?: string;
}

interface IVideo {
    title: string;
    youtubeId: string;
}

export interface IItemData {
    contactPerson: string;
    companyName: string;
    phone: string;
    category: string;
    location: ILocation;
    title: string;
    description: string;
    price: number;
    photos: IPhoto[];
    videos: IVideo[];
    tags: string[];
}

class AddItemController {

    loading: boolean = false;
    itemData: IItemData;
    initialData: IItemData;
    keys: any[];
    editId: number;
    sendform: boolean = false;

    /** @ngInject */
    constructor(private AddItemService: AddItemService,
                private $mdConstant: any,
                private $window: ng.IWindowService,
                private $rootScope: ng.IRootScopeService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {
        this.$timeout(() => {
            this.initialData = angular.copy(this.itemData);
        });

        this.$window.onbeforeunload = event => {
            if (!angular.equals(this.itemData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };

        if (this.editId) {
            this.AddItemService.getItem(this.editId).then((item: IItemData) => {
                this.itemData = item;
                this.initialData = angular.copy(this.itemData);
            })
            .catch(() => {
                this.$window.location.href = '/';
            })
        }

        // working with commas when user does copy/paste or when user writes keywords with commas
        this.$rootScope.$watch(() => this.itemData.tags, (tags) => {
            let res = [];
            if (tags.length) {
                tags.forEach((tag: string) => {
                    res = res.concat(tag.split(',').map(tag => tag.trim())).filter(tag => tag != '');
                });
            }
            this.itemData.tags = _.uniq(res);
        }, true);

    }

    sendForm(formValid: boolean) {
        if (!formValid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.itemData);
        let action = this.editId ? this.AddItemService.edit(this.itemData) : this.AddItemService.add(this.itemData);
        action
            .then(url => {
                this.$window.location.href = url;
            })
            .catch((err: any) => {
                console.log(err);
                this.loading = false;
                (this.$rootScope as any).loading = true;
            });
    }

}

export default AddItemController;

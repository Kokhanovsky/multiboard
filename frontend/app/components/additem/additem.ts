import * as angular from "angular";
import additemComponentOptions from './additem.component';
import {AddItemService} from './additem.service';

let additemModule: ng.IModule = angular.module('additemModule', []);

additemModule.component('additem', additemComponentOptions);
additemModule.service('AddItemService', AddItemService);

let additemModuleName: string = additemModule.name;

export default additemModuleName;



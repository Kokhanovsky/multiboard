import * as angular from 'angular';
import { CommonService } from '../../lib/common.service';
import * as _ from 'lodash';
import { ICard } from '../card/card.controller';

export interface IBreadcrumb {
    title: string;
    slug: string;
}

export interface IPagination {
    total: number;
    page: number;
    pages: number;
    limit: number;
}

export interface ICat {
    slug: string;
    name: string;
}
export interface ILoc {
    slug: string;
    name: string;
}
interface ISubcategory {
    name: string;
    slug: string;
    counts: number;
}

export class FindService {

    findParams: any = {};
    subcategories: ISubcategory[];
    pagination: IPagination;
    cards: ICard[];
    canceler: ng.IDeferred<any>;
    breadcrumbs: IBreadcrumb[];
    title: string;
    titleHead: string;
    category: ICat;
    locations: ILoc[];
    mapPoints: any[];

    /** @ngInject */
    constructor(private $http: angular.IHttpService,
                private $rootScope: ng.IRootScopeService,
                private $location: ng.ILocationService,
                private $window: ng.IWindowService,
                private $timeout: ng.ITimeoutService,
                private $q: ng.IQService) {
    }

    getCards() {
        if (this.canceler) {
            this.canceler.resolve();
        }
        this.canceler = this.$q.defer();
        this.$rootScope.$emit('cardsRequest');
        (this.$rootScope as any).loading = true;
        return this.$http(
            {
                method: 'POST',
                url: '/api/find',
                data: this.findParams,
                timeout: this.canceler.promise
            })
        .then((response: any): any => {
            let res = response.data;
            res.items.docs = this.makeCards(res.items.docs);
            this.cards = res.items.docs;
            this.pagination = {
                page: res.items.page,
                pages: res.items.pages,
                limit: res.items.limit,
                total: res.items.total
            };
            this.subcategories = res.subcategories;
            this.breadcrumbs = res.breadcrumbs;
            this.title = res.title;
            this.titleHead = res.titleHead;
            this.category = res.category;
            this.locations = res.locations;
            this.mapPoints = res.mapPoints;
            this.$rootScope.$emit('cardsLoaded', res);
            return res;
        })
        .catch(err => this.$rootScope.$emit('cardsError', err))
        .then(() => {
            this.$rootScope.$emit('cardsFinally');
            (this.$rootScope as any).loading = false;
        })
    }

    makeCards(docs: any) {
        docs = docs.map((item: any) => {
            item.price = CommonService.thousandSeparator(item.price);
            item.createdAt = CommonService.formatToHuman(item.createdAt);
            return item;
        });
        return docs;
    }

    buildUrl() {
        let path = '';
        if (this.findParams.locations && this.findParams.locations.length) {
            path += '/' + this.findParams.locations[0];
        }
        if (this.findParams.category) {
            path += '/' + this.findParams.category;
        }
        let search: any = {};
        if (this.findParams.p) {
            search.p = this.findParams.p;
        }
        if (this.findParams.locations && this.findParams.locations.length > 1) {
            search.locations = this.findParams.locations.splice(1);
        }

        for (let p in this.findParams) {
            if ((p == 'category') || (p == 'locations')) {
                continue;
            }
            if (this.findParams.hasOwnProperty(p)) {
                if (this.findParams[p]) {
                    search[p] = this.findParams[p];
                }
            }
        }

        this.$location.path(path).search(search);

    }

    watchUrl() {
        this.$rootScope.$on('$locationChangeSuccess', () => {
            let path = this.$location.path();
            let regex = /^(\/[a-z0-9_-]+)(\/[a-z0-9\/_-]+)?\/?$/gi;
            let m;
            this.findParams = {};
            while ((m = regex.exec(path)) !== null) {
                // This is necessary to avoid infinite loops with zero-width matches
                if (m.index === regex.lastIndex) {
                    regex.lastIndex++;
                }
                let locations = m[1] ? [CommonService.trimChar('/', m[1])] : undefined;
                if (locations) {
                    this.findParams.locations = locations;
                }
                let category = CommonService.trimChar('/', m[2] ? m[2] : undefined);
                if (category) {
                    this.findParams.category = category;
                }

                let query = this.$location.search();

                if (query.locations) {
                    if (!query.locations.length) query.locations = [query.locations];
                    this.findParams.locations = !this.findParams.locations ? query.locations : _.uniq(this.findParams.locations.concat(query.locations));
                    delete query.locations;
                }

                if (query.p && query.p > 1) {
                    this.findParams.p = query.p << 0;
                    delete query.p;
                }

                for (let p in query) {
                    if (query.hasOwnProperty(p)) {
                        if (query[p]) {
                            this.findParams[p] = query[p];
                        }
                    }
                }

            }
            this.getCards();
        });
    }

    getWindowCards() {
        let window: any = this.$window;
        if (!window.items)
            return;
        if (window.items.docs) {
            this.pagination = {
                page: window.items.page,
                pages: window.items.pages,
                limit: window.items.limit,
                total: window.items.total
            };
            this.cards = this.makeCards(window.items.docs);
        } else {
            this.cards = this.makeCards(window.items);
        }
        this.$timeout(() => this.$rootScope.$emit('cardsLoaded', 23));
    }

}

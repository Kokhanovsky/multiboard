import { FindService, ILoc, ICat } from './find.service';

class FindController {

    title: string;
    titleHead: string;
    category: ICat;
    locations: ILoc[];
    mapPoints: any[];

    /** @ngInject */
    constructor(private FindService: FindService, private $rootScope: ng.IRootScopeService) {
    }

    $onInit() {
        this.FindService.watchUrl();
        this.$rootScope.$on('cardsLoaded', (event: any, result: any) => {
            this.title = this.FindService.title;
            this.titleHead = this.FindService.titleHead;
            this.category = this.FindService.category;
            this.locations = this.FindService.locations;
            this.mapPoints = this.FindService.mapPoints;
        });
    }

}

export default FindController;

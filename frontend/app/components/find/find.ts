import * as angular from "angular";
import FindController from './find.controller';
import { FindService } from './find.service';

let findModule: ng.IModule = angular.module('findModule', []);

findModule.service('FindService', FindService);
findModule.controller('find', FindController);

let findModuleName: string = findModule.name;

export default findModuleName;

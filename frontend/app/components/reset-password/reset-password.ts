import * as angular from "angular";
import resetPasswordComponentOptions from './reset-password.component';
import updatePasswordComponentOptions from './update-password.component';
import { ResetPasswordService } from './reset-password.service';

let resetPasswordModule: ng.IModule = angular.module('resetPasswordModule', []);

resetPasswordModule.component('resetPassword', resetPasswordComponentOptions);
resetPasswordModule.component('updatePassword', updatePasswordComponentOptions);
resetPasswordModule.service('ResetPasswordService', ResetPasswordService);

let resetPasswordModuleName: string = resetPasswordModule.name;

export default resetPasswordModuleName;



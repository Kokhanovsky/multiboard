import { ResetPasswordService } from './reset-password.service';

export interface IModelReset {
    email: string;
    captcha: string;
}

class ResetPasswordController {

    recaptchaSiteKey: string;
    widgetId: string;
    resetSuccessful:  boolean = false;
    loading: boolean = false;

    model: IModelReset = {
        email: '',
        captcha: null
    };
    serverError: string;

    /** @ngInject */
    constructor(private ResetPasswordService: ResetPasswordService,
                private $scope: ng.IScope,
                private vcRecaptchaService: any) {

        $scope.$watch(() => this.model, () => {
            this.serverError = null;
        }, true);

    }

    reset() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.ResetPasswordService.reset(this.model).then(() => {
            this.resetSuccessful = true;
        }).catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Ошибка сервера';
        }).then(() => {
            this.vcRecaptchaService.reload(this.widgetId);
            this.loading = false;
        });
    }

    setResponse(response: string) {
        this.model.captcha = response;
        console.info('Captcha response available');
    }

    setWidgetId(widgetId: string) {
        console.info('Created widget ID: %s', widgetId);
        this.widgetId = widgetId;
    }

    cbExpiration() {
        console.info('Captcha expired. Resetting response object');
        this.vcRecaptchaService.reload(this.widgetId);
        this.model.captcha = null;
    }

}

export default ResetPasswordController;

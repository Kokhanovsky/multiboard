import ResetPasswordController from './reset-password.controller';
import './reset-password.sass';

let ResetPasswordComponent: ng.IComponentOptions = {
    bindings: {
        recaptchaSiteKey: '<',
        showComponent: '='
    },
    template: require('./reset-password.html'),
    controller:  ResetPasswordController
};

export default ResetPasswordComponent;

import { ResendVerificationService } from './resend-verification.service';
import { ICustomRootScope } from '../../app.run';

export interface IModelReset {
    email: string;
    captcha: string;
}

class ResendVerificationController {

    recaptchaSiteKey: string;
    widgetId: string;
    successful:  boolean = false;
    loading: boolean = false;

    model: IModelReset = {
        email: '',
        captcha: null
    };
    serverError: string;

    /** @ngInject */
    constructor(private ResendVerificationService: ResendVerificationService,
                private $scope: ng.IScope,
                private $rootScope: ICustomRootScope,
                private $window: ng.IWindowService,
                private vcRecaptchaService: any) {

        if (!$rootScope.user) {
            $window.location.href = '/';
        }

        this.model.email = $rootScope.user.email;

        $scope.$watch(() => this.model, () => {
            this.serverError = null;
        }, true);

    }

    resend() {
        if (this.loading) {
            return;
        }
        this.loading = true;
        this.ResendVerificationService.resend(this.model).then(() => {
            this.successful = true;
        }).catch((err: any) => {
            this.serverError = err.data.message ? err.data.message : 'Произошла ошибка сервера.';
        }).then(() => {
            this.vcRecaptchaService.reload(this.widgetId);
            this.loading = false;
        });
    }

    setResponse(response: string) {
        this.model.captcha = response;
        console.info('Captcha response available');
    }

    setWidgetId(widgetId: string) {
        console.info('Created widget ID: %s', widgetId);
        this.widgetId = widgetId;
    }

    cbExpiration() {
        console.info('Captcha expired. Resetting response object');
        this.vcRecaptchaService.reload(this.widgetId);
        this.model.captcha = null;
    }

}

export default ResendVerificationController;

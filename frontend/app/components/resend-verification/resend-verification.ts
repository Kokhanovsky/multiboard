import * as angular from "angular";
import resendVerificationComponentOptions from './resend-verification.component';
import { ResendVerificationService } from './resend-verification.service';

let resendVerificationModule: ng.IModule = angular.module('resendVerificationModule', []);

resendVerificationModule.component('stResendVerification', resendVerificationComponentOptions);
resendVerificationModule.service('ResendVerificationService', ResendVerificationService);

let resendVerificationModuleName: string = resendVerificationModule.name;

export default resendVerificationModuleName;



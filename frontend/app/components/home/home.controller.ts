import * as angular from "angular";
import { YamapCommonService } from '../yamap-common/yamap-common.service';

class HomeController {

    itemWidth: string;
    itemHeight: string;
    city: string;
    mapPoints: any[];

    /** @ngInject */
    constructor(private YamapCommonService: YamapCommonService, private $timeout: ng.ITimeoutService, private $mdMedia: any, $scope: ng.IScope, $window: ng.IWindowService) {

        this.calcItemSizes();

        angular.element($window).bind('resize', () => {
            this.calcItemSizes();
            $scope.$apply();
        });

        this.$timeout(() => {
            if (this.city) {
                this.YamapCommonService.getPointsByCity(this.city).then(points => this.mapPoints = points);
            }
        });
    }

    private calcItemSizes() {
        let width = (this.$mdMedia('xs') ? 280 : this.$mdMedia('sm') ? 225 : this.$mdMedia('md') ? 170 : 160);
        this.itemWidth = width + 'px';
        this.itemHeight = width * 3/4 + 'px';
    }


}

export default HomeController;

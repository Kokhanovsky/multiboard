import AccountController from './login.controller';
import './login.sass';

let loginComponent: ng.IComponentOptions = {
    bindings: {
        loginRedir: '<',
        showComponent: '='
    },
    template: require('./login.html'),
    controller:  AccountController
};

export default loginComponent;

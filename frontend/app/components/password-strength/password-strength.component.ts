import PasswordStrengthController from './password-strength.controller';
import './password-strength.sass';

let passwordStrengthComponent: ng.IComponentOptions = {
    bindings: {
        value: '<'
    },
    template: require('./password-strength.html'),
    controller:  PasswordStrengthController
};

export default passwordStrengthComponent;

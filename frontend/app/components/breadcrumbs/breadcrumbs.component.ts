import BreadcrumbsController from './breadcrumbs.controller';
import './breadcrumbs.sass';

let breadcrumbsComponent: angular.IComponentOptions = {
    bindings: {},
    template: require('./breadcrumbs.html'),
    controller:  BreadcrumbsController
};

export default breadcrumbsComponent;

import { FindService, IBreadcrumb } from '../find/find.service';

class BreadcrumbsController {

    breadcrumbs: IBreadcrumb[];

    /** @ngInject */
    constructor(private FindService: FindService,
                private $rootScope: ng.IRootScopeService) {
    }

    $onInit() {
        this.$rootScope.$on('cardsLoaded', (event: any, result: any) => {
            this.breadcrumbs = this.FindService.breadcrumbs;
        });
    }

    go(slug: string, type: string) {
        if (type === 'category') {
            this.FindService.findParams.category = slug;
        }
        if (type === 'city') {
            this.FindService.findParams.locations[0] = slug;
            delete this.FindService.findParams.category;
        }
        delete this.FindService.findParams.p;
        this.FindService.buildUrl();
    }


}

export default BreadcrumbsController;

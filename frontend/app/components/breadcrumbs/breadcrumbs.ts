import * as angular from "angular";

import breadcrumbsComponentOptions from './breadcrumbs.component';

let breadcrumbsModule: ng.IModule = angular.module('breadcrumbsModule', []);

breadcrumbsModule.component('breadcrumbs', breadcrumbsComponentOptions);

let breadcrumbsModuleName: string = breadcrumbsModule.name;

export default breadcrumbsModuleName;



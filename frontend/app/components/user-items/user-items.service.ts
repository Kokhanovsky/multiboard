import * as angular from 'angular';
import { CommonService } from '../../lib/common.service';
import { IItem, IPhoto, ILocation } from '../../../../backend/models/item';
import * as moment from 'moment';

export interface IItemCustom {
    id: string;
    title: string;
    status: string;
    location: ILocation,
    description: string;
    remainRatio: number;
    upAt: string;
    isUp: string;
    url: string;
    price: string;
    selected?: boolean;
    itemNumber: number;
    user: any;
    photos: IPhoto[];
    photosCount: number;
    videosCount: number;
    isExpired: boolean;
    createdAt: string;
    expiredAt: string;
}

export class UserItemsService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getUserItems(page: number, titleFilter: string) {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user-items',
                params: { page, titleFilter }
            })
            .then((response: any): any => {
                if (!response.data.docs || !response.data.docs.length) {
                    return null;
                }
                let currentDate = response.data.current_date;
                let items = response.data.docs.map((item: IItem) => {
                    let isActivated = item.upAt && (item.upAt !== item.createdAt);
                    let remainDays = Math.round(moment.duration(moment(item.expiredAt).diff(moment(currentDate))).asDays());
                    let totalDays = 60;
                    let res = {
                        id: item._id,
                        title: item.title,
                        remainRatio: remainDays / totalDays * 100,
                        user: item.user,
                        itemNumber: item.itemNumber,
                        location: item.location,
                        price: CommonService.thousandSeparator(item.price),
                        url: CommonService.getItemUrl(item),
                        createdAt: CommonService.formatToHuman(item.createdAt),
                        upAt: CommonService.formatToHuman(item.upAt),
                        isUp: item.upAt && (item.upAt !== item.createdAt),
                        isExpired: moment(item.expiredAt) < moment(currentDate),
                        expiredAt: CommonService.declOfNum(remainDays, ['Остался', 'Осталось', 'Осталось']) + ' ' + remainDays + ' ' + CommonService.declOfNum(remainDays, ['день', 'дня', 'дней']),
                        photosCount: item.photos && item.photos.length,
                        videosCount: item.videos && item.videos.length,
                        photos: CommonService.getItemThumbnails(item.photos, item.videos, item.user as any),
                        tags: item.tags
                    };
                    return res;
                });
                let total = response.data.total;
                return {items, total};
            });
    }

    deleteItem(itemId: string) {
        return this.$http(
            {
                method: 'DELETE',
                url: '/api/item',
                data: { itemId }
            });
    }

    activate(itemIds: string[]) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/user-items/prolong',
                data: { itemIds }
            });
    }

}

import * as angular from "angular";
import userItemsComponentOptions from './user-items.component';
import {UserItemsService} from './user-items.service';

let userItemsModule: ng.IModule = angular.module('userItemsModule', []);

userItemsModule.component('userItems', userItemsComponentOptions);
userItemsModule.service('UserItemsService', UserItemsService);

let userItemsModuleName: string = userItemsModule.name;

export default userItemsModuleName;



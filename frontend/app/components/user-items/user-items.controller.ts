import * as angular from "angular";
import IHttpService = angular.IHttpService;
import { UserItemsService, IItemCustom } from "./user-items.service";
import { CommonService } from '../../lib/common.service';

class UserItemsController {

    userItems: IItemCustom[] = [];
    page: number = 1;
    total: number;
    _titleFilter: string;
    perPage: number = 10;
    loading: boolean = true;
    selected: string[] = [];
    _selectAll: boolean = false;
    someChecked: boolean = false;

    set titleFilter(value: string) {
        this._titleFilter = value;
        this.page = 1;
        this.getItems();
    }

    get titleFilter() {
       return this._titleFilter;
    }

    set selectAll(value: boolean) {
        this.someChecked = false;
        this.getSelected();
        if (this.selected.length) {
            value = false;
        }
        this._selectAll = value;
        this.userItems.map((item: IItemCustom) => item.selected = value);
    }

    get selectAll() {
        return this._selectAll;
    }

    /** @ngInject */
    constructor(private UserItemsService: UserItemsService,
                private $scope: ng.IScope,
                private $rootScope: any,
                private $mdDialog: any) {
        this.getItems();
    }

    getItems() {
        this.$rootScope.loading = true;
        this.selectAll = false;
        this.UserItemsService.getUserItems(this.page, this._titleFilter).then((res: any) => {
            if (!res) {
                this.total = 0;
                this.userItems = [];
                return;
            }
            this.userItems = res.items;
            this.highlightFilter();
            this.total = res.total;
        })
        .catch(() => { console.log('There were errors while loading user items') })
        .then(() => { this.$rootScope.loading = false })
    }

    getPage() {
        setTimeout(() => {
            this.getItems();
        })
    }

    select(itemId: string) {
        setTimeout(() => {
            let filteredItems = this.userItems.filter((item: IItemCustom) => item.selected);
            this._selectAll = (filteredItems.length === this.userItems.length);
            this.someChecked = (filteredItems.length !== this.userItems.length) && filteredItems.length > 0;
            this.$scope.$apply();
            this.getSelected();
        }, 5)
    }

    deleteItem(itemId: string) {
        let confirm = this.$mdDialog.confirm()
            .parent(document.body)
            .clickOutsideToClose(true)
            .title('Удалить объявление?')
            .textContent('Пожалуйста, выберите ответ Да или Нет.')
            .ok('Да')
            .cancel('Нет');
        this.$mdDialog.show(confirm).then(() => {
            this.UserItemsService.deleteItem(itemId).then(() => {
                this.userItems = this.userItems.filter((item: IItemCustom) => item.id !== itemId);
            });
        });
    }

    prolong(itemId?: string) {
        let ids = itemId ? [itemId] : this.selected;
        let content =  (ids.length > 1 ? ids.length + ' ' : '') + CommonService.declOfNum(ids.length, [
            'Объявление будет продлено',
            'объявления будут продлены',
            'объявлений будут продлены']) + ' на 60 дней';

        let confirm = this.$mdDialog.confirm()
        .parent(document.body)
        .clickOutsideToClose(true)
        .textContent(content)
        .ok('Продлить')
        .cancel('Отменить');
        this.$mdDialog.show(confirm).then(() => {
            this.UserItemsService.activate(ids).then(() => {

                let alert = this.$mdDialog.alert()
                .parent(document.body)
                .clickOutsideToClose(true)
                .textContent(CommonService.declOfNum(ids.length, [
                        'Срок размещения объявления успешно продлен',
                        'Сроки размещения объявлений успешно продлены',
                        'Сроки размещения объявлений успешно продлено']))
                .ok('Ok');
                this.$mdDialog.show(alert);

                this.getPage();
            }).catch(() => {
                alert('Возникла ошибка при продлении объявлений, повторите позже');
            });
        });
    }


    getSelected() {
        setTimeout(() => {
            this.selected = this.userItems.map((item: IItemCustom) => {
                return item.selected ? item.id : null;
            }).filter((val: string) => val);
            this.$scope.$apply();
        }, 5);
    }

    private highlightFilter(): IItemCustom[] {
        if (!this._titleFilter) {
            return this.userItems;
        }
        let res = angular.copy(this.userItems);
        this.userItems = res.map((item: IItemCustom) => {
                item.title = item.title.replace(new RegExp('('+this._titleFilter+')', 'gi'),
                    '<span class="highlighted">$1</span>');
            return item;
        });
    }

}

export default UserItemsController;

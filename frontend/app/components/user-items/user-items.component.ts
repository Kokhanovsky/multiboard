import UserItemsController from './user-items.controller';
import './user-items.sass';

let userItemsComponent: ng.IComponentOptions = {
    bindings: {},
    template: require('./user-items.html'),
    controller:  UserItemsController
};

export default userItemsComponent;

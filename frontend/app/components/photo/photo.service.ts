import IPromise = angular.IPromise;
import { IFile } from './photo.controller';

export class PhotoService {

    /** @ngInject */
    constructor(private Upload: any, private $http: angular.IHttpService) {
    }

    upload(files: Array<angular.angularFileUpload.IFileUploadConfigFile>): angular.angularFileUpload.IUploadPromise<any> {
        return this.Upload.upload({
            url: '/api/additem/photo/upload',
            arrayKey: '',
            data: {
                files
            },
            method: 'POST'
        });
    }

    deletePhoto(file: IFile) {
        return this.$http({
            method: 'DELETE',
            url: '/api/additem/photo',
            data: { file }
        });
    }
}

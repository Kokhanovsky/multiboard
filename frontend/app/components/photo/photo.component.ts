import PhotoController from './photo.controller';
import './photo.sass';

let PhotoComponent: ng.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    bindings: {
        itemId: "<",
        loading: "=",
        editMode: "<" // is it in edit mode or not
    },
    template: require('./photo.html'),
    controller:  PhotoController
};

export default PhotoComponent;

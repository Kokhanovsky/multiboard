import * as angular from "angular";
import AddItemRegionController from './additem-region.controller';
import { AddItemRegionService } from './additem-region.service';

let additemRegionModule: ng.IModule = angular.module('additemRegionModule', []);

additemRegionModule.controller('addItemRegionCtrl', AddItemRegionController);
additemRegionModule.service('AddItemRegionService', AddItemRegionService);

let additemRegionModuleName: string = additemRegionModule.name;

export default additemRegionModuleName;



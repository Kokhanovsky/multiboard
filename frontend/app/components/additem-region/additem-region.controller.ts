import { AddItemRegionService } from './additem-region.service';
import * as angular from 'angular';
import { CommonService } from '../../lib/common.service';
import * as _ from 'lodash';

interface ICity {
    id: string;
    name: string;
}

interface IMetro {
    id: string;
    name: string;
}

interface IDistrict {
    id: string;
    name: string;
}

interface ILocation {
    city: ICity;
    metro?: IMetro;
    district?: IDistrict;
}

interface IPhoto {
    id: string;
    user?: string;
    item?: string;
}

interface IVideo {
    title: string;
    youtubeId: string;
}

export interface IItemData {
    contactPerson: string;
    companyName: string;
    phone: string;
    category: string;
    location: ILocation;
    title: string;
    description: string;
    price: number;
    photos: IPhoto[];
    videos: IVideo[];
    tags: string[];
}

class AddItemRegionController {

    loading: boolean = false;
    itemData: IItemData;
    initialData: IItemData;
    keys: any[];
    sendform: boolean = false;
    recaptchaSiteKey: string;
    localStorageKey: string = 'addItemRegion' + window.location.pathname;
    form: ng.IFormController;

    /** @ngInject */
    constructor(private AddItemRegionService: AddItemRegionService,
                private localStorageService: ng.local.storage.ILocalStorageService,
                private $mdConstant: any,
                private $mdDialog: any,
                private $scope: ng.IScope,
                private $rootScope: ng.IRootScopeService,
                private $window: ng.IWindowService,
                private $location: ng.ILocationService,
                private $timeout: ng.ITimeoutService) {
        this.keys = [$mdConstant.KEY_CODE.ENTER];
    }

    $onInit() {

        let add = CommonService.getUrlParameter('add');

        if (add) {
            this.itemData = this.localStorageService.get(this.localStorageKey) as any;
            this.autoSendForm();
        }

        this.$timeout(() => {
            this.initialData = angular.copy(this.itemData);
        });

        this.$window.onbeforeunload = event => {
            if (!angular.equals(this.itemData, this.initialData)) {
                return 'confirm';
            } else {
                return;
            }
        };

        // working with commas when user does copy/paste or when user writes keywords with commas
        this.$scope.$watch(() => this.itemData.tags, (tags) => {
            let res = [];
            if (tags.length) {
                tags.forEach((tag: string) => {
                    res = res.concat(tag.split(',').map(tag => tag.trim())).filter(tag => tag != '');
                });
            }
            this.itemData.tags = _.uniq(res);
        }, true);

    }

    login() {
        this.$mdDialog.show({
            locals: { recaptchaSiteKey: this.recaptchaSiteKey },
            controller: RegisterController,
            controllerAs: '$ctrl',
            template: require('./templates/register.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then(() => {
            //
        }, () => {
            console.log('cancel');
        });
    }

    sendForm() {
        if (!this.form.$valid || !this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.initialData = angular.copy(this.itemData);
        this.localStorageService.set(this.localStorageKey, this.itemData);
        this.AddItemRegionService.add(this.itemData)
        .then(url => {
            this.localStorageService.remove(this.localStorageKey);
            this.$window.location.href = url;
        })
        .catch((err: any) => {
            this.loading = false;
            (this.$rootScope as any).loading = true;
            // if not authorized then open login dialog
            if (err.status && err.status === 401) {
                this.$window.scrollTo(0, 0); // needed for recaptcha
                this.login();
            }
        });
    }

    // count down and autosend form
    private autoSendForm() {
        this.$timeout(()=>{ // a problem with the backdrop
            if (!this.form.$valid) {
                return;
            }
            this.$mdDialog.show({
                controller: ['$scope', '$mdDialog', ($scope, $mdDialog) => {
                    $scope.counter = 3;
                    let timeout;
                    let onTimeout = () => {
                        $scope.counter--;
                        if ($scope.counter <= 0) {
                            $mdDialog.hide();
                            this.$timeout.cancel(timeout);
                            return;
                        }
                        timeout = this.$timeout(onTimeout, 1000);
                        console.log('created new timeout')
                    };
                    timeout = this.$timeout(onTimeout, 1000);

                    $scope.cancel = () => { this.$timeout.cancel(timeout); $mdDialog.cancel(); }
                }],
                autoWrap: true,
                skipHide: true,
                template: require('./templates/form-sending.html'),
                parent: angular.element(document.body)
            }).then(() => {
                this.sendform = true;
                this.sendForm();
            }).catch(() => {
                // cancel auto sending
            });
        });
    }

}

class RegisterController {

    serverError: string;
    loading: boolean = false;
    success: boolean = false;
    widgetId: string;
    captchaResponse: string;
    response: any;
    redirUrl: string = window.location.pathname + '?add=1';

    /** @ngInject */
    constructor(private $mdDialog: any,
                private recaptchaSiteKey: string,
                private vcRecaptchaService: any,
                private AddItemRegionService: AddItemRegionService) {

    }

    register() {
    }

    cancel() {
        this.$mdDialog.cancel();
    }

    setResponse(response: string) {
        this.captchaResponse = response;
        console.info('Captcha response available');
    }

    setWidgetId(widgetId: string) {
        console.info('Created widget ID: %s', widgetId);
        this.widgetId = widgetId;
    }

    cbExpiration() {
        console.info('Captcha expired. Resetting response object');
        this.vcRecaptchaService.reload(this.widgetId);
        this.captchaResponse = null;
    }

}

export default AddItemRegionController;

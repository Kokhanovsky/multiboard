import IPromise = angular.IPromise;
import { IItemData } from './additem-region.controller';

export class AddItemRegionService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    add(itemData: IItemData) {
        return this.$http(
            {
                method: 'POST',
                url: '/api/item',
                data: itemData
            }).then((res: any) => res.data.url);
    }

}

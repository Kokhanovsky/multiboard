import * as angular from "angular";
import cardComponentOptions from './card.component';
import './card.sass';

let cardModule: ng.IModule = angular.module('cardModule', []);

cardModule.component('card', cardComponentOptions);

let cardModuleName: string = cardModule.name;

export default cardModuleName;



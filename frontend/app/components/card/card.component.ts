import CardController from './card.controller';
import './card.sass';

let cardComponent: ng.IComponentOptions = {
    bindings: {
        cardData: '<',
        cardTile: '<'
    },
    template: require('./card.html'),
    controller:  CardController
};

export default cardComponent;

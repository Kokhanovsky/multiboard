export interface ICard {
    title: string;
    price: number;
    url: string;
    photosCount: number;
    videosCount: number;
    imagesSrc: string[];
    createdAt: Date;
}

class CardController {

    cardData: ICard;
    cardTail: boolean;

    /** @ngInject */
    constructor() {

    }

    $onInit() {
    }

}

export default CardController;

import * as angular from "angular";
import searchModuleName from './search/search';
import accountModuleName from './account/account';
import accountProfileModuleName from './account-profile/account-profile';
import loginModuleName from './login/login';
import signUpModuleName from './signup/signup';
import passwordStrengthModuleName from './password-strength/password-strength';
import resetPasswordModuleName from './reset-password/reset-password';
import resendVerificationModuleName from './resend-verification/resend-verification';
import avatarUploadModuleName from './avatar-upload/avatar-upload';
import photoModuleName from './photo/photo';
import treeModuleName from './category/category';
import additemModuleName from './additem/additem';
import additemRegionModuleName from './additem-region/additem-region';
import userItemsModuleName from './user-items/user-items';
import locationsModuleName from './location/location';
import itemModuleName from './item/item';
import videoEditModuleName from './video-edit/video-edit';
import statusModuleName from './status/status';
import itemImageModuleName from './item-image/item-image';
import yamapEditModuleName from './yamap-edit/yamap-edit';
import yamapCommonModuleName from './yamap-common/yamap-common';
import chartStatModuleName from './chart-stat/chart-stat';
import homeModuleName from './home/home';
import bigCitiesModuleName from './big-cities/big-cities';
import meterModuleName from './meter/meter';
import cardModuleName from './card/card';
import cardListModuleName from './card-list/card-list';
import findModuleName from './find/find';
import seoArticleModuleName from './seo-article/seo-article';
import searchFiltersModuleName from './search-filters/search-filters';
import breadcrumbsModuleName from './breadcrumbs/breadcrumbs';

import './image-gallery/ng-image-gallery';

let componentsModule: ng.IModule = angular.module('app.components', [
    searchModuleName,
    accountModuleName,
    accountProfileModuleName,
    loginModuleName,
    signUpModuleName,
    passwordStrengthModuleName,
    resetPasswordModuleName,
    resendVerificationModuleName,
    photoModuleName,
    avatarUploadModuleName,
    treeModuleName,
    additemModuleName,
    userItemsModuleName,
    locationsModuleName,
    itemModuleName,
    statusModuleName,
    videoEditModuleName,
    itemImageModuleName,
    yamapEditModuleName,
    yamapCommonModuleName,
    chartStatModuleName,
    additemRegionModuleName,
    homeModuleName,
    meterModuleName,
    bigCitiesModuleName,
    cardListModuleName,
    cardModuleName,
    findModuleName,
    seoArticleModuleName,
    searchFiltersModuleName,
    breadcrumbsModuleName
]);

let componentsModuleName: string = componentsModule.name;

export default componentsModuleName;

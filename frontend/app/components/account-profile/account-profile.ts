import * as angular from "angular";
import accountProfileComponentOptions from './account-profile.component';
import { AccountProfileService } from './account-profile.service';

let accountProfileModule: ng.IModule = angular.module('accountProfileModule', []);

accountProfileModule.component('stAccountProfile', accountProfileComponentOptions)
accountProfileModule.service('AccountProfileService', AccountProfileService);

let accountProfileModuleName: string = accountProfileModule.name;

export default accountProfileModuleName;



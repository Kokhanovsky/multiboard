import * as angular from "angular";
import seoArticleComponentOptions from './seo-article.component';
import { SeoArticleService } from './seo-article.service';

let seoArticleModule: ng.IModule = angular.module('seoArticleModule', []);

seoArticleModule.component('seoArticle', seoArticleComponentOptions);
seoArticleModule.service('SeoArticleService', SeoArticleService);

let seoArticleModuleName: string = seoArticleModule.name;

export default seoArticleModuleName;



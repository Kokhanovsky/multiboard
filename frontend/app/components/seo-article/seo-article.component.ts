import SeoArticleController from './seo-article.controller';
import './seo-article.sass';

let seoArticleComponent: ng.IComponentOptions = {
    bindings: {
        seoArticleOpen: '='
    },
    template: require('./seo-article.html'),
    controller:  SeoArticleController
};

export default seoArticleComponent;

import { ISeoArticleData } from './seo-article.controller';
import { FindService } from '../find/find.service';
import * as angular from "angular";

export class SeoArticleService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService,  private FindService: FindService,) {
    }

    submit(article: ISeoArticleData) {
        let data = angular.extend({}, article, this.FindService.findParams);
        return this.$http(
            {
                method: 'POST',
                url: '/api/seo-article',
                data
            }).then((res: any) => res.data.url);
    }

    getArticle() {
        return this.$http(
            {
                method: 'POST',
                url: '/api/seo-article-get',
                data: this.FindService.findParams
            }).then((res: any) => res.data);
    }

}

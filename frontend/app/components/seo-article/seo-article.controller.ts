import { SeoArticleService } from './seo-article.service';

export interface ISeoArticleData {
    title?: string;
    headerTitle?: string;
    metaDescription?: string;
    metaKeywords?: string[];
    content?: string;
}

class SeoArticleController {

    loading: boolean = false;
    sendform: boolean = false;
    formData: ISeoArticleData;
    seoArticleOpen: boolean = false;

    /** @ngInject */
    constructor(private $window: ng.IWindowService,
                private SeoArticleService: SeoArticleService,
                private $rootScope: ng.IRootScopeService) {
    }

    $onInit() {
        this.$rootScope.$on('$locationChangeSuccess', () => {
            this.seoArticleOpen = false;
        });
    }

    showArticle() {
        this.seoArticleOpen = true;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.SeoArticleService.getArticle()
        .then(res => res ? this.formData = res : this.resetForm())
        .catch(err => console.log(err))
        .then(() => {
            this.loading = false;
            (this.$rootScope as any).loading = false;
        });
    }

    sendForm() {
        if (!this.sendform) {
            return;
        }
        this.sendform = false;
        this.loading = true;
        (this.$rootScope as any).loading = true;
        this.SeoArticleService.submit(this.formData)
        .then(res => this.$window.location.reload())
        .catch(err => {
            this.loading = false;
            (this.$rootScope as any).loading = false;
        });
    }

    private resetForm() {
        this.formData = {
            title: '',
            metaDescription: '',
            metaKeywords: [],
            content: ''
        };
    }

}

export default SeoArticleController;

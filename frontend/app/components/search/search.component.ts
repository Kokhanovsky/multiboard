import SearchController from './search.controller';
import './search.sass';

let searchComponent: angular.IComponentOptions = {
    bindings: {},
    template: require('./search.html'),
    controller:  SearchController
};

export default searchComponent;

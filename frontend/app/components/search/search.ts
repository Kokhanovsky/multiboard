import * as angular from "angular";

import searchComponentOptions from './search.component';

let searchModule: ng.IModule = angular.module('searchModule', []);

searchModule.component('search', searchComponentOptions);

let searchModuleName: string = searchModule.name;

export default searchModuleName;



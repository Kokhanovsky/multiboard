import { FindService } from '../find/find.service';

interface ISubcategory {
    name: string;
    slug: string;
    counts: number;
}

class SearchController {

    focus: boolean = false;
    query: string;

    /** @ngInject */
    constructor(private FindService: FindService,
                private $location: ng.ILocationService,
                private $rootScope: ng.IRootScopeService) {
        let search = this.$location.search();
        this.query = (search && search.q) ? search.q : '';
        this.FindService.findParams.q = this.query;
    }

    $onInit() {
        this.$rootScope.$on('cardsLoaded', (event: any, result: any) => {
            this.query = this.FindService.findParams.q;
        });
    }

    doSearch() {
        this.FindService.findParams.q = this.query;
        delete this.FindService.findParams.p;
        this.FindService.buildUrl();
        //this.FindService.getCards();
    }

    clear() {
        this.query = '';
        delete this.FindService.findParams.q;
        delete this.FindService.findParams.p;
        this.FindService.buildUrl();
    }

}

export default SearchController;

import * as angular from "angular";
import locationController from './location.controller';
import './location.sass';

let locationComponent: angular.IComponentOptions = {
    require: {
        model: "ngModel"
    },
    template: require('./templates/location.html'),
    controller:  locationController
};

export default locationComponent;

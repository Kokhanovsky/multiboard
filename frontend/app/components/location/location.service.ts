import IPromise = angular.IPromise;

export class LocationService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getCitiesTop(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/cities-top'
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    getRegions(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/regions-grouped'
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    getCities(regionId: string): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/cities',
                data: { regionId }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    getMetros(cityId: string): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/metros',
                data: { cityId }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

    getDistricts(cityId: string): IPromise<any> {
        return this.$http(
            {
                method: 'POST',
                url: '/api/districts',
                data: { cityId }
            })
            .then((response: any): any => {
                return response.data;
            });
    }

}

import * as angular from "angular";
import { LocationService } from './location.service';

interface ICity {
    _id: string;
    name: string;
    slug: string;
    metros: number;
    districts: number;
}

interface IRegion {
    _id: string;
    name: string;
    slug: string;
}

interface IMetro {
    _id: string;
    name: string;
    slug: string;
}

interface IDistrict {
    _id: string;
    name: string;
    slug: string;
}

interface ILocationVal {
    region?: IRegion;
    city?: ICity;
    metro?: IMetro;
    district?: IDistrict;
}

class LocationController {
    public model: ng.INgModelController;
    locationVal: ILocationVal;

    /** @ngInject */
    constructor(private $mdDialog: any, private $scope: ng.IScope) {
        $scope.$watch(() => this.model.$viewValue, (val: ILocationVal) => {
            if (!val) {
                this.model.$setValidity("required", false);
            } else {
                this.model.$setValidity("required", true);
                this.locationVal = val;
            }
        });
    }

    select() {
        this.$mdDialog.show({
            //locals: { user: this.user },
            controller: SelectLocationController,
            controllerAs: '$ctrl',
            template: require('./templates/location-select.html'),
            parent: angular.element(document.body),
            clickOutsideToClose: true
        })
        .then((locationVal: any) => {
            this.locationVal = locationVal;
            this.model.$setViewValue(locationVal);
        }, () => {
            console.log('cancel');
        });
    }
}

class SelectLocationController {

    citiesTop: ICity[];
    regions: any[];
    cities: any[];
    metros: any[];
    districts: any[];
    selectedRegion: IRegion;
    selectedCity: ICity;
    mode: string;

    /** @ngInject */
    constructor(private $q: ng.IQService,
                private $scope: ng.IScope,
                private LocationService: LocationService,
                private $mdDialog: any) {
        this.setMode('cities-top');
    }

    selectRegion(region: IRegion, skipSub = false) {
        this.mode = 'loading';
        this.selectedRegion = region;
        if (skipSub) {
            let loc: ILocationVal = {
                region
            };
            this.$mdDialog.hide(loc);
        } else
        this.getCities(region._id).then(() => {
            this.setMode('cities');
        });
    }

    selectCity(city: ICity, skipSub = false) {
        this.mode = 'loading';
        this.selectedCity = city;
        if (city.metros && !skipSub) {
            this.LocationService.getMetros(city._id).then((metros: IMetro[]) => {
                this.metros = metros;
                this.setMode('metros');
            });
        } else
        if (city.districts && !skipSub) {
            this.LocationService.getDistricts(city._id).then((districts: IDistrict[]) => {
                this.districts = districts;
                this.setMode('districts');
            });
        } else {
            let loc: ILocationVal = {
                city
            };
            this.$mdDialog.hide(loc);
        }
    }

    selectMetro(metro: IMetro) {
        this.$mdDialog.hide({
            city: this.selectedCity,
            metro
        });
    }

    selectDistrict(district: IMetro) {
        this.$mdDialog.hide({
            city: this.selectedCity,
            district
        });
    }

    backToRegion() {
        if (this.selectedRegion) {
            this.selectRegion(this.selectedRegion);
        } else {
            this.setMode('cities-top');
        }
    }

    cancel() {
        this.$mdDialog.cancel();
    }

    private setMode(mode: string) {
        let self = this;
        this.mode = 'loading';
        let promise;
        switch (mode) {
            case 'cities-top':
                promise = this.getCitiesTop();
                break;
            case 'regions':
                this.selectedRegion = null;
                promise = this.getRegions();
                break;
            default:
                switchMode(mode);
        }
        if (promise) {
            promise.then(() => {
                switchMode(mode);
            });
        }
        function switchMode(mode: string) {
            setTimeout(() => {
                self.mode = mode;
                self.$scope.$apply();
            });
        }
    }

    private getCitiesTop() {
        let deferred = this.$q.defer();
        if (this.citiesTop) {
            deferred.resolve(this.citiesTop);
        } else {
            this.LocationService.getCitiesTop().then((citiesTop: ICity[]) => {
                this.citiesTop = citiesTop;
                deferred.resolve(citiesTop);
            });
        }
        return deferred.promise;
    }

    private getRegions() {
        let deferred = this.$q.defer();
        if (this.regions) {
            deferred.resolve(this.regions);
        } else {
            this.LocationService.getRegions().then((regions: IRegion[]) => {
                this.regions = regions;
                deferred.resolve(regions);
            });
        }
        return deferred.promise;
    }

    private getCities(regionId: string) {
        return this.LocationService.getCities(regionId).then((cities: IRegion[]) => {
            this.cities = cities;
        });
    }

}


export default LocationController;

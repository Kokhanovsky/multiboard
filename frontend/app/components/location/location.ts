import * as angular from "angular";
import locationComponentOptions from './location.component';
import { LocationService } from './location.service';

let locationModule: ng.IModule = angular.module('locationModule', []);

locationModule.component('location', locationComponentOptions);
locationModule.service('LocationService', LocationService);

let locationModuleName: string = locationModule.name;

export default locationModuleName;


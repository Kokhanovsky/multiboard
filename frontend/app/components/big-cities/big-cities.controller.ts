import * as angular from "angular";
import { BigCitiesService } from './big-cities.service';
import { CommonService } from '../../lib/common.service';

interface IBigCity {
    slug: string;
    name: string;
    region: string;
    cnt: number;
    population: number;
}

class BigCitiesController {

    cities: IBigCity[];

    /** @ngInject */
    constructor(private BigCitiesService: BigCitiesService, private CommonService: CommonService) {
        this.activate();
    }

    activate() {
        this.BigCitiesService.getCities().then(cities => this.cities = cities);
    }

    thousandSep(num: number) {
        return CommonService.thousandSeparator(num);
    }

}

export default BigCitiesController;

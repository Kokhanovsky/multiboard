import IPromise = angular.IPromise;

export class BigCitiesService {

    /** @ngInject */
    constructor(private $http: angular.IHttpService) {
    }

    getCities(): IPromise<any> {
        return this.$http(
            {
                method: 'GET',
                url: '/api/big-cities'
            })
            .then((response: any): any => {
                return response.data;
            });
    }
}

import * as angular from "angular";
import BigCitiesController from './big-cities.controller';
import { BigCitiesService } from './big-cities.service';
import './big-cities.controller';
import './big-cities.sass';

let BigCitiesModule: ng.IModule = angular.module('bigCitiesModule', []);

BigCitiesModule.controller('bigCitiesCtrl', BigCitiesController);
BigCitiesModule.service('BigCitiesService', BigCitiesService);

let bigCitiesModuleName: string = BigCitiesModule.name;

export default bigCitiesModuleName;

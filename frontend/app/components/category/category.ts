import * as angular from "angular";
import categoryComponentOptions from './category.component';
import { CategoryService } from './category.service';

let categoryModule: ng.IModule = angular.module('categoryModule', []);

categoryModule.component('category', categoryComponentOptions);
categoryModule.service('CategoryService', CategoryService);

let categoryModuleName: string = categoryModule.name;

export default categoryModuleName;


import { CategoryService } from './category.service';

interface ICategory {
    id: string;
    parentId: string;
    name: string;
    selected: boolean;
    children?: ICategory[];
}

interface ICurrentNode {
    categoryId: string;
    node: ICategory;
    caption: string;
    branch: ICategory[];
}

class CategoryController {

    public model: ng.INgModelController;
    public categories: any;
    public lists: any[] = [];
    public currentNode: ICurrentNode;

    /** @ngInject */
    constructor(private CategoryService: CategoryService, private $scope: ng.IScope) {
        CategoryService.getCategories().then((categories: ICategory[]) => {
            this.categories = categories;
            this.updateControl(this.model.$viewValue);
        });

        $scope.$watch(() => this.model.$viewValue, (catId: string) => {
            this.updateControl(catId);
        });
    }

    updateControl(catId: string) {
        if (!this.categories) {
            return;
        }
        this.currentNode = this.getCurrentNode(catId);
        if (!this.currentNode) {
            this.model.$setValidity("required", false);
            this.openMenu();
        } else {
            this.lists = [];
        }
    }

    openMenu() {
        this.lists = [this.categories];
        if (!this.currentNode) {
            return;
        }
        this.currentNode.branch.forEach(node => {
            this.getSiblings(node).map(node => node.selected = false);
            node.selected = true;
            if (node.children) {
                this.lists.push(node.children);
            }
        });
    }

    selectItem(node: ICategory) {
        if (!node.children) {
            this.model.$setViewValue(node.id);
            this.model.$setValidity("required",true);
            this.lists = [];
        } else {
            this.model.$setValidity("required",false);
            this.currentNode = null;
        }
        this.unselectSiblChild(node);
        node.selected = true;
        this.pushToLists(node);
    }

    selectThis(node: ICategory) {
        this.model.$setViewValue(node.id);
        this.model.$setValidity("required",true);
        this.lists = [];
        this.unselectSiblChild(node);
        node.selected = true;
        this.pushToLists(node);
    }

    private unselectSiblChild(node: ICategory) {
        this.getSiblings(node).map(node => {
            node.selected = false;
            unselect(node);
        });
        function unselect(node: ICategory) {
            if (!node.children)
                return;
            node.children.map(node => {
                node.selected = false;
                unselect(node);
            })
        }
    }

    private getCurrentNode(id: string): ICurrentNode {
        let res: ICurrentNode;
        let branch = this.getBranch(id);
        if (!branch || !branch.length) {
            return null;
        }
        res = {
            branch,
            categoryId: branch[branch.length - 1].id,
            node: branch[branch.length - 1],
            caption: branch.map((node: ICategory) => node.name).join(' / ')
        };
        return res;
    }

    private getBranch(id: string) {
        let res: any[] = [];
        let node = this.getCategoryById(id);
        if (!node) {
            return [];
        }
        res.push(node);
        while (node.parentId) {
            node = this.getCategoryById(node.parentId);
            res.push(node);
        }
        return res.reverse();
    }

    private pushToLists(node: any) {
        this.lists = this.lists.slice(0, node.depth + 1);
        if (node.children) {
            this.lists.push(node.children);
        }
    }

    private getSiblings(node: ICategory): ICategory[] {
        if (!node.parentId) {
            return this.categories;
        }
        return this.getCategoryById(node.parentId).children;
    }

    private getCategoryById(id: string) {
        let cat: ICategory = null;
        goThrough(this.categories);
        function goThrough(node: ICategory[]) {
            node.forEach((node: ICategory) => {
                if (node.id === id) {
                    cat = node;
                    return;
                }
                if (node.children) {
                    goThrough(node.children);
                }
            });
        }
        return cat;
    }

}

export default CategoryController;

import IRootScopeService = angular.IRootScopeService;
/** @ngInject */
export default function AppConfig($mdAriaProvider: any,
                                  $locationProvider: ng.ILocationProvider,
                                  ChartJsProvider: any,
                                  localStorageServiceProvider: ng.local.storage.ILocalStorageServiceProvider) {

    localStorageServiceProvider.setPrefix('multiboard');
    $mdAriaProvider.disableWarnings();
    ChartJsProvider.setOptions({
        maintainAspectRatio: false
    });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

}


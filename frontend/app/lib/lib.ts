import { AuthService } from './auth.service';
import { IdleService } from './idle.service';
import { CommonService } from './common.service';
import appConfig from './app.config';
import idleConfig from './idle.config';
import httpConfig from './http.config';
import themeConfig from './theme.config';
import * as angular from "angular";

let libModule: ng.IModule = angular.module('app.lib', []);

libModule.service('AuthService', AuthService);
libModule.service('IdleService', IdleService);
libModule.service('CommonService', CommonService);
libModule.config(appConfig);
libModule.config(idleConfig);
libModule.config(httpConfig);
libModule.config(themeConfig);

let libModuleName: string = libModule.name;

export default libModuleName;

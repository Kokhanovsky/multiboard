export class AuthService {

    /** @ngInject */
    constructor(private $http: ng.IHttpService,
                private $rootScope: ng.IRootScopeService,
                private Idle: ng.idle.IIdleService,
                private $window: ng.IWindowService) {
    }

    authstatus() {
        console.log('Auth status request');
        return this.$http.get('/api/authstatus');
    }

    logout() {
        console.log('Logout request');
        return this.$http.get('/api/logout').then(() => {
            (this.$rootScope as any).user = null;
            this.Idle.unwatch();
            if ((this.$rootScope as any).authRequired) {
                this.$window.location.href = '/login';
            }
        });
    }

}

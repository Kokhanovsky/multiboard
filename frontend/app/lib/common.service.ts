import { ICustomRootScope } from '../app.run';
import * as moment from 'moment';
import { IUser } from '../../../backend/models/user';
import { IPhoto, IYoutubeItem, IItem } from '../../../backend/models/item';

export class CommonService {

    /** @ngInject */
    constructor(private $rootScope: ICustomRootScope, private $http: ng.IHttpService) {
    }

    getUserData() {
        return this.$http(
            {
                method: 'GET',
                url: '/api/user'
            })
        .then((response: any): any => {
            return response.data;
        });

    }

    static declOfNum(num: number, titles: string[]) {
        let cases = [2, 0, 1, 1, 1, 2];
        num = Math.abs(num);
        return titles[(num % 100 > 4 && num % 100 < 20) ? 2 : cases[(num % 10 < 5) ? num % 10 : 5]];
    }

    static thousandSeparator(str: any) {
        if (!str) {
            return str;
        }
        return str.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1&thinsp;')
    }

    static formatToHuman(date: Date) {
        moment.locale("ru");
        let long = 'D MMMM в H:mm';
        if (moment().year() > moment(date).year()) {
            long = 'D MMMM YYYY г. в H:mm';
        }
        return moment(date).calendar(null, {
            lastWeek: long,
            nextWeek: long,
            sameElse: long
        }).toLocaleLowerCase();
    }

    static getProfileUrl(user: IUser) {
        return '/' + (user.nickname ? 'user/' + user.nickname : 'id/' + user.shortId);
    }

    static stripTags(input: string, allowed: string) {
        allowed = (((allowed || '') + '').toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
        let tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi;
        let commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : ''
        });
    }

    static getItemThumbnails(photos: IPhoto[], videos: IYoutubeItem[], userId: string) {
        let resPhotos = photos.map((photo: IPhoto) => '/storage/cache/items/' + userId + '/' + photo.file + '/200x200.jpg');
        let resVideos = videos.map((video: IYoutubeItem) => 'https://img.youtube.com/vi/' + video.youtubeId + '/mqdefault.jpg');
        return resPhotos.concat(resVideos);
    }

    static getItemUrl(item: IItem) {
        return '/' + (item.location.city ? item.location.city.slug : item.location.region.slug) + '/' + item.category.slug + '/' + item.slug + '_' + item.itemNumber;
    }

    static getUrlParameter(param: string, dummyPath?: string) {
        let sPageURL = dummyPath || window.location.search.substring(1),
            sURLVariables = sPageURL.split(/[&||?]/),
            res;

        for (let i = 0; i < sURLVariables.length; i += 1) {
            let paramName = sURLVariables[i],
                sParameterName = (paramName || '').split('=');

            if (sParameterName[0] === param) {
                res = sParameterName[1];
            }
        }

        return res;
    }

    static truncate(text: string, n: number, useWordBoundary: boolean = true) {
        let isTooLong = text.length > n,
            s_ = isTooLong ? text.substr(0, n - 1) : text;
        s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
        return isTooLong ? s_ + '...' : s_;
    }

    static trimChar(charToRemove: string, str: string) {
        if (!str) {
            return str;
        }
        while (str.charAt(0) == charToRemove) {
            str = str.substring(1);
        }
        while (str.charAt(str.length - 1) == charToRemove) {
            str = str.substring(0, str.length - 1);
        }
        return str;
    }

}

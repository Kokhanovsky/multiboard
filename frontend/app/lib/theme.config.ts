/** @ngInject */
export default function ThemeConfig($mdThemingProvider: any) {
    $mdThemingProvider.theme('altTheme')
    .primaryPalette('grey',{
        'default': '900'})
    .accentPalette('grey',{
        'default': '700'})
    $mdThemingProvider.theme('default');
    $mdThemingProvider.setDefaultTheme('altTheme');
    $mdThemingProvider.alwaysWatchTheme(true);
}

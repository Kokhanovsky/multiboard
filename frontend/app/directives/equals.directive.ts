import * as angular from "angular";

let equals = function (): angular.IDirective {

    return {
        restrict: 'A', // only activate on element attribute
        require: '?ngModel', // get a hold of NgModelController
        link: (scope: ng.IScope, elem: any, attrs: any, ngModel: ng.INgModelController) => {
            if(!ngModel) {
                return;
            } // do nothing if no ng-model

            // watch own value and re-validate on change
            scope.$watch(attrs.ngModel, () => {
                validate();
            });

            // observe the other value and re-validate on change
            attrs.$observe('equals', (val: any) => {
                validate();
            });

            let validate = () => {
                // values
                let val1 = ngModel.$viewValue;
                let val2 = attrs.equals;

                // set validity
                ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
            };
        }
    }

};

let equalsModule: ng.IModule = angular.module('equals', []);

equalsModule.directive('equals', equals);

let equalsModuleName: string = equalsModule.name;

export default equalsModuleName;


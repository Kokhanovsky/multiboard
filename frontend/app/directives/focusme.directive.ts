import * as angular from "angular";

let focusMe = function (): angular.IDirective {

    return {
        scope: {
            focusMe: '=focusMe'
        },
        link: function (scope: any, element: any) {
            scope.$watch(() => {
                return scope.focusMe;
            }, (value: any) => {
                if (value === true) {
                    setTimeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                console.log('blur activated');
                scope.focusMe = false;
                scope.$apply();
            });
            element.bind('focus', function () {
                scope.focusMe = true;
                scope.$apply();
            });
        }
    };

};

let focusMeModule: ng.IModule = angular.module('focusMe', []);

focusMeModule.directive('focusMe', focusMe);

let focusMeModuleName: string = focusMeModule.name;

export default focusMeModuleName;


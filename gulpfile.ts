import { Gulpclass, Task, SequenceTask } from 'gulpclass/Decorators';
import { Gulp } from 'gulp';

let gulp: Gulp = require('gulp');
let ts = require('gulp-typescript');
let del = require('del');

@Gulpclass()
export class Gulpfile {

    @Task()
    copy(cb: Function) {
        // copy config
        return gulp.src(['./backend/config/**/*', './backend/public/**/*', './backend/views/**/*', './backend/data/**/*', './backend/**/*.ejs', './backend/**/*.txt'], { base: './backend', dot: true })
        .pipe(gulp.dest('dist'), cb);
    }

    @Task()
    copyPackage(cb: Function) {
        return gulp.src('package.json')
        .pipe(gulp.dest('./dist'), cb);
    }

    @Task()
    clean(cb: Function) {
        return del(['dist/**/*', '!dist/node_modules', '!dist/node_modules/**/*'], cb);
    }

    @Task()
    compile(cb: Function) {
        return gulp.src(['./backend/**/*.ts', './typings/index.d.ts'])
        .pipe(ts({
            'sourceMap': true,
            "target": "ES5"
        }))
        .pipe(gulp.dest('dist'), cb);
    }

    @Task('const')
    compileConst(cb: any) {
        return gulp.src(['./backend/const.ts', './typings/index.d.ts'])
        .pipe(ts({
            'sourceMap': true,
            "target": "ES5"
        }))
        .pipe(gulp.dest('./backend'), cb);
    }

    @Task('wp')
    compileWebpackConf(cb: any) {
        return gulp.src(['./webpack*.ts', './typings/index.d.ts'])
        .pipe(ts({
            'sourceMap': true,
            "target": "ES5"
        }))
        .pipe(gulp.dest('./'), cb);
    }

    @SequenceTask()
    default() {
        return ['clean', 'compile', 'copy', 'copyPackage'];
    }


}
